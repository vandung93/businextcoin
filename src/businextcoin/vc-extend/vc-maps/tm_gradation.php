<?php

class WPBakeryShortCode_TM_Gradation extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		Businextcoin_VC::get_vc_spacing_css( $selector, $atts );
	}
}

vc_map( array(
	'name'                      => esc_html__( 'Gradation', 'businextcoin' ),
	'base'                      => 'tm_gradation',
	'category'                  => BUSINEXTCOIN_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-list',
	'allowed_container_element' => 'vc_row',
	'params'                    => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'businextcoin' ),
			'type'        => 'dropdown',
			'param_name'  => 'list_style',
			'value'       => array(
				esc_html__( 'Basic', 'businextcoin' ) => 'basic',
			),
			'admin_label' => true,
			'std'         => 'basic',
		),
		array(
			'group'      => esc_html__( 'Items', 'businextcoin' ),
			'heading'    => esc_html__( 'Items', 'businextcoin' ),
			'type'       => 'param_group',
			'param_name' => 'items',
			'params'     => array(
				array(
					'heading'     => esc_html__( 'Title', 'businextcoin' ),
					'type'        => 'textfield',
					'param_name'  => 'title',
					'admin_label' => true,
				),
				array(
					'heading'    => esc_html__( 'Description', 'businextcoin' ),
					'type'       => 'textarea',
					'param_name' => 'text',
				),
			),

		),

	), Businextcoin_VC::get_vc_spacing_tab() ),
) );
