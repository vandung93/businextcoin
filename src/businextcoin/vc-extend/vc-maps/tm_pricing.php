<?php

class WPBakeryShortCode_TM_Pricing extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		Businextcoin_VC::get_vc_spacing_css( $selector, $atts );
	}
}

vc_map( array(
	'name'                      => esc_html__( 'Pricing Table', 'businextcoin' ),
	'base'                      => 'tm_pricing',
	'category'                  => BUSINEXTCOIN_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-pricing',
	'allowed_container_element' => 'vc_row',
	'params'                    => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'businextcoin' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( '01', 'businextcoin' ) => '1',
				esc_html__( '02', 'businextcoin' ) => '2',
				esc_html__( '03', 'businextcoin' ) => '3',
			),
			'std'         => '1',
		),
		array(
			'heading'     => esc_html__( 'Featured', 'businextcoin' ),
			'description' => esc_html__( 'Checked the box if you want make this item featured', 'businextcoin' ),
			'type'        => 'checkbox',
			'param_name'  => 'featured',
			'value'       => array( esc_html__( 'Yes', 'businextcoin' ) => '1' ),
		),
		array(
			'heading'    => esc_html__( 'Image', 'businextcoin' ),
			'type'       => 'attach_image',
			'param_name' => 'image',
		),
		array(
			'heading'     => esc_html__( 'Title', 'businextcoin' ),
			'type'        => 'textfield',
			'admin_label' => true,
			'param_name'  => 'title',
		),
		array(
			'heading'     => esc_html__( 'Description', 'businextcoin' ),
			'description' => esc_html__( 'Controls the text that display under price', 'businextcoin' ),
			'type'        => 'textarea',
			'param_name'  => 'desc',
		),
		array(
			'heading'          => esc_html__( 'Currency', 'businextcoin' ),
			'type'             => 'textfield',
			'param_name'       => 'currency',
			'value'            => '$',
			'edit_field_class' => 'vc_col-sm-4',
		),
		array(
			'heading'          => esc_html__( 'Price', 'businextcoin' ),
			'type'             => 'textfield',
			'param_name'       => 'price',
			'edit_field_class' => 'vc_col-sm-4',
		),
		array(
			'heading'          => esc_html__( 'Period', 'businextcoin' ),
			'type'             => 'textfield',
			'param_name'       => 'period',
			'value'            => 'per monthly',
			'edit_field_class' => 'vc_col-sm-4',
		),
		array(
			'type'       => 'vc_link',
			'heading'    => esc_html__( 'Button', 'businextcoin' ),
			'param_name' => 'button',
		),
		Businextcoin_VC::get_animation_field(),
		Businextcoin_VC::extra_class_field(),
		array(
			'group'      => esc_html__( 'Items', 'businextcoin' ),
			'heading'    => esc_html__( 'Items', 'businextcoin' ),
			'type'       => 'param_group',
			'param_name' => 'items',
			'params'     => array(
				array(
					'heading'    => esc_html__( 'Icon', 'businextcoin' ),
					'type'       => 'iconpicker',
					'param_name' => 'icon',
					'settings'   => array(
						'emptyIcon'    => true,
						'type'         => 'ion',
						'iconsPerPage' => 400,
					),
					'value'      => '',
				),
				array(
					'heading'     => esc_html__( 'Text', 'businextcoin' ),
					'type'        => 'textfield',
					'param_name'  => 'text',
					'admin_label' => true,
				),
			),
		),
	), Businextcoin_VC::icon_libraries( array(
		'allow_none' => true,
	) ), Businextcoin_VC::get_vc_spacing_tab() ),
) );
