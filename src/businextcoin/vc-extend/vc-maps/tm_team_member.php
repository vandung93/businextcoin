<?php

class WPBakeryShortCode_TM_Team_Member extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		Businextcoin_VC::get_vc_spacing_css( $selector, $atts );
	}
}

vc_map( array(
	'name'                      => esc_html__( 'Team Member', 'businextcoin' ),
	'base'                      => 'tm_team_member',
	'category'                  => BUSINEXTCOIN_VC_SHORTCODE_CATEGORY,
	'allowed_container_element' => 'vc_row',
	'icon'                      => 'insight-i insight-i-member',
	'params'                    => array_merge( array(
		array(
			'type'        => 'dropdown',
			'heading'     => esc_html__( 'Style', 'businextcoin' ),
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( '01', 'businextcoin' ) => '01',
				esc_html__( '02', 'businextcoin' ) => '02',
				esc_html__( '03', 'businextcoin' ) => '03',
				esc_html__( '04', 'businextcoin' ) => '04',
				esc_html__( '05', 'businextcoin' ) => '05',
				esc_html__( '06', 'businextcoin' ) => '06',
				esc_html__( '07', 'businextcoin' ) => '07',
				esc_html__( '08', 'businextcoin' ) => '08',
				esc_html__( '09', 'businextcoin' ) => '09',
			),
			'std'         => '01',
		),
		array(
			'type'       => 'dropdown',
			'heading'    => esc_html__( 'Skin', 'businextcoin' ),
			'param_name' => 'skin',
			'value'      => array(
				esc_html__( 'Dark', 'businextcoin' )  => 'dark',
				esc_html__( 'Light', 'businextcoin' ) => 'light',
			),
			'std'        => 'light',
		),
		array(
			'type'        => 'attach_image',
			'heading'     => esc_html__( 'Photo of member', 'businextcoin' ),
			'param_name'  => 'photo',
			'admin_label' => true,
		),
		array(
			'type'        => 'textfield',
			'heading'     => esc_html__( 'Name', 'businextcoin' ),
			'admin_label' => true,
			'param_name'  => 'name',
		),
		array(
			'type'        => 'textfield',
			'heading'     => esc_html__( 'Position', 'businextcoin' ),
			'param_name'  => 'position',
			'description' => esc_html__( 'Example: CEO/Founder', 'businextcoin' ),
		),
		array(
			'type'       => 'textarea',
			'heading'    => esc_html__( 'Description', 'businextcoin' ),
			'param_name' => 'desc',
		),
		array(
			'type'       => 'textfield',
			'heading'    => esc_html__( 'Profile url', 'businextcoin' ),
			'param_name' => 'profile',
		),
		Businextcoin_VC::get_animation_field(),
		Businextcoin_VC::extra_class_field(),
		array(
			'group'      => esc_html__( 'Social Networks', 'businextcoin' ),
			'heading'    => esc_html__( 'Show tooltip as item title.', 'businextcoin' ),
			'type'       => 'checkbox',
			'param_name' => 'tooltip_enable',
			'value'      => array(
				esc_html__( 'Yes', 'businextcoin' ) => '1',
			),
		),
		array(
			'group'      => esc_html__( 'Social Networks', 'businextcoin' ),
			'heading'    => esc_html__( 'Tooltip Skin', 'businextcoin' ),
			'type'       => 'dropdown',
			'param_name' => 'tooltip_skin',
			'value'      => Businextcoin_VC::get_tooltip_skin_list(),
			'std'        => '',
			'dependency' => array(
				'element' => 'tooltip_enable',
				'value'   => '1',
			),
		),
		array(
			'group'      => esc_html__( 'Social Networks', 'businextcoin' ),
			'heading'    => esc_html__( 'Tooltip Position', 'businextcoin' ),
			'type'       => 'dropdown',
			'param_name' => 'tooltip_position',
			'value'      => array(
				esc_html__( 'Top', 'businextcoin' )    => 'top',
				esc_html__( 'Right', 'businextcoin' )  => 'right',
				esc_html__( 'Bottom', 'businextcoin' ) => 'bottom',
				esc_html__( 'Left', 'businextcoin' )   => 'left',
			),
			'std'        => 'top',
			'dependency' => array(
				'element' => 'tooltip_enable',
				'value'   => '1',
			),
		),
		array(
			'group'      => esc_html__( 'Social Networks', 'businextcoin' ),
			'type'       => 'param_group',
			'heading'    => esc_html__( 'Social Networks', 'businextcoin' ),
			'param_name' => 'social_networks',
			'params'     => array_merge( Businextcoin_VC::icon_libraries( array( 'allow_none' => true ) ), array(
				array(
					'type'        => 'textfield',
					'heading'     => esc_html__( 'Link', 'businextcoin' ),
					'param_name'  => 'link',
					'admin_label' => true,
				),
				array(
					'type'        => 'textfield',
					'heading'     => esc_html__( 'Title', 'businextcoin' ),
					'param_name'  => 'title',
					'admin_label' => true,
				),
			) ),
			'value'      => rawurlencode( wp_json_encode( array(
				array(
					'link'      => '#',
					'icon_type' => 'ion',
					'icon_ion'  => 'ion-social-twitter',
					'title'     => esc_html__( 'Twitter', 'businextcoin' ),
				),
				array(
					'link'      => '#',
					'icon_type' => 'ion',
					'icon_ion'  => 'ion-social-facebook',
					'title'     => esc_html__( 'Facebook', 'businextcoin' ),
				),
				array(
					'link'      => '#',
					'icon_type' => 'ion',
					'icon_ion'  => 'ion-social-googleplus',
					'title'     => esc_html__( 'Google+', 'businextcoin' ),
				),
				array(
					'link'      => '#',
					'icon_type' => 'ion',
					'icon_ion'  => 'ion-social-linkedin',
					'title'     => esc_html__( 'Linkedin', 'businextcoin' ),
				),
			) ) ),
		),
	), Businextcoin_VC::get_vc_spacing_tab() ),
) );
