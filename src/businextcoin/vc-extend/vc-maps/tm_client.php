<?php

class WPBakeryShortCode_TM_Client extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		if ( in_array( $atts['style'], array( 'grid' ) ) ) {
			Businextcoin_VC::get_grid_css( $selector, $atts );
		}

		Businextcoin_VC::get_vc_spacing_css( $selector, $atts );
	}
}

$slides_tab = esc_html__( 'Slides', 'businextcoin' );

vc_map( array(
	'name'                      => esc_html__( 'Client Logos', 'businextcoin' ),
	'base'                      => 'tm_client',
	'category'                  => BUSINEXTCOIN_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-carousel',
	'allowed_container_element' => 'vc_row',
	'params'                    => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'businextcoin' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Carousel', 'businextcoin' ) => 'carousel',
				esc_html__( 'Grid', 'businextcoin' )     => 'grid',
			),
			'std'         => 'carousel',
		),
		array(
			'heading'     => esc_html__( 'Items Display', 'businextcoin' ),
			'type'        => 'number_responsive',
			'param_name'  => 'columns',
			'min'         => 1,
			'max'         => 10,
			'suffix'      => 'item (s)',
			'media_query' => array(
				'lg' => 4,
				'md' => 3,
				'sm' => 2,
				'xs' => 1,
			),
			'dependency'  => array( 'element' => 'style', 'value' => 'grid' ),
		),
		array(
			'heading'     => esc_html__( 'Items Display', 'businextcoin' ),
			'type'        => 'number_responsive',
			'param_name'  => 'items_display',
			'min'         => 1,
			'max'         => 10,
			'suffix'      => 'item (s)',
			'media_query' => array(
				'lg' => 6,
				'md' => 4,
				'sm' => 3,
				'xs' => 2,
			),
			'dependency'  => array( 'element' => 'style', 'value_not_equal_to' => 'grid' ),
		),
		array(
			'heading'    => esc_html__( 'Gutter', 'businextcoin' ),
			'type'       => 'number',
			'param_name' => 'gutter',
			'std'        => 30,
			'min'        => 0,
			'max'        => 50,
			'step'       => 1,
			'suffix'     => 'px',
		),
		array(
			'heading'    => esc_html__( 'Loop', 'businextcoin' ),
			'type'       => 'checkbox',
			'param_name' => 'loop',
			'value'      => array( esc_html__( 'Yes', 'businextcoin' ) => '1' ),
			'std'        => '1',
			'dependency' => array( 'element' => 'style', 'value_not_equal_to' => 'grid' ),
		),
		array(
			'heading'     => esc_html__( 'Auto Play', 'businextcoin' ),
			'description' => esc_html__( 'Delay between transitions (in ms), ex: 3000. Leave blank to disabled.', 'businextcoin' ),
			'type'        => 'number',
			'suffix'      => 'ms',
			'param_name'  => 'auto_play',
			'std'         => 5000,
			'dependency'  => array( 'element' => 'style', 'value_not_equal_to' => 'grid' ),
		),
		Businextcoin_VC::extra_class_field(),
		array(
			'group'      => $slides_tab,
			'heading'    => esc_html__( 'Slides', 'businextcoin' ),
			'type'       => 'param_group',
			'param_name' => 'items',
			'params'     => array(
				array(
					'heading'     => esc_html__( 'Image', 'businextcoin' ),
					'type'        => 'attach_image',
					'param_name'  => 'image',
					'admin_label' => true,
				),
				array(
					'heading'     => esc_html__( 'Image Hover', 'businextcoin' ),
					'type'        => 'attach_image',
					'param_name'  => 'image_hover',
					'admin_label' => true,
				),
				array(
					'heading'    => esc_html__( 'Link', 'businextcoin' ),
					'type'       => 'vc_link',
					'param_name' => 'link',
					'value'      => esc_html__( 'Link', 'businextcoin' ),
				),
			),
		),
	), Businextcoin_VC::get_vc_spacing_tab() ),
) );
