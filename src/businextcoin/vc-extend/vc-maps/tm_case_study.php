<?php

add_filter( 'vc_autocomplete_tm_case_study_taxonomies_callback', array(
	'WPBakeryShortCode_TM_Case_Study',
	'autocomplete_taxonomies_field_search',
), 10, 1 );

add_filter( 'vc_autocomplete_tm_case_study_taxonomies_render', array(
	Businextcoin_VC::instance(),
	'autocomplete_taxonomies_field_render',
), 10, 1 );

class WPBakeryShortCode_TM_Case_Study extends WPBakeryShortCode {

	/**
	 * @param $search_string
	 *
	 * @return array|bool
	 */
	function autocomplete_taxonomies_field_search( $search_string ) {
		$data = Businextcoin_VC::instance()->autocomplete_get_data_from_post_type( $search_string, 'case_study' );

		return $data;
	}

	public function get_inline_css( $selector = '', $atts ) {
		Businextcoin_VC::get_grid_css( $selector, $atts );

		Businextcoin_VC::get_vc_spacing_css( $selector, $atts );
	}
}

vc_map( array(
	'name'     => esc_html__( 'Case Study', 'businextcoin' ),
	'base'     => 'tm_case_study',
	'category' => BUSINEXTCOIN_VC_SHORTCODE_CATEGORY,
	'icon'     => 'insight-i insight-i-portfoliogrid',
	'params'   => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'businextcoin' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Grid', 'businextcoin' )         => 'grid',
				esc_html__( 'Grid Caption', 'businextcoin' ) => 'grid-caption',
				esc_html__( 'Modern List', 'businextcoin' )  => 'modern-list',
			),
			'std'         => 'grid',
		),
		array(
			'heading'     => esc_html__( 'Columns', 'businextcoin' ),
			'type'        => 'number_responsive',
			'param_name'  => 'columns',
			'min'         => 1,
			'max'         => 6,
			'step'        => 1,
			'suffix'      => '',
			'media_query' => array(
				'lg' => '3',
				'md' => '',
				'sm' => '2',
				'xs' => '1',
			),
			'dependency'  => array(
				'element' => 'style',
				'value'   => array(
					'grid',
					'grid-caption',
				),
			),
		),
		array(
			'heading'     => esc_html__( 'Columns Gutter', 'businextcoin' ),
			'description' => esc_html__( 'Controls the gutter of grid columns.', 'businextcoin' ),
			'type'        => 'number',
			'param_name'  => 'gutter',
			'std'         => 30,
			'min'         => 0,
			'max'         => 100,
			'step'        => 1,
			'suffix'      => 'px',
		),
		array(
			'heading'     => esc_html__( 'Rows Gutter', 'businextcoin' ),
			'description' => esc_html__( 'Controls the gutter of grid rows.', 'businextcoin' ),
			'type'        => 'number',
			'param_name'  => 'row_gutter',
			'std'         => 30,
			'min'         => 0,
			'max'         => 100,
			'step'        => 1,
			'suffix'      => 'px',
		),
		array(
			'heading'    => esc_html__( 'Image Size', 'businextcoin' ),
			'type'       => 'dropdown',
			'param_name' => 'image_size',
			'value'      => array(
				esc_html__( '480x480', 'businextcoin' ) => '480x480',
				esc_html__( '370x250', 'businextcoin' ) => '370x250',
				esc_html__( '570x385', 'businextcoin' ) => '570x385',
			),
			'std'        => '480x480',
			'dependency' => array(
				'element' => 'style',
				'value'   => array(
					'grid',
					'grid-caption',
				),
			),
		),
		Businextcoin_VC::get_animation_field( array(
			'std'        => 'move-up',
			'dependency' => array(
				'element' => 'style',
				'value'   => array(
					'grid',
					'grid-caption',
				),
			),
		) ),
		Businextcoin_VC::extra_class_field(),
		array(
			'group'      => esc_html__( 'Data Settings', 'businextcoin' ),
			'type'       => 'hidden',
			'param_name' => 'main_query',
			'std'        => '',
		),
		array(
			'group'       => esc_html__( 'Data Settings', 'businextcoin' ),
			'heading'     => esc_html__( 'Items per page', 'businextcoin' ),
			'description' => esc_html__( 'Number of items to show per page.', 'businextcoin' ),
			'type'        => 'number',
			'param_name'  => 'number',
			'std'         => 9,
			'min'         => 1,
			'max'         => 100,
			'step'        => 1,
		),
		array(
			'group'              => esc_html__( 'Data Settings', 'businextcoin' ),
			'heading'            => esc_html__( 'Narrow data source', 'businextcoin' ),
			'description'        => esc_html__( 'Enter categories, tags or custom taxonomies.', 'businextcoin' ),
			'type'               => 'autocomplete',
			'param_name'         => 'taxonomies',
			'settings'           => array(
				'multiple'       => true,
				'min_length'     => 1,
				'groups'         => true,
				// In UI show results grouped by groups, default false.
				'unique_values'  => true,
				// In UI show results except selected. NB! You should manually check values in backend, default false.
				'display_inline' => true,
				// In UI show results inline view, default false (each value in own line).
				'delay'          => 500,
				// delay for search. default 500.
				'auto_focus'     => true,
				// auto focus input, default true.
			),
			'param_holder_class' => 'vc_not-for-custom',
		),
		array(
			'group'       => esc_html__( 'Data Settings', 'businextcoin' ),
			'heading'     => esc_html__( 'Order by', 'businextcoin' ),
			'type'        => 'dropdown',
			'param_name'  => 'orderby',
			'value'       => array(
				esc_html__( 'Date', 'businextcoin' )                  => 'date',
				esc_html__( 'Post ID', 'businextcoin' )               => 'ID',
				esc_html__( 'Author', 'businextcoin' )                => 'author',
				esc_html__( 'Title', 'businextcoin' )                 => 'title',
				esc_html__( 'Last modified date', 'businextcoin' )    => 'modified',
				esc_html__( 'Post/page parent ID', 'businextcoin' )   => 'parent',
				esc_html__( 'Number of comments', 'businextcoin' )    => 'comment_count',
				esc_html__( 'Menu order/Page Order', 'businextcoin' ) => 'menu_order',
				esc_html__( 'Meta value', 'businextcoin' )            => 'meta_value',
				esc_html__( 'Meta value number', 'businextcoin' )     => 'meta_value_num',
				esc_html__( 'Random order', 'businextcoin' )          => 'rand',
			),
			'description' => esc_html__( 'Select order type. If "Meta value" or "Meta value Number" is chosen then meta key is required.', 'businextcoin' ),
			'std'         => 'date',
		),
		array(
			'group'       => esc_html__( 'Data Settings', 'businextcoin' ),
			'heading'     => esc_html__( 'Sort order', 'businextcoin' ),
			'type'        => 'dropdown',
			'param_name'  => 'order',
			'value'       => array(
				esc_html__( 'Descending', 'businextcoin' ) => 'DESC',
				esc_html__( 'Ascending', 'businextcoin' )  => 'ASC',
			),
			'description' => esc_html__( 'Select sorting order.', 'businextcoin' ),
			'std'         => 'DESC',
		),
		array(
			'group'       => esc_html__( 'Data Settings', 'businextcoin' ),
			'heading'     => esc_html__( 'Meta key', 'businextcoin' ),
			'description' => esc_html__( 'Input meta key for grid ordering.', 'businextcoin' ),
			'type'        => 'textfield',
			'param_name'  => 'meta_key',
			'dependency'  => array(
				'element' => 'orderby',
				'value'   => array(
					'meta_value',
					'meta_value_num',
				),
			),
		),
	), Businextcoin_VC::get_grid_filter_fields(), Businextcoin_VC::get_grid_pagination_fields(), Businextcoin_VC::get_vc_spacing_tab(), Businextcoin_VC::get_custom_style_tab() ),
) );

