<?php

class WPBakeryShortCode_TM_Social_Networks extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		global $businextcoin_shortcode_lg_css;
		global $businextcoin_shortcode_md_css;
		global $businextcoin_shortcode_sm_css;
		global $businextcoin_shortcode_xs_css;

		$tmp = $link_css = $link_hover_css = $icon_css = $icon_hover_css = $text_css = $text_hover_css = '';
		extract( $atts );

		$icon_css       .= Businextcoin_Helper::get_shortcode_css_color_inherit( 'color', $atts['icon_color'], $atts['custom_icon_color'] );
		$icon_hover_css .= Businextcoin_Helper::get_shortcode_css_color_inherit( 'color', $atts['icon_hover_color'], $atts['custom_icon_hover_color'] );
		$text_css       .= Businextcoin_Helper::get_shortcode_css_color_inherit( 'color', $atts['text_color'], $atts['custom_text_color'] );
		$text_hover_css .= Businextcoin_Helper::get_shortcode_css_color_inherit( 'color', $atts['text_hover_color'], $atts['custom_text_hover_color'] );
		$link_css       .= Businextcoin_Helper::get_shortcode_css_color_inherit( 'border-color', $atts['border_color'], $atts['custom_border_color'] );
		$link_hover_css .= Businextcoin_Helper::get_shortcode_css_color_inherit( 'border-color', $atts['border_hover_color'], $atts['custom_border_hover_color'] );
		$link_css       .= Businextcoin_Helper::get_shortcode_css_color_inherit( 'background-color', $atts['background_color'], $atts['custom_background_color'] );
		$link_hover_css .= Businextcoin_Helper::get_shortcode_css_color_inherit( 'background-color', $atts['background_hover_color'], $atts['custom_background_hover_color'] );

		if ( $atts['align'] !== '' ) {
			$tmp .= "text-align: {$atts['align']};";
		}

		if ( $atts['md_align'] !== '' ) {
			$businextcoin_shortcode_md_css .= "$selector { text-align: {$atts['md_align']} }";
		}

		if ( $atts['sm_align'] !== '' ) {
			$businextcoin_shortcode_sm_css .= "$selector { text-align: {$atts['sm_align']} }";
		}

		if ( $atts['xs_align'] !== '' ) {
			$businextcoin_shortcode_xs_css .= "$selector { text-align: {$atts['xs_align']} }";
		}

		if ( $tmp !== '' ) {
			$businextcoin_shortcode_lg_css .= "$selector { $tmp }";
		}

		if ( $icon_css !== '' ) {
			$businextcoin_shortcode_lg_css .= "$selector .link-icon { $icon_css }";
		}

		if ( $icon_hover_css !== '' ) {
			$businextcoin_shortcode_lg_css .= "$selector .item:hover .link-icon { $icon_hover_css }";
		}

		if ( $text_css !== '' ) {
			$businextcoin_shortcode_lg_css .= "$selector .link-text { $text_css }";
		}

		if ( $text_hover_css !== '' ) {
			$businextcoin_shortcode_lg_css .= "$selector .item:hover .link-text { $text_hover_css }";
		}

		if ( $link_css !== '' ) {
			$businextcoin_shortcode_lg_css .= "$selector .link { $link_css }";
		}

		if ( $link_hover_css !== '' ) {
			$businextcoin_shortcode_lg_css .= "$selector .item:hover .link { $link_hover_css }";
		}
	}
}

$styling_tab = esc_html__( 'Styling', 'businextcoin' );

vc_map( array(
	'name'                      => esc_html__( 'Social Networks', 'businextcoin' ),
	'base'                      => 'tm_social_networks',
	'category'                  => BUSINEXTCOIN_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-social-networks',
	'allowed_container_element' => 'vc_row',
	'params'                    => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'businextcoin' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Icons', 'businextcoin' )              => 'icons',
				esc_html__( 'Large Icons', 'businextcoin' )        => 'large-icons',
				esc_html__( 'Extra Large Icons', 'businextcoin' )  => 'extra-large-icons',
				esc_html__( 'Solid Square Icon', 'businextcoin' )  => 'solid-square-icon',
				esc_html__( 'Solid Rounded Icon', 'businextcoin' ) => 'solid-rounded-icon',
				esc_html__( 'Title', 'businextcoin' )              => 'title',
				esc_html__( 'Icon + Title', 'businextcoin' )       => 'icon-title',
			),
			'std'         => 'icons',
		),
		array(
			'heading'     => esc_html__( 'Layout', 'businextcoin' ),
			'type'        => 'dropdown',
			'param_name'  => 'layout',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Inline', 'businextcoin' )    => 'inline',
				esc_html__( 'List', 'businextcoin' )      => 'list',
				esc_html__( '2 Columns', 'businextcoin' ) => 'two-columns',
			),
			'std'         => 'inline',
		),
	), Businextcoin_VC::get_alignment_fields(), array(
		array(
			'heading'    => esc_html__( 'Open link in a new tab.', 'businextcoin' ),
			'type'       => 'checkbox',
			'param_name' => 'target',
			'value'      => array(
				esc_html__( 'Yes', 'businextcoin' ) => '1',
			),
			'std'        => '1',
		),
		array(
			'heading'    => esc_html__( 'Show tooltip as item title.', 'businextcoin' ),
			'type'       => 'checkbox',
			'param_name' => 'tooltip_enable',
			'value'      => array(
				esc_html__( 'Yes', 'businextcoin' ) => '1',
			),
		),
		array(
			'heading'    => esc_html__( 'Tooltip Skin', 'businextcoin' ),
			'type'       => 'dropdown',
			'param_name' => 'tooltip_skin',
			'value'      => Businextcoin_VC::get_tooltip_skin_list(),
			'std'        => '',
			'dependency' => array(
				'element' => 'tooltip_enable',
				'value'   => '1',
			),
		),
		array(
			'heading'    => esc_html__( 'Tooltip Position', 'businextcoin' ),
			'type'       => 'dropdown',
			'param_name' => 'tooltip_position',
			'value'      => array(
				esc_html__( 'Top', 'businextcoin' )    => 'top',
				esc_html__( 'Right', 'businextcoin' )  => 'right',
				esc_html__( 'Bottom', 'businextcoin' ) => 'bottom',
				esc_html__( 'Left', 'businextcoin' )   => 'left',
			),
			'std'        => 'top',
			'dependency' => array(
				'element' => 'tooltip_enable',
				'value'   => '1',
			),
		),
		Businextcoin_VC::extra_class_field(),
		array(
			'group'      => esc_html__( 'Items', 'businextcoin' ),
			'heading'    => esc_html__( 'Items', 'businextcoin' ),
			'type'       => 'param_group',
			'param_name' => 'items',
			'params'     => array(
				array(
					'heading'     => esc_html__( 'Title', 'businextcoin' ),
					'type'        => 'textfield',
					'param_name'  => 'title',
					'admin_label' => true,
				),
				array(
					'heading'    => esc_html__( 'Link', 'businextcoin' ),
					'type'       => 'textfield',
					'param_name' => 'link',
				),
				array(
					'type'        => 'iconpicker',
					'heading'     => esc_html__( 'Icon', 'businextcoin' ),
					'param_name'  => 'icon_ion',
					'value'       => 'ion-social-facebook',
					'settings'    => array(
						'emptyIcon'    => true,
						'type'         => 'ion',
						'iconsPerPage' => 400,
					),
					'description' => esc_html__( 'Select icon from library.', 'businextcoin' ),
				),
			),
			'value'      => rawurlencode( wp_json_encode( array(
				array(
					'title'    => esc_html__( 'Facebook', 'businextcoin' ),
					'link'     => '#',
					'icon_ion' => 'ion-social-facebook',
				),
				array(
					'title'    => esc_html__( 'Twitter', 'businextcoin' ),
					'link'     => '#',
					'icon_ion' => 'ion-social-twitter',
				),

				array(
					'title'    => esc_html__( 'Vimeo', 'businextcoin' ),
					'link'     => '#',
					'icon_ion' => 'ion-social-vimeo',
				),
				array(
					'title'    => esc_html__( 'Linkedin', 'businextcoin' ),
					'link'     => '#',
					'icon_ion' => 'ion-social-linkedin',
				),
			) ) ),

		),
		array(
			'group'            => $styling_tab,
			'heading'          => esc_html__( 'Icon Color', 'businextcoin' ),
			'type'             => 'dropdown',
			'param_name'       => 'icon_color',
			'value'            => array(
				esc_html__( 'Default Color', 'businextcoin' )   => '',
				esc_html__( 'Primary Color', 'businextcoin' )   => 'primary',
				esc_html__( 'Secondary Color', 'businextcoin' ) => 'secondary',
				esc_html__( 'Custom Color', 'businextcoin' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $styling_tab,
			'heading'          => esc_html__( 'Custom Icon Color', 'businextcoin' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_icon_color',
			'dependency'       => array(
				'element' => 'icon_color',
				'value'   => 'custom',
			),
			'std'              => '#fff',
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $styling_tab,
			'heading'          => esc_html__( 'Icon Hover Color', 'businextcoin' ),
			'type'             => 'dropdown',
			'param_name'       => 'icon_hover_color',
			'value'            => array(
				esc_html__( 'Default Color', 'businextcoin' )   => '',
				esc_html__( 'Primary Color', 'businextcoin' )   => 'primary',
				esc_html__( 'Secondary Color', 'businextcoin' ) => 'secondary',
				esc_html__( 'Custom Color', 'businextcoin' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $styling_tab,
			'heading'          => esc_html__( 'Custom Icon Hover Color', 'businextcoin' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_icon_hover_color',
			'dependency'       => array(
				'element' => 'icon_hover_color',
				'value'   => 'custom',
			),
			'std'              => '#fff',
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $styling_tab,
			'heading'          => esc_html__( 'Text Color', 'businextcoin' ),
			'type'             => 'dropdown',
			'param_name'       => 'text_color',
			'value'            => array(
				esc_html__( 'Default Color', 'businextcoin' )   => '',
				esc_html__( 'Primary Color', 'businextcoin' )   => 'primary',
				esc_html__( 'Secondary Color', 'businextcoin' ) => 'secondary',
				esc_html__( 'Custom Color', 'businextcoin' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $styling_tab,
			'heading'          => esc_html__( 'Custom Text Color', 'businextcoin' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_text_color',
			'dependency'       => array(
				'element' => 'text_color',
				'value'   => array( 'custom' ),
			),
			'std'              => '#fff',
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $styling_tab,
			'heading'          => esc_html__( 'Text Hover Color', 'businextcoin' ),
			'type'             => 'dropdown',
			'param_name'       => 'text_hover_color',
			'value'            => array(
				esc_html__( 'Default Color', 'businextcoin' )   => '',
				esc_html__( 'Primary Color', 'businextcoin' )   => 'primary',
				esc_html__( 'Secondary Color', 'businextcoin' ) => 'secondary',
				esc_html__( 'Custom Color', 'businextcoin' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $styling_tab,
			'heading'          => esc_html__( 'Custom Text Hover Color', 'businextcoin' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_text_hover_color',
			'dependency'       => array(
				'element' => 'text_hover_color',
				'value'   => array( 'custom' ),
			),
			'std'              => '#fff',
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $styling_tab,
			'heading'          => esc_html__( 'Border Color', 'businextcoin' ),
			'type'             => 'dropdown',
			'param_name'       => 'border_color',
			'value'            => array(
				esc_html__( 'Default Color', 'businextcoin' )   => '',
				esc_html__( 'Primary Color', 'businextcoin' )   => 'primary',
				esc_html__( 'Secondary Color', 'businextcoin' ) => 'secondary',
				esc_html__( 'Custom Color', 'businextcoin' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $styling_tab,
			'heading'          => esc_html__( 'Custom Border Color', 'businextcoin' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_border_color',
			'dependency'       => array(
				'element' => 'border_color',
				'value'   => array( 'custom' ),
			),
			'std'              => '#fff',
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $styling_tab,
			'heading'          => esc_html__( 'Border Hover Color', 'businextcoin' ),
			'type'             => 'dropdown',
			'param_name'       => 'border_hover_color',
			'value'            => array(
				esc_html__( 'Default Color', 'businextcoin' )   => '',
				esc_html__( 'Primary Color', 'businextcoin' )   => 'primary',
				esc_html__( 'Secondary Color', 'businextcoin' ) => 'secondary',
				esc_html__( 'Custom Color', 'businextcoin' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $styling_tab,
			'heading'          => esc_html__( 'Custom Border Hover Color', 'businextcoin' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_border_hover_color',
			'dependency'       => array(
				'element' => 'border_hover_color',
				'value'   => array( 'custom' ),
			),
			'std'              => '#fff',
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $styling_tab,
			'heading'          => esc_html__( 'Background Color', 'businextcoin' ),
			'type'             => 'dropdown',
			'param_name'       => 'background_color',
			'value'            => array(
				esc_html__( 'Default Color', 'businextcoin' )   => '',
				esc_html__( 'Primary Color', 'businextcoin' )   => 'primary',
				esc_html__( 'Secondary Color', 'businextcoin' ) => 'secondary',
				esc_html__( 'Custom Color', 'businextcoin' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $styling_tab,
			'heading'          => esc_html__( 'Custom Background Color', 'businextcoin' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_background_color',
			'dependency'       => array(
				'element' => 'background_color',
				'value'   => array( 'custom' ),
			),
			'std'              => '#fff',
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $styling_tab,
			'heading'          => esc_html__( 'Background Hover Color', 'businextcoin' ),
			'type'             => 'dropdown',
			'param_name'       => 'background_hover_color',
			'value'            => array(
				esc_html__( 'Default Color', 'businextcoin' )   => '',
				esc_html__( 'Primary Color', 'businextcoin' )   => 'primary',
				esc_html__( 'Secondary Color', 'businextcoin' ) => 'secondary',
				esc_html__( 'Custom Color', 'businextcoin' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $styling_tab,
			'heading'          => esc_html__( 'Custom Background Hover Color', 'businextcoin' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_background_hover_color',
			'dependency'       => array(
				'element' => 'background_hover_color',
				'value'   => array( 'custom' ),
			),
			'std'              => '#fff',
			'edit_field_class' => 'vc_col-sm-6',
		),
	) ),
) );
