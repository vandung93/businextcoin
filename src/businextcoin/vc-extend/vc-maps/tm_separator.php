<?php

class WPBakeryShortCode_TM_Separator extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		global $businextcoin_shortcode_lg_css;
		global $businextcoin_shortcode_md_css;
		global $businextcoin_shortcode_sm_css;
		global $businextcoin_shortcode_xs_css;
		extract( $atts );

		$wrapper_tmp = '';

		if ( $atts['align'] !== '' ) {
			$wrapper_tmp .= "text-align: {$atts['align']};";
		}

		if ( $atts['md_align'] !== '' ) {
			$businextcoin_shortcode_md_css .= "$selector { text-align: {$atts['md_align']} }";
		}

		if ( $atts['sm_align'] !== '' ) {
			$businextcoin_shortcode_sm_css .= "$selector { text-align: {$atts['sm_align']} }";
		}

		if ( $atts['xs_align'] !== '' ) {
			$businextcoin_shortcode_xs_css .= "$selector { text-align: {$atts['xs_align']} }";
		}

		if ( $wrapper_tmp !== '' ) {
			$businextcoin_shortcode_lg_css .= "$selector { $wrapper_tmp }";
		}

		Businextcoin_VC::get_vc_spacing_css( $selector, $atts );
	}

}

vc_map( array(
	'name'     => esc_html__( 'Separator', 'businextcoin' ),
	'base'     => 'tm_separator',
	'category' => BUSINEXTCOIN_VC_SHORTCODE_CATEGORY,
	'icon'     => 'insight-i insight-i-call-to-action',
	'params'   => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'businextcoin' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Modern Dots', 'businextcoin' )      => 'modern-dots',
				esc_html__( 'Thick Short Line', 'businextcoin' ) => 'thick-short-line',
				esc_html__( 'Single Line', 'businextcoin' )      => 'single-line',
			),
			'std'         => 'thick-short-line',
		),
		array(
			'heading'     => esc_html__( 'Smooth Scroll', 'businextcoin' ),
			'description' => esc_html__( 'Input valid id to smooth scroll to a section on click. ( For Ex: #about-us-section )', 'businextcoin' ),
			'type'        => 'textfield',
			'param_name'  => 'smooth_scroll',
		),
		Businextcoin_VC::extra_class_field(),
	), Businextcoin_VC::get_alignment_fields(), Businextcoin_VC::get_vc_spacing_tab() ),
) );

