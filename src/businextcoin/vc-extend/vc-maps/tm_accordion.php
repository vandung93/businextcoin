<?php

class WPBakeryShortCode_TM_Accordion extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		Businextcoin_VC::get_vc_spacing_css( $selector, $atts );
	}
}

vc_map( array(
	'name'                      => esc_html__( 'Accordion', 'businextcoin' ),
	'base'                      => 'tm_accordion',
	'category'                  => BUSINEXTCOIN_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-accordion',
	'allowed_container_element' => 'vc_row',
	'params'                    => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'businextcoin' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Style 01', 'businextcoin' ) => '01',
				esc_html__( 'Style 02', 'businextcoin' ) => '02',
				esc_html__( 'Style 03', 'businextcoin' ) => '03',
			),
			'std'         => '01',
		),
		array(
			'heading'    => esc_html__( 'Multi Open', 'businextcoin' ),
			'type'       => 'checkbox',
			'param_name' => 'multi_open',
			'value'      => array( esc_html__( 'Yes', 'businextcoin' ) => '1' ),
		),
		array(
			'heading'    => esc_html__( 'Open First Item', 'businextcoin' ),
			'type'       => 'checkbox',
			'param_name' => 'open_first_item',
			'value'      => array( esc_html__( 'Yes', 'businextcoin' ) => '1' ),
		),
		Businextcoin_VC::extra_class_field(),
		array(
			'group'      => esc_html__( 'Items', 'businextcoin' ),
			'heading'    => esc_html__( 'Items', 'businextcoin' ),
			'type'       => 'param_group',
			'param_name' => 'items',
			'params'     => array(
				array(
					'heading'     => esc_html__( 'Title', 'businextcoin' ),
					'type'        => 'textfield',
					'param_name'  => 'title',
					'admin_label' => true,
				),
				array(
					'heading'    => esc_html__( 'Content', 'businextcoin' ),
					'type'       => 'textarea',
					'param_name' => 'content',
				),
			),
		),
	), Businextcoin_VC::get_vc_spacing_tab() ),
) );
