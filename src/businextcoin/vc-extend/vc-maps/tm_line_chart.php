<?php

class WPBakeryShortCode_TM_Line_Chart extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		Businextcoin_VC::get_vc_spacing_css( $selector, $atts );
	}
}

$legend_tab = esc_html__( 'Tooltips and Legends', 'businextcoin' );

vc_map( array(
	'name'                      => esc_html__( 'Line Chart', 'businextcoin' ),
	'base'                      => 'tm_line_chart',
	'category'                  => BUSINEXTCOIN_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-accordion',
	'allowed_container_element' => 'vc_row',
	'params'                    => array_merge( array(
		array(
			'heading'     => esc_html__( 'X axis labels', 'businextcoin' ),
			'description' => esc_html__( 'List of labels for X axis (separate labels with ";").', 'businextcoin' ),
			'type'        => 'textfield',
			'param_name'  => 'labels',
			'std'         => 'Jul; Aug; Sep; Oct; Nov; Dec',
		),
		array(
			'heading'    => esc_html__( 'Datasets', 'businextcoin' ),
			'type'       => 'param_group',
			'param_name' => 'datasets',
			'params'     => array(
				array(
					'heading'     => esc_html__( 'Title', 'businextcoin' ),
					'description' => esc_html__( 'Dataset title used in tooltips and legends.', 'businextcoin' ),
					'type'        => 'textfield',
					'param_name'  => 'title',
					'admin_label' => true,
				),
				array(
					'heading'     => esc_html__( 'Values', 'businextcoin' ),
					'description' => esc_html__( 'text format for the tooltip (available placeholders: {d} dataset title, {x} X axis label, {y} Y axis value)', 'businextcoin' ),
					'type'        => 'textfield',
					'param_name'  => 'values',
				),
				array(
					'heading'    => esc_html__( 'Dataset Color', 'businextcoin' ),
					'type'       => 'colorpicker',
					'param_name' => 'color',
				),
				array(
					'heading'     => esc_html__( 'Area filling', 'businextcoin' ),
					'description' => esc_html__( 'How to fill the area below the line', 'businextcoin' ),
					'type'        => 'dropdown',
					'param_name'  => 'fill',
					'value'       => array(
						esc_html__( 'Custom', 'businextcoin' ) => 'custom',
						esc_html__( 'None', 'businextcoin' )   => 'none',
					),
					'std'         => 'none',
				),
				array(
					'heading'    => esc_html__( 'Fill Color', 'businextcoin' ),
					'type'       => 'colorpicker',
					'param_name' => 'fill_color',
					'dependency' => array(
						'element' => 'fill',
						'value'   => array( 'custom' ),
					),
				),
				array(
					'type'       => 'dropdown',
					'param_name' => 'point_style',
					'heading'    => esc_html__( 'Point Style', 'businextcoin' ),
					'value'      => array(
						esc_html__( 'none', 'businextcoin' )              => 'none',
						esc_html__( 'circle', 'businextcoin' )            => 'circle',
						esc_html__( 'triangle', 'businextcoin' )          => 'triangle',
						esc_html__( 'rectangle', 'businextcoin' )         => 'rect',
						esc_html__( 'rotated rectangle', 'businextcoin' ) => 'rectRot',
						esc_html__( 'cross', 'businextcoin' )             => 'cross',
						esc_html__( 'rotated cross', 'businextcoin' )     => 'crossRot',
						esc_html__( 'star', 'businextcoin' )              => 'star',
					),
					'std'        => 'circle',
				),
				array(
					'type'       => 'dropdown',
					'param_name' => 'line_type',
					'heading'    => esc_html__( 'Line type', 'businextcoin' ),
					'value'      => array(
						esc_html__( 'normal', 'businextcoin' )  => 'normal',
						esc_html__( 'stepped', 'businextcoin' ) => 'step',
					),
					'std'        => 'normal',
				),
				array(
					'type'       => 'dropdown',
					'param_name' => 'line_style',
					'heading'    => esc_html__( 'Line style', 'businextcoin' ),
					'value'      => array(
						esc_html__( 'solid', 'businextcoin' )  => 'solid',
						esc_html__( 'dashed', 'businextcoin' ) => 'dashed',
						esc_html__( 'dotted', 'businextcoin' ) => 'dotted',
					),
					'std'        => 'solid',
				),
				array(
					'heading'     => esc_html__( 'Thickness', 'businextcoin' ),
					'description' => esc_html__( 'line and points thickness', 'businextcoin' ),
					'type'        => 'dropdown',
					'param_name'  => 'thickness',
					'value'       => array(
						esc_html__( 'thin', 'businextcoin' )    => 'thin',
						esc_html__( 'normal', 'businextcoin' )  => 'normal',
						esc_html__( 'thick', 'businextcoin' )   => 'thick',
						esc_html__( 'thicker', 'businextcoin' ) => 'thicker',
					),
					'std'         => 'normal',
				),
				array(
					'heading'     => esc_html__( 'Line tension', 'businextcoin' ),
					'description' => esc_html__( 'tension of the line ( 100 for a straight line )', 'businextcoin' ),
					'type'        => 'number',
					'param_name'  => 'line_tension',
					'std'         => 10,
					'min'         => 0,
					'max'         => 100,
					'step'        => 1,
				),
			),
			'value'      => rawurlencode( wp_json_encode( array(
				array(
					'title'        => esc_html__( 'Item 01', 'businextcoin' ),
					'values'       => '15; 10; 22; 19; 23; 17',
					'color'        => 'rgba(105, 59, 255, 0.55)',
					'fill'         => 'none',
					'thickness'    => 'normal',
					'point_style'  => 'circle',
					'line_style'   => 'solid',
					'line_tension' => 10,

				),
				array(
					'title'        => esc_html__( 'Item 02', 'businextcoin' ),
					'values'       => '34; 38; 35; 33; 37; 40',
					'color'        => 'rgba(0, 110, 253, 0.56)',
					'fill'         => 'none',
					'thickness'    => 'normal',
					'point_style'  => 'circle',
					'line_style'   => 'solid',
					'line_tension' => 10,
				),
			) ) ),
		),
		Businextcoin_VC::extra_class_field(),
		array(
			'group'      => $legend_tab,
			'heading'    => esc_html__( 'Enable legends', 'businextcoin' ),
			'type'       => 'checkbox',
			'param_name' => 'legend',
			'value'      => array(
				esc_html__( 'Yes', 'businextcoin' ) => '1',
			),
			'std'        => '1',
		),
		array(
			'group'      => $legend_tab,
			'heading'    => esc_html__( 'Legends Style', 'businextcoin' ),
			'type'       => 'dropdown',
			'param_name' => 'legend_style',
			'value'      => array(
				esc_html__( 'Normal', 'businextcoin' )          => 'normal',
				esc_html__( 'Use Point Style', 'businextcoin' ) => 'point',
			),
			'std'        => 'normal',
		),
		array(
			'group'      => $legend_tab,
			'heading'    => esc_html__( 'Legends Position', 'businextcoin' ),
			'type'       => 'dropdown',
			'param_name' => 'legend_position',
			'value'      => array(
				esc_html__( 'Top', 'businextcoin' )    => 'top',
				esc_html__( 'Right', 'businextcoin' )  => 'right',
				esc_html__( 'Bottom', 'businextcoin' ) => 'bottom',
				esc_html__( 'Left', 'businextcoin' )   => 'left',
			),
			'std'        => 'bottom',
		),
		array(
			'group'       => $legend_tab,
			'heading'     => esc_html__( 'Click on legends', 'businextcoin' ),
			'description' => esc_html__( 'Hide dataset on click on legend', 'businextcoin' ),
			'type'        => 'checkbox',
			'param_name'  => 'legend_onclick',
			'value'       => array(
				esc_html__( 'Yes', 'businextcoin' ) => '1',
			),
			'std'         => '1',
		),
		array(
			'group'      => esc_html__( 'Chart Options', 'businextcoin' ),
			'heading'    => esc_html__( 'Aspect Ratio', 'businextcoin' ),
			'type'       => 'dropdown',
			'param_name' => 'aspect_ratio',
			'value'      => array(
				'1:1'  => '1:1',
				'21:9' => '21:9',
				'16:9' => '16:9',
				'4:3'  => '4:3',
				'3:4'  => '3:4',
				'9:16' => '9:16',
				'9:21' => '9:21',
			),
			'std'        => '4:3',
		),
	), Businextcoin_VC::get_vc_spacing_tab() ),
) );
