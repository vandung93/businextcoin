<?php

vc_add_params( 'vc_separator', array(
	array(
		'heading'     => esc_html__( 'Position', 'businextcoin' ),
		'description' => esc_html__( 'Make the separator position absolute with column', 'businextcoin' ),
		'type'        => 'dropdown',
		'param_name'  => 'position',
		'value'       => array(
			esc_html__( 'None', 'businextcoin' )   => '',
			esc_html__( 'Top', 'businextcoin' )    => 'top',
			esc_html__( 'Bottom', 'businextcoin' ) => 'bottom',
		),
		'std'         => '',
	),
) );
