<?php

class WPBakeryShortCode_TM_Loan_Form extends WPBakeryShortCode {

	function __construct( $settings ) {
		parent::__construct( $settings );

		global $post;
		if ( is_a( $post, 'WP_Post' ) && has_shortcode( $post->post_content, 'tm_loan_form' ) ) {
			add_action( 'wp_footer', array( $this, 'popup_loan_form_template' ) );
		}
	}

	function popup_loan_form_template() {
		?>
		<div class="form-overdraft">
			<div class="inner">
				<div id="form-overdraft-close" class="form-overdraft-close">
					<span class="ion-close-round"></span>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="overdraft-header">
								<h3 class="overdraft-title"><?php echo esc_html__( 'New Crypto Overdrafts', 'businextcoin' ) . '.'; ?></h3>
								<div class="overdraft-sub-title">
									<?php echo esc_html__( 'Don\'t sell your crypto. Don\'t lose the upside potential.', 'businextcoin' ); ?>
								</div>
							</div>

							<div class="overdraft-body">
								<div class="form-step-section form-step-select-amount">
									<h5 class="form-step-title step-1">
										<span>01</span> <?php echo esc_html__( 'Choose Overdraft Amount', 'businextcoin' ); ?>
									</h5>

									<div class="form-step-content">
										<div class="left-column">
											<h5 class="form-label"><?php esc_html_e( 'Select Change Unit', 'businextcoin' ); ?></h5>
											<div class="unit-choices">
												<a href="javascript:void(0)" id="bitcoin-unit"
												   class="btn-unit selected" data-value="4663.07">
													<img
														src="<?php echo BUSINEXTCOIN_THEME_IMAGE_URI . '/bitcoin-icon.png'; ?>"
														alt="<?php esc_attr_e( 'Bitcoin', 'businextcoin' ); ?>">
													<span><?php esc_html_e( 'Bitcoin', 'businextcoin' ); ?></span>
												</a>
												<a href="javascript:void(0)" id="ethereum-unit" class="btn-unit"
												   data-value="364.89">
													<img
														src="<?php echo BUSINEXTCOIN_THEME_IMAGE_URI . '/ethereum-icon.png'; ?>"
														alt="<?php esc_attr_e( 'Bitcoin', 'businextcoin' ); ?>">
													<span><?php esc_html_e( 'Ethereum', 'businextcoin' ); ?></span>
												</a>
												<a href="javascript:void(0)" id="ripple-unit" class="btn-unit"
												   data-value="3146.12">
													<img
														src="<?php echo BUSINEXTCOIN_THEME_IMAGE_URI . '/ripple-icon.png'; ?>"
														alt="<?php esc_attr_e( 'Bitcoin', 'businextcoin' ); ?>">
													<span><?php esc_html_e( 'Ripple', 'businextcoin' ); ?></span>
													<!--<radio name="init" value="r"></radio>-->
												</a>
											</div>
										</div>
										<div class="right-column">
											<div class="form-wrap control-amount">
												<h5 class="form-label"><?php esc_html_e( 'Overdraft Amount', 'businextcoin' ); ?></h5>
												<div class="form-control">
													<input type="text" id="overdraft-amount" class="overdraft"
													       name="form-loanAmount"/>
												</div>
											</div>
											<div class="form-wrap control-wallet">
												<h5 class="form-label"><?php esc_html_e( 'Minimum Wallet Balance', 'businextcoin' ); ?></h5>
												<div class="form-control">
													<input type="text" id="wallet-balance" class="wallet-balance"
													       name="form-loanCollateral"/>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="form-step-section form-step-select-duration">
									<h5 class="form-step-title step-1">
										<span>02</span> <?php echo esc_html__( 'Select Duration', 'businextcoin' ); ?>
									</h5>

									<div class="form-step-content">
										<div class="slider-wrap">
											<div id="slider">
												<div id="custom-handle" class="ui-slider-handle"></div>
											</div>
											<div class="slider-labels">
												<span
													class="slider-label-min"><?php esc_html_e( '01 month', 'businextcoin' ); ?></span>
												<span
													class="slider-label-max"><?php esc_html_e( '24 months', 'businextcoin' ); ?></span>
											</div>
										</div>
										<div class="total-info">
											<h6 class="total-label"><?php esc_html_e( 'Total Repayments', 'businextcoin' ); ?></h6>
											<div class="sub-total">
													<span id="loan-cost" class="cost secondary-color">
													<?php esc_html_e( '$12,305.6', 'businextcoin' ) ?>
												</span>
												<h6 class="per-month">
													<?php esc_html_e( '12% APR', 'businextcoin' ); ?>
												</h6>
											</div>
										</div>
									</div>
								</div>

								<div class="form-step-section form-step-payment-method">
									<h5 class="form-step-title step-1">
										<span>03</span> <?php echo esc_html__( 'Payout Method', 'businextcoin' ); ?>
									</h5>

									<div class="form-step-content">
										<h6 class="form-label"><?php esc_html_e( 'Bank Transfer', 'businextcoin' ); ?></h6>
										<div class="form-description">
											<?php esc_html_e( 'to any personal or business bank account', 'businextcoin' ); ?>
										</div>

										<div class="payment-method-list">
											<div class="payment-item">
												<a href="javascript:void(0);">
													<div><img
															src="<?php echo BUSINEXTCOIN_THEME_IMAGE_URI . '/swift-payment.png'; ?>"
															alt="<?php esc_attr_e( 'Swift', 'businextcoin' ); ?>"/></div>
													<div><?php esc_html_e( '1-3 days', 'businextcoin' ) ?></div>
												</a>
											</div>
											<div class="payment-item">
												<a href="javascript:void(0);">
													<div><img
															src="<?php echo BUSINEXTCOIN_THEME_IMAGE_URI . '/sepa-payment.png'; ?>"
															alt="<?php esc_attr_e( 'Sepa', 'businextcoin' ); ?>"/></div>
													<div><?php esc_html_e( 'Same/Next Day', 'businextcoin' ) ?></div>
												</a>
											</div>
											<div class="payment-item">
												<a href="javascript:void(0);">
													<div><img
															src="<?php echo BUSINEXTCOIN_THEME_IMAGE_URI . '/ach-payment.png'; ?>"
															alt="<?php esc_attr_e( 'ACH', 'businextcoin' ); ?>"/></div>
													<div><?php esc_html_e( 'Same/Next Day', 'businextcoin' ) ?></div>
												</a>
											</div>
										</div>

										<div class="form-submit">
											<button><?php esc_html_e( 'Make bank transfer', 'businextcoin' ) ?></button>
										</div>
									</div>
								</div>
							</div>

							<div class="overdraft-footer">
								<h4 class="overdraft-footer-title"><?php echo wp_kses( __( 'Thank you for your interest! <br/> We are launching the <span class="secondary-color">BusinextCoin</span> soon !', 'businextcoin' ), 'businextcoin-default' ); ?></h4>
								<div class="subscribe-form">
									<h6><?php esc_html_e( 'Sign Up for Updates', 'businextcoin' ); ?></h6>
									<?php echo do_shortcode( '[tm_mailchimp_form]' ); ?>
								</div>

								<a class="tm-button style-border-icon has-icon icon-right join-telegram-btn"
								   href="#">
									<span class="button-text"
									      data-text="<?php esc_attr_e( 'Join Telegram', 'businextcoin' ); ?>">
										<?php esc_html_e( 'Join Telegram', 'businextcoin' ); ?>
									</span>
									<span class="button-icon">
										<i class="ion-paper-airplane"></i>
									</span>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
	}
}

vc_map( array(
	'name'                      => esc_html__( 'Loan Form', 'businextcoin' ),
	'base'                      => 'tm_loan_form',
	'category'                  => BUSINEXTCOIN_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-contact-form-7',
	'allowed_container_element' => 'vc_row',
	'params'                    => array(
		array(
			'heading'     => esc_html__( 'Style', 'businextcoin' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( '01', 'businextcoin' ) => '01',
				esc_html__( '02', 'businextcoin' ) => '02',
			),
			'std'         => '01',
		),
		Businextcoin_VC::extra_class_field(),
	),
) );
