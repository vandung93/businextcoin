<?php

vc_add_params( 'vc_widget_sidebar', array(
	array(
		'heading'    => esc_html__( 'Sidebar Position', 'businextcoin' ),
		'type'       => 'dropdown',
		'param_name' => 'sidebar_position',
		'value'      => array(
			esc_html__( 'Left', 'businextcoin' )  => 'left',
			esc_html__( 'Right', 'businextcoin' ) => 'right',
		),
		'std'        => 'right',
	),
) );
