<?php

class WPBakeryShortCode_TM_List extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		global $businextcoin_shortcode_lg_css;
		global $businextcoin_shortcode_md_css;
		global $businextcoin_shortcode_sm_css;
		global $businextcoin_shortcode_xs_css;

		$marker_tmp  = Businextcoin_Helper::get_shortcode_css_color_inherit( 'color', $atts['marker_color'], $atts['custom_marker_color'] );
		$marker_tmp  .= Businextcoin_Helper::get_shortcode_css_color_inherit( 'background-color', $atts['marker_background_color'], $atts['custom_marker_background_color'] );
		$heading_tmp = Businextcoin_Helper::get_shortcode_css_color_inherit( 'color', $atts['title_color'], $atts['custom_title_color'] );
		$text_tmp    = Businextcoin_Helper::get_shortcode_css_color_inherit( 'color', $atts['desc_color'], $atts['custom_desc_color'] );

		if ( $marker_tmp !== '' ) {
			$businextcoin_shortcode_lg_css .= "$selector .marker { $marker_tmp }";
		}

		if ( $heading_tmp !== '' ) {
			$businextcoin_shortcode_lg_css .= "$selector .title { $heading_tmp }";
		}

		if ( $text_tmp !== '' ) {
			$businextcoin_shortcode_lg_css .= "$selector .desc { $text_tmp }";
		}

		if ( isset( $atts['columns'] ) && $atts['columns'] !== '' ) {
			$arr = explode( ';', $atts['columns'] );
			foreach ( $arr as $value ) {
				$tmp = explode( ':', $value );

				switch ( $tmp[0] ) {
					case 'sm' :
						$businextcoin_shortcode_sm_css .= "$selector { grid-template-columns: repeat({$tmp[1]}, 1fr); }";
						break;
					case 'md' :
						$businextcoin_shortcode_md_css .= "$selector { grid-template-columns: repeat({$tmp[1]}, 1fr); }";
						break;
					case 'xs' :
						$businextcoin_shortcode_xs_css .= "$selector { grid-template-columns: repeat({$tmp[1]}, 1fr); }";
						break;
					case 'lg' :
						$businextcoin_shortcode_lg_css .= "$selector { grid-template-columns: repeat({$tmp[1]}, 1fr); }";
						break;
				}
			}
		}

		Businextcoin_VC::get_responsive_css( array(
			'element' => "$selector .title",
			'atts'    => array(
				'font-size' => array(
					'media_str' => $atts['heading_font_size'],
					'unit'      => 'px',
				),
			),
		) );

		Businextcoin_VC::get_vc_spacing_css( $selector, $atts );
	}
}

$styling_tab = esc_html__( 'Styling', 'businextcoin' );

vc_map( array(
	'name'                      => esc_html__( 'List', 'businextcoin' ),
	'base'                      => 'tm_list',
	'category'                  => BUSINEXTCOIN_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-list',
	'allowed_container_element' => 'vc_row',
	'params'                    => array_merge( array(
		array(
			'heading'     => esc_html__( 'Widget title', 'businextcoin' ),
			'description' => esc_html__( 'What text use as a widget title.', 'businextcoin' ),
			'type'        => 'textfield',
			'param_name'  => 'widget_title',
		),
		array(
			'heading'     => esc_html__( 'List Style', 'businextcoin' ),
			'type'        => 'dropdown',
			'param_name'  => 'list_style',
			'value'       => array(
				esc_html__( 'Circle List', 'businextcoin' )               => 'circle',
				esc_html__( 'Circle List 02', 'businextcoin' )            => 'circle-02',
				esc_html__( 'Check List', 'businextcoin' )                => 'check',
				esc_html__( 'Check List 02', 'businextcoin' )             => 'check-02',
				esc_html__( 'Icon List', 'businextcoin' )                 => 'icon',
				esc_html__( 'Icon List 02', 'businextcoin' )              => 'icon-02',
				esc_html__( 'Modern Icon List', 'businextcoin' )          => 'modern-icon',
				esc_html__( '(Automatic) Numbered List', 'businextcoin' ) => 'auto-numbered',
				esc_html__( '(Manual) Numbered List', 'businextcoin' )    => 'manual-numbered',
			),
			'admin_label' => true,
			'std'         => 'icon',
		),
		array(
			'heading'     => esc_html__( 'Columns', 'businextcoin' ),
			'type'        => 'number_responsive',
			'param_name'  => 'columns',
			'min'         => 1,
			'max'         => 10,
			'suffix'      => 'item (s)',
			'media_query' => array(
				'lg' => 1,
				'md' => '',
				'sm' => '',
				'xs' => 1,
			),
		),
		array(
			'group'            => $styling_tab,
			'heading'          => esc_html__( 'Marker Color', 'businextcoin' ),
			'type'             => 'dropdown',
			'param_name'       => 'marker_color',
			'value'            => array(
				esc_html__( 'Default Color', 'businextcoin' )   => '',
				esc_html__( 'Primary Color', 'businextcoin' )   => 'primary',
				esc_html__( 'Secondary Color', 'businextcoin' ) => 'secondary',
				esc_html__( 'Custom Color', 'businextcoin' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $styling_tab,
			'heading'          => esc_html__( 'Custom Marker Color', 'businextcoin' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_marker_color',
			'dependency'       => array(
				'element' => 'marker_color',
				'value'   => array( 'custom' ),
			),
			'std'              => '#fff',
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $styling_tab,
			'heading'          => esc_html__( 'Marker Background Color', 'businextcoin' ),
			'type'             => 'dropdown',
			'param_name'       => 'marker_background_color',
			'value'            => array(
				esc_html__( 'Default Color', 'businextcoin' )   => '',
				esc_html__( 'Primary Color', 'businextcoin' )   => 'primary',
				esc_html__( 'Secondary Color', 'businextcoin' ) => 'secondary',
				esc_html__( 'Custom Color', 'businextcoin' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $styling_tab,
			'heading'          => esc_html__( 'Custom Marker Background Color', 'businextcoin' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_marker_background_color',
			'dependency'       => array(
				'element' => 'marker_background_color',
				'value'   => array( 'custom' ),
			),
			'std'              => '#fff',
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $styling_tab,
			'heading'          => esc_html__( 'Title Color', 'businextcoin' ),
			'type'             => 'dropdown',
			'param_name'       => 'title_color',
			'value'            => array(
				esc_html__( 'Default Color', 'businextcoin' )   => '',
				esc_html__( 'Primary Color', 'businextcoin' )   => 'primary',
				esc_html__( 'Secondary Color', 'businextcoin' ) => 'secondary',
				esc_html__( 'Custom Color', 'businextcoin' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $styling_tab,
			'heading'          => esc_html__( 'Custom Title Color', 'businextcoin' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_title_color',
			'dependency'       => array(
				'element' => 'title_color',
				'value'   => array( 'custom' ),
			),
			'std'              => '#fff',
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $styling_tab,
			'heading'          => esc_html__( 'Description Color', 'businextcoin' ),
			'type'             => 'dropdown',
			'param_name'       => 'desc_color',
			'value'            => array(
				esc_html__( 'Default Color', 'businextcoin' )   => '',
				esc_html__( 'Primary Color', 'businextcoin' )   => 'primary',
				esc_html__( 'Secondary Color', 'businextcoin' ) => 'secondary',
				esc_html__( 'Custom Color', 'businextcoin' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $styling_tab,
			'heading'          => esc_html__( 'Custom Description Color', 'businextcoin' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_desc_color',
			'dependency'       => array(
				'element' => 'desc_color',
				'value'   => array( 'custom' ),
			),
			'std'              => '#fff',
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'       => $styling_tab,
			'heading'     => esc_html__( 'Heading Font Size', 'businextcoin' ),
			'type'        => 'number_responsive',
			'param_name'  => 'heading_font_size',
			'min'         => 8,
			'suffix'      => 'px',
			'media_query' => array(
				'lg' => '',
				'md' => '',
				'sm' => '',
				'xs' => '',
			),
		),
	),

		Businextcoin_VC::icon_libraries( array(
			'allow_none' => true,
			'group'      => '',
			'dependency' => array(
				'element' => 'list_style',
				'value'   => array(
					'icon',
					'icon-02',
					'modern-icon',
				),
			),
		) ), array(
			Businextcoin_VC::get_animation_field(),
			Businextcoin_VC::extra_class_field(),
			array(
				'group'      => esc_html__( 'Items', 'businextcoin' ),
				'heading'    => esc_html__( 'Items', 'businextcoin' ),
				'type'       => 'param_group',
				'param_name' => 'items',
				'params'     => array(
					array(
						'heading'     => esc_html__( 'Number', 'businextcoin' ),
						'type'        => 'textfield',
						'param_name'  => 'item_number',
						'admin_label' => true,
						'description' => esc_html__( 'Only work with List Type: (Manual) Numbered list.', 'businextcoin' ),
					),
					array(
						'heading'     => esc_html__( 'Title', 'businextcoin' ),
						'type'        => 'textfield',
						'param_name'  => 'item_title',
						'admin_label' => true,
					),
					array(
						'heading'    => esc_html__( 'Sub Title', 'businextcoin' ),
						'type'       => 'textfield',
						'param_name' => 'item_sub_title',
					),
					array(
						'heading'    => esc_html__( 'Link', 'businextcoin' ),
						'type'       => 'vc_link',
						'param_name' => 'link',
					),
					array(
						'heading'    => esc_html__( 'Description', 'businextcoin' ),
						'type'       => 'textarea',
						'param_name' => 'item_desc',
					),
					array(
						'type'       => 'iconpicker',
						'heading'    => esc_html__( 'Icon', 'businextcoin' ),
						'param_name' => 'icon',
						'settings'   => array(
							'emptyIcon'    => true,
							'type'         => 'ion',
							'iconsPerPage' => 400,
						),
					),
				),
			),

		), Businextcoin_VC::get_vc_spacing_tab() ),
) );
