<?php

class WPBakeryShortCode_TM_CountDown extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		global $businextcoin_shortcode_lg_css;
		$skin = '';
		extract( $atts );

		if ( $skin === 'custom' ) {
			$number_tmp = Businextcoin_Helper::get_shortcode_css_color_inherit( 'color', $atts['number_color'], $atts['custom_number_color'] );
			$text_tmp   = Businextcoin_Helper::get_shortcode_css_color_inherit( 'color', $atts['text_color'], $atts['custom_text_color'] );

			if ( $number_tmp !== '' ) {
				$businextcoin_shortcode_lg_css .= "$selector .number { $number_tmp }";
			}

			if ( $text_tmp !== '' ) {
				$businextcoin_shortcode_lg_css .= "$selector .text { $text_tmp }";
			}
		}

		Businextcoin_VC::get_vc_spacing_css( $selector, $atts );
	}
}

vc_map( array(
	'name'                      => esc_html__( 'Countdown', 'businextcoin' ),
	'base'                      => 'tm_countdown',
	'category'                  => BUSINEXTCOIN_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-countdownclock',
	'allowed_container_element' => 'vc_row',
	'params'                    => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'businextcoin' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( '01', 'businextcoin' ) => '01',
				esc_html__( '02', 'businextcoin' ) => '02',
				esc_html__( '03', 'businextcoin' ) => '03',
			),
			'std'         => '01',
		),
		array(
			'heading'     => esc_html__( 'Skin', 'businextcoin' ),
			'type'        => 'dropdown',
			'param_name'  => 'skin',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Custom', 'businextcoin' ) => 'custom',
				esc_html__( 'Dark', 'businextcoin' )   => 'dark',
				esc_html__( 'Light', 'businextcoin' )  => 'light',
			),
			'std'         => 'dark',
		),
		array(
			'type'             => 'dropdown',
			'heading'          => esc_html__( 'Number Color', 'businextcoin' ),
			'param_name'       => 'number_color',
			'value'            => array(
				esc_html__( 'Default Color', 'businextcoin' )   => '',
				esc_html__( 'Primary Color', 'businextcoin' )   => 'primary',
				esc_html__( 'Secondary Color', 'businextcoin' ) => 'secondary',
				esc_html__( 'Custom Color', 'businextcoin' )    => 'custom',
			),
			'std'              => 'secondary',
			'edit_field_class' => 'vc_col-sm-6 col-break',
			'dependency'       => array(
				'element' => 'skin',
				'value'   => array( 'custom' ),
			),
		),
		array(
			'type'             => 'colorpicker',
			'heading'          => esc_html__( 'Custom Number Color', 'businextcoin' ),
			'param_name'       => 'custom_number_color',
			'edit_field_class' => 'vc_col-sm-6',
			'dependency'       => array(
				'element' => 'number_color',
				'value'   => array( 'custom' ),
			),
		),
		array(
			'type'             => 'dropdown',
			'heading'          => esc_html__( 'Text Color', 'businextcoin' ),
			'param_name'       => 'text_color',
			'value'            => array(
				esc_html__( 'Default Color', 'businextcoin' )   => '',
				esc_html__( 'Primary Color', 'businextcoin' )   => 'primary',
				esc_html__( 'Secondary Color', 'businextcoin' ) => 'secondary',
				esc_html__( 'Custom Color', 'businextcoin' )    => 'custom',
			),
			'std'              => 'custom',
			'edit_field_class' => 'vc_col-sm-6 col-break',
			'dependency'       => array(
				'element' => 'skin',
				'value'   => array( 'custom' ),
			),
		),
		array(
			'type'             => 'colorpicker',
			'heading'          => esc_html__( 'Custom Text Color', 'businextcoin' ),
			'param_name'       => 'custom_text_color',
			'edit_field_class' => 'vc_col-sm-6',
			'dependency'       => array(
				'element' => 'text_color',
				'value'   => array( 'custom' ),
			),
			'std'              => '#ababab',
		),
		array(
			'heading'     => esc_html__( 'Date Time', 'businextcoin' ),
			'description' => esc_html__( 'Date and time format (yyyy/mm/dd hh:mm).', 'businextcoin' ),
			'type'        => 'datetimepicker',
			'param_name'  => 'datetime',
			'value'       => '',
			'admin_label' => true,
			'settings'    => array(
				'minDate' => 0,
			),
		),
		array(
			'type'        => 'textfield',
			'heading'     => esc_html__( '"Days" text', 'businextcoin' ),
			'description' => esc_html__( 'Leave blank to use default.', 'businextcoin' ),
			'param_name'  => 'days',
		),
		array(
			'type'        => 'textfield',
			'heading'     => esc_html__( '"Hours" text', 'businextcoin' ),
			'description' => esc_html__( 'Leave blank to use default.', 'businextcoin' ),
			'param_name'  => 'hours',
		),
		array(
			'type'        => 'textfield',
			'heading'     => esc_html__( '"Minutes" text', 'businextcoin' ),
			'description' => esc_html__( 'Leave blank to use default.', 'businextcoin' ),
			'param_name'  => 'minutes',
		),
		array(
			'type'        => 'textfield',
			'heading'     => esc_html__( '"Seconds" text', 'businextcoin' ),
			'description' => esc_html__( 'Leave blank to use default.', 'businextcoin' ),
			'param_name'  => 'seconds',
		),
		Businextcoin_VC::extra_class_field(),
	), Businextcoin_VC::get_vc_spacing_tab() ),
) );

