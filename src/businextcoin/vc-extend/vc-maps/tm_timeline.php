<?php

class WPBakeryShortCode_TM_Timeline extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		Businextcoin_VC::get_vc_spacing_css( $selector, $atts );
	}
}

vc_map( array(
	'name'                      => esc_html__( 'Timeline', 'businextcoin' ),
	'base'                      => 'tm_timeline',
	'category'                  => BUSINEXTCOIN_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-timeline',
	'allowed_container_element' => 'vc_row',
	'params'                    => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'businextcoin' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( '01', 'businextcoin' ) => '01',
				esc_html__( '02', 'businextcoin' ) => '02',
				esc_html__( '03', 'businextcoin' ) => '03',
				esc_html__( '04', 'businextcoin' ) => '04',
				esc_html__( '05', 'businextcoin' ) => '05',
			),
			'std'         => '01',
		),
		array(
			'heading'     => esc_html__( 'Skin', 'businextcoin' ),
			'type'        => 'dropdown',
			'param_name'  => 'skin',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Dark', 'businextcoin' )  => 'dark',
				esc_html__( 'Light', 'businextcoin' ) => 'light',
			),
			'std'         => 'dark',
		),
		Businextcoin_VC::extra_class_field(),
		Businextcoin_VC::extra_id_field(),
		array(
			'group'      => esc_html__( 'Items', 'businextcoin' ),
			'heading'    => esc_html__( 'Items', 'businextcoin' ),
			'type'       => 'param_group',
			'param_name' => 'items',
			'params'     => array_merge( array(
				array(
					'heading'    => esc_html__( 'Image', 'businextcoin' ),
					'type'       => 'attach_image',
					'param_name' => 'image',
				),
				array(
					'heading'     => esc_html__( 'Title', 'businextcoin' ),
					'type'        => 'textfield',
					'param_name'  => 'title',
					'admin_label' => true,
				),
				array(
					'heading'     => esc_html__( 'Date Time', 'businextcoin' ),
					'description' => esc_html__( 'Date and time format (yyyy/mm/dd hh:mm).', 'businextcoin' ),
					'type'        => 'datetimepicker',
					'param_name'  => 'datetime',
					'value'       => '',
					'admin_label' => true,
				),
				array(
					'heading'    => esc_html__( 'Text', 'businextcoin' ),
					'type'       => 'textarea',
					'param_name' => 'text',
				),
			), Businextcoin_VC::icon_libraries_in_param_group( array(
				'allow_none' => true,
				'group'      => '',
			) ) ),
		),
	), Businextcoin_VC::get_vc_spacing_tab(), Businextcoin_VC::get_custom_style_tab() ),
) );
