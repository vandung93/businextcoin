<?php

class WPBakeryShortCode_TM_Popup_Video extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		Businextcoin_VC::get_vc_spacing_css( $selector, $atts );
	}
}

vc_map( array(
	'name'                      => esc_html__( 'Popup Video', 'businextcoin' ),
	'base'                      => 'tm_popup_video',
	'category'                  => BUSINEXTCOIN_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-video',
	'allowed_container_element' => 'vc_row',
	'params'                    => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'businextcoin' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Poster Style 01', 'businextcoin' ) => 'poster-01',
				esc_html__( 'Poster Style 02', 'businextcoin' ) => 'poster-02',
				esc_html__( 'Poster Style 03', 'businextcoin' ) => 'poster-03',
				esc_html__( 'Button Style 01', 'businextcoin' ) => 'button',
			),
			'std'         => 'poster-01',
		),
		array(
			'heading'     => esc_html__( 'Video Url', 'businextcoin' ),
			'description' => esc_html__( 'Ex: "https://www.youtube.com/watch?v=9No-FiEInLA"', 'businextcoin' ),
			'type'        => 'textfield',
			'param_name'  => 'video',
		),
		array(
			'heading'    => esc_html__( 'Video Text', 'businextcoin' ),
			'type'       => 'textfield',
			'param_name' => 'video_text',
			'std'        => esc_html__( 'Play Video', 'businextcoin' ),
			'dependency' => array(
				'element'            => 'style',
				'value_not_equal_to' => array(
					'poster-02',
					'poster-03',
				),
			),
		),
		array(
			'heading'    => esc_html__( 'Poster Image', 'businextcoin' ),
			'type'       => 'attach_image',
			'param_name' => 'poster',
			'dependency' => array(
				'element' => 'style',
				'value'   => array(
					'poster-01',
					'poster-02',
					'poster-03',
				),
			),
		),
		array(
			'heading'     => esc_html__( 'Poster Image Size', 'businextcoin' ),
			'description' => esc_html__( 'Controls the size of poster image.', 'businextcoin' ),
			'type'        => 'dropdown',
			'param_name'  => 'image_size',
			'value'       => array(
				esc_html__( '570x420', 'businextcoin' ) => '570x420',
				esc_html__( 'Full', 'businextcoin' )    => 'full',
				esc_html__( 'Custom', 'businextcoin' )  => 'custom',
			),
			'std'         => '570x420',
			'dependency'  => array(
				'element' => 'style',
				'value'   => array(
					'poster-01',
					'poster-02',
					'poster-03',
				),
			),
		),
		array(
			'heading'          => esc_html__( 'Image Width', 'businextcoin' ),
			'type'             => 'number',
			'param_name'       => 'image_size_width',
			'min'              => 0,
			'max'              => 1920,
			'step'             => 10,
			'suffix'           => 'px',
			'dependency'       => array(
				'element' => 'image_size',
				'value'   => array( 'custom' ),
			),
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'heading'          => esc_html__( 'Image Height', 'businextcoin' ),
			'type'             => 'number',
			'param_name'       => 'image_size_height',
			'min'              => 0,
			'max'              => 1920,
			'step'             => 10,
			'suffix'           => 'px',
			'dependency'       => array(
				'element' => 'image_size',
				'value'   => array( 'custom' ),
			),
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		Businextcoin_VC::extra_class_field(),
	), Businextcoin_VC::get_vc_spacing_tab(), Businextcoin_VC::get_custom_style_tab() ),
) );
