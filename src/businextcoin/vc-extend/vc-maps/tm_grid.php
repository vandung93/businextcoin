<?php

class WPBakeryShortCode_TM_Grid extends WPBakeryShortCodesContainer {

	public function get_inline_css( $selector = '', $atts ) {
		Businextcoin_VC::get_grid_css( $selector, $atts );

		Businextcoin_VC::get_vc_spacing_css( $selector, $atts );
	}
}

vc_map( array(
	'name'            => esc_html__( 'Grid', 'businextcoin' ),
	'base'            => 'tm_grid',
	'category'        => BUSINEXTCOIN_VC_SHORTCODE_CATEGORY,
	'icon'            => 'insight-i insight-i-portfoliogrid',
	'as_parent'       => array( 'only' => array( 'tm_box_icon', 'tm_card' ) ),
	'content_element' => true,
	'is_container'    => true,
	'js_view'         => 'VcColumnView',
	'params'          => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'businextcoin' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'None', 'businextcoin' )                   => '',
				esc_html__( 'With rounded', 'businextcoin' )           => 'rounded',
				esc_html__( 'With separator', 'businextcoin' )         => 'border',
				esc_html__( 'With separator rounded', 'businextcoin' ) => 'border-rounded',
			),
			'std'         => '',
		),
		array(
			'heading'     => esc_html__( 'Columns', 'businextcoin' ),
			'type'        => 'number_responsive',
			'param_name'  => 'columns',
			'min'         => 1,
			'max'         => 6,
			'step'        => 1,
			'suffix'      => '',
			'media_query' => array(
				'lg' => '4',
				'md' => '3',
				'sm' => '2',
				'xs' => '1',
			),
		),
		array(
			'heading'     => esc_html__( 'Columns Gutter', 'businextcoin' ),
			'description' => esc_html__( 'Controls the gutter of grid columns.', 'businextcoin' ),
			'type'        => 'number',
			'param_name'  => 'gutter',
			'std'         => 30,
			'min'         => 0,
			'max'         => 100,
			'step'        => 1,
			'suffix'      => 'px',
		),
		array(
			'heading'     => esc_html__( 'Rows Gutter', 'businextcoin' ),
			'description' => esc_html__( 'Controls the gutter of grid rows.', 'businextcoin' ),
			'type'        => 'number',
			'param_name'  => 'row_gutter',
			'std'         => 30,
			'min'         => 0,
			'max'         => 100,
			'step'        => 1,
			'suffix'      => 'px',
		),
		Businextcoin_VC::get_animation_field( array(
			'std' => 'move-up',
		) ),
		Businextcoin_VC::equal_height_class_field(),
		Businextcoin_VC::extra_class_field(),
	), Businextcoin_VC::get_vc_spacing_tab(), Businextcoin_VC::get_custom_style_tab() ),
) );

