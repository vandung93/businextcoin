<?php

class WPBakeryShortCode_TM_Contact_Form_7 extends WPBakeryShortCode {

}

/**
 * Add Shortcode To Visual Composer
 */
$cf7 = get_posts( 'post_type="wpcf7_contact_form"&numberposts=-1' );

$contact_forms = array();
if ( $cf7 ) {
	foreach ( $cf7 as $cform ) {
		$contact_forms[ $cform->post_title ] = $cform->ID;
	}
} else {
	$contact_forms[ esc_html__( 'No contact forms found', 'businextcoin' ) ] = 0;
}

vc_map( array(
	'name'                      => esc_html__( 'Contact Form 7', 'businextcoin' ),
	'base'                      => 'tm_contact_form_7',
	'category'                  => BUSINEXTCOIN_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-contact-form-7',
	'allowed_container_element' => 'vc_row',
	'params'                    => array(
		array(
			'type'        => 'dropdown',
			'heading'     => esc_html__( 'Form', 'businextcoin' ),
			'param_name'  => 'id',
			'value'       => $contact_forms,
			'save_always' => true,
			'admin_label' => true,
			'description' => esc_html__( 'Choose previously created contact form from the drop down list.', 'businextcoin' ),
		),
		array(
			'heading'     => esc_html__( 'Style', 'businextcoin' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( '01', 'businextcoin' ) => '01',
				esc_html__( '02', 'businextcoin' ) => '02',
			),
			'std'         => '01',
		),
		array(
			'heading'     => esc_html__( 'Form Box Style', 'businextcoin' ),
			'type'        => 'dropdown',
			'param_name'  => 'wrap_style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'None', 'businextcoin' ) => '',
				esc_html__( '01', 'businextcoin' )   => '01',
				esc_html__( '02', 'businextcoin' )   => '02',
			),
			'std'         => '',
		),
		Businextcoin_VC::extra_class_field(),
	),
) );
