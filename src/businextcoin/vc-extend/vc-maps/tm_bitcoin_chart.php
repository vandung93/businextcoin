<?php

class WPBakeryShortCode_TM_Bitcoin_Chart extends WPBakeryShortCode {

	function __construct( $settings ) {
		parent::__construct( $settings );

		global $post;
		if ( is_a( $post, 'WP_Post' ) && has_shortcode( $post->post_content, 'tm_bitcoin_chart' ) ) {
			add_action( 'wp_footer', array( $this, 'bitcoin_chart_script' ) );
		}
	}

	function bitcoin_chart_script() {
		?>
		<script>
            (
                function ( b, i, t, C, O, I, N ) {
                    window.addEventListener( 'load', function () {
                        if ( b.getElementById( C ) ) {
                            return;
                        }
                        I = b.createElement( i ), N = b.getElementsByTagName( i )[0];
                        I.src = t;
                        I.id  = C;
                        N.parentNode.insertBefore( I, N );
                    }, false )
                }
            )( document, 'script', 'https://widgets.bitcoin.com/widget.js', 'btcwdgt' );
		</script>
		<?php
	}

	public function get_inline_css( $selector = '', $atts ) {
		Businextcoin_VC::get_vc_spacing_css( $selector, $atts );
	}
}

$content_tab = esc_html__( 'Content', 'businextcoin' );
$styling_tab = esc_html__( 'Styling', 'businextcoin' );

vc_map( array(
	'name'                      => esc_html__( 'Bitcoin Chart', 'businextcoin' ),
	'base'                      => 'tm_bitcoin_chart',
	'category'                  => BUSINEXTCOIN_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-processbar',
	'allowed_container_element' => 'vc_row',
	'params'                    => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'businextcoin' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'value'       => array(
				esc_html__( 'Style 01', 'businextcoin' ) => '1',
			),
			'admin_label' => true,
			'std'         => '1',
		),
		array(
			'heading'     => esc_html__( 'Skin', 'businextcoin' ),
			'type'        => 'dropdown',
			'param_name'  => 'skin',
			'value'       => array(
				esc_html__( 'Light', 'businextcoin' ) => 'light',
				esc_html__( 'Dark', 'businextcoin' )  => 'dark',
			),
			'admin_label' => true,
			'std'         => 'light',
		),
		Businextcoin_VC::extra_class_field(),
	), Businextcoin_VC::get_vc_spacing_tab() ),

) );
