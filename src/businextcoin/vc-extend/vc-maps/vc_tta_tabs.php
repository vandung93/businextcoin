<?php

function businextcoin_vc_tta_tabs_get_inline_css( $selector = '', $atts ) {
	global $businextcoin_shortcode_lg_css;

	$border_color = $atts['custom_border_color'];

	if ( isset( $border_color ) && $border_color !== '' ) {
		$businextcoin_shortcode_lg_css .= "$selector .vc_tta-tab { border-color: {$border_color}; }";
	}
}

$_color_field                                                  = WPBMap::getParam( 'vc_tta_tabs', 'color' );
$_color_field['value'][ esc_html__( 'Primary', 'businextcoin' ) ] = 'primary';
$_color_field['std']                                           = 'primary';
vc_update_shortcode_param( 'vc_tta_tabs', $_color_field );

vc_update_shortcode_param( 'vc_tta_tabs', array(
	'param_name' => 'style',
	'value'      => array(
		esc_html__( 'Businextcoin 01', 'businextcoin' ) => 'businextcoin-01',
		esc_html__( 'Businextcoin 02', 'businextcoin' ) => 'businextcoin-02',
		esc_html__( 'Classic', 'businextcoin' )      => 'classic',
		esc_html__( 'Modern', 'businextcoin' )       => 'modern',
		esc_html__( 'Flat', 'businextcoin' )         => 'flat',
		esc_html__( 'Outline', 'businextcoin' )      => 'outline',
	),
) );

vc_add_params( 'vc_tta_tabs', array(
	array(
		'group'      => esc_html__( 'Styling', 'businextcoin' ),
		'heading'    => esc_html__( 'Border Color', 'businextcoin' ),
		'type'       => 'colorpicker',
		'param_name' => 'custom_border_color',
		'dependency' => array(
			'element' => 'style',
			'value'   => array( 'businextcoin-02' ),
		),
		'std'        => '',
	),
) );
