<?php

add_filter( 'vc_autocomplete_tm_portfolio_taxonomies_callback', array(
	'WPBakeryShortCode_TM_Portfolio',
	'autocomplete_taxonomies_field_search',
), 10, 1 );

add_filter( 'vc_autocomplete_tm_portfolio_taxonomies_render', array(
	Businextcoin_VC::instance(),
	'autocomplete_taxonomies_field_render',
), 10, 1 );

class WPBakeryShortCode_TM_Portfolio extends WPBakeryShortCode {

	/**
	 * @param $search_string
	 *
	 * @return array|bool
	 */
	function autocomplete_taxonomies_field_search( $search_string ) {
		$data = Businextcoin_VC::instance()->autocomplete_get_data_from_post_type( $search_string, 'portfolio' );

		return $data;
	}

	public function get_inline_css( $selector = '', $atts ) {
		global $businextcoin_shortcode_lg_css;
		global $businextcoin_shortcode_md_css;
		global $businextcoin_shortcode_sm_css;
		global $businextcoin_shortcode_xs_css;
		extract( $atts );

		if ( isset( $atts['carousel_height'] ) && $atts['carousel_height'] !== '' ) {
			$arr = explode( ';', $atts['carousel_height'] );

			foreach ( $arr as $value ) {
				$tmp = explode( ':', $value );
				if ( $tmp['0'] === 'lg' ) {
					$businextcoin_shortcode_lg_css .= "$selector .swiper-slide img { height: {$tmp['1']}px; }";
				} elseif ( $tmp['0'] === 'md' ) {
					$businextcoin_shortcode_md_css .= "$selector .swiper-slide img { height: {$tmp['1']}px; }";
				} elseif ( $tmp['0'] === 'sm' ) {
					$businextcoin_shortcode_sm_css .= "$selector .swiper-slide img { height: {$tmp['1']}px; }";
				} elseif ( $tmp['0'] === 'xs' ) {
					$businextcoin_shortcode_xs_css .= "$selector .swiper-slide img { height: {$tmp['1']}px; }";
				}
			}
		}

		$image_tmp = '';

		if ( $custom_styling_enable === '1' ) {
			Businextcoin_VC::get_responsive_css( array(
				'element' => "$selector .post-overlay-title",
				'atts'    => array(
					'font-size' => array(
						'media_str' => $overlay_title_font_size,
						'unit'      => 'px',
					),
				),
			) );

			if ( isset( $atts['image_rounded'] ) && $atts['image_rounded'] !== '' ) {
				$image_tmp .= Businextcoin_Helper::get_css_prefix( 'border-radius', $atts['image_rounded'] );
			}
		}

		if ( $image_tmp !== '' ) {
			$businextcoin_shortcode_lg_css .= "$selector .post-thumbnail img { {$image_tmp} }";
		}

		Businextcoin_VC::get_vc_spacing_css( $selector, $atts );
	}
}

$carousel_tab = esc_html__( 'Carousel Settings', 'businextcoin' );
$styling_tab  = esc_html__( 'Styling', 'businextcoin' );

vc_map( array(
	'name'     => esc_html__( 'Portfolio', 'businextcoin' ),
	'base'     => 'tm_portfolio',
	'category' => BUSINEXTCOIN_VC_SHORTCODE_CATEGORY,
	'icon'     => 'insight-i insight-i-portfoliogrid',
	'params'   => array_merge( array(
		array(
			'heading'     => esc_html__( 'Portfolio Style', 'businextcoin' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Grid Classic', 'businextcoin' )         => 'grid',
				esc_html__( 'Grid Metro', 'businextcoin' )           => 'metro',
				esc_html__( 'Grid Masonry', 'businextcoin' )         => 'masonry',
				esc_html__( 'Carousel Slider', 'businextcoin' )      => 'carousel',
				esc_html__( 'Full Wide Slider', 'businextcoin' )     => 'full-wide-slider',
				esc_html__( 'Grid Justify Gallery', 'businextcoin' ) => 'justified',
			),
			'std'         => 'grid',
		),
		array(
			'heading'    => esc_html__( 'Metro Layout', 'businextcoin' ),
			'type'       => 'param_group',
			'param_name' => 'metro_layout',
			'params'     => array(
				array(
					'heading'     => esc_html__( 'Item Size', 'businextcoin' ),
					'type'        => 'dropdown',
					'param_name'  => 'size',
					'admin_label' => true,
					'value'       => array(
						esc_html__( 'Width 1 - Height 1', 'businextcoin' ) => '1:1',
						esc_html__( 'Width 1 - Height 2', 'businextcoin' ) => '1:2',
						esc_html__( 'Width 2 - Height 1', 'businextcoin' ) => '2:1',
						esc_html__( 'Width 2 - Height 2', 'businextcoin' ) => '2:2',
					),
					'std'         => '1:1',
				),
			),
			'value'      => rawurlencode( wp_json_encode( array(
				array(
					'size' => '1:1',
				),
				array(
					'size' => '2:2',
				),
				array(
					'size' => '1:2',
				),
				array(
					'size' => '1:1',
				),
				array(
					'size' => '1:1',
				),
				array(
					'size' => '2:1',
				),
				array(
					'size' => '1:1',
				),
			) ) ),
			'dependency' => array(
				'element' => 'style',
				'value'   => array( 'metro' ),
			),
		),
		array(
			'heading'     => esc_html__( 'Columns', 'businextcoin' ),
			'type'        => 'number_responsive',
			'param_name'  => 'columns',
			'min'         => 1,
			'max'         => 6,
			'step'        => 1,
			'suffix'      => '',
			'media_query' => array(
				'lg' => '3',
				'md' => '',
				'sm' => '2',
				'xs' => '1',
			),
			'dependency'  => array(
				'element' => 'style',
				'value'   => array(
					'grid',
					'metro',
					'masonry',
				),
			),
		),
		array(
			'heading'     => esc_html__( 'Grid Gutter', 'businextcoin' ),
			'description' => esc_html__( 'Controls the gutter of grid.', 'businextcoin' ),
			'type'        => 'number',
			'param_name'  => 'gutter',
			'std'         => 30,
			'min'         => 0,
			'max'         => 100,
			'step'        => 1,
			'suffix'      => 'px',
			'dependency'  => array(
				'element' => 'style',
				'value'   => array(
					'grid',
					'metro',
					'masonry',
					'justified',
				),
			),
		),
		array(
			'heading'    => esc_html__( 'Image Size', 'businextcoin' ),
			'type'       => 'dropdown',
			'param_name' => 'image_size',
			'value'      => array(
				esc_html__( '480x480', 'businextcoin' ) => '480x480',
				esc_html__( '480x311', 'businextcoin' ) => '480x311',
				esc_html__( '481x325', 'businextcoin' ) => '481x325',
				esc_html__( '500x324', 'businextcoin' ) => '500x324',
			),
			'std'        => '480x480',
			'dependency' => array(
				'element' => 'style',
				'value'   => array(
					'grid',
				),
			),
		),
		array(
			'heading'     => esc_html__( 'Row Height', 'businextcoin' ),
			'description' => esc_html__( 'Controls the height of grid row.', 'businextcoin' ),
			'type'        => 'number',
			'param_name'  => 'justify_row_height',
			'std'         => 300,
			'min'         => 50,
			'max'         => 500,
			'step'        => 10,
			'suffix'      => 'px',
			'dependency'  => array(
				'element' => 'style',
				'value'   => array( 'justified' ),
			),
		),
		array(
			'heading'     => esc_html__( 'Max Row Height', 'businextcoin' ),
			'description' => esc_html__( 'Controls the max height of grid row. Leave blank or 0 keep it disabled.', 'businextcoin' ),
			'type'        => 'number',
			'param_name'  => 'justify_max_row_height',
			'std'         => 0,
			'min'         => 0,
			'max'         => 500,
			'step'        => 10,
			'suffix'      => 'px',
			'dependency'  => array(
				'element' => 'style',
				'value'   => array( 'justified' ),
			),
		),
		array(
			'heading'    => esc_html__( 'Last row alignment', 'businextcoin' ),
			'type'       => 'dropdown',
			'param_name' => 'justify_last_row_alignment',
			'value'      => array(
				esc_html__( 'Justify', 'businextcoin' )                              => 'justify',
				esc_html__( 'Left', 'businextcoin' )                                 => 'nojustify',
				esc_html__( 'Center', 'businextcoin' )                               => 'center',
				esc_html__( 'Right', 'businextcoin' )                                => 'right',
				esc_html__( 'Hide ( if row can not be justified )', 'businextcoin' ) => 'hide',
			),
			'std'        => 'justify',
			'dependency' => array(
				'element' => 'style',
				'value'   => array( 'justified' ),
			),
		),
		array(
			'heading'    => esc_html__( 'Overlay Style', 'businextcoin' ),
			'type'       => 'dropdown',
			'param_name' => 'overlay_style',
			'value'      => array(
				esc_html__( 'None', 'businextcoin' )                             => 'none',
				esc_html__( 'Modern', 'businextcoin' )                           => 'modern',
				esc_html__( 'Image zoom - content below', 'businextcoin' )       => 'zoom',
				esc_html__( 'Zoom and Move Up - content below', 'businextcoin' ) => 'zoom2',
				esc_html__( 'Faded', 'businextcoin' )                            => 'faded',
				esc_html__( 'Faded - Light', 'businextcoin' )                    => 'faded-light',
			),
			'std'        => 'faded-light',
			'dependency' => array(
				'element' => 'style',
				'value'   => array(
					'grid',
					'metro',
					'masonry',
					'carousel',
					'justified',
				),
			),
		),
		Businextcoin_VC::get_animation_field( array(
			'std'        => 'move-up',
			'dependency' => array(
				'element' => 'style',
				'value'   => array(
					'grid',
					'metro',
					'masonry',
					'justified',
				),
			),
		) ),
		Businextcoin_VC::extra_class_field(),
		array(
			'group'       => $carousel_tab,
			'heading'     => esc_html__( 'Auto Play', 'businextcoin' ),
			'description' => esc_html__( 'Delay between transitions (in ms), ex: 3000. Leave blank to disabled.', 'businextcoin' ),
			'type'        => 'number',
			'suffix'      => 'ms',
			'param_name'  => 'carousel_auto_play',
			'dependency'  => array(
				'element' => 'style',
				'value'   => array(
					'carousel',
					'full-wide-slider',
				),
			),
		),
		array(
			'group'      => $carousel_tab,
			'heading'    => esc_html__( 'Navigation', 'businextcoin' ),
			'type'       => 'dropdown',
			'param_name' => 'carousel_nav',
			'value'      => Businextcoin_VC::get_slider_navs(),
			'std'        => '',
			'dependency' => array(
				'element' => 'style',
				'value'   => array(
					'carousel',
					'full-wide-slider',
				),
			),
		),
		Businextcoin_VC::extra_id_field( array(
			'group'      => $carousel_tab,
			'heading'    => esc_html__( 'Slider Button ID', 'businextcoin' ),
			'param_name' => 'slider_button_id',
			'dependency' => array(
				'element' => 'carousel_nav',
				'value'   => array(
					'custom',
				),
			),
		) ),
		array(
			'group'      => $carousel_tab,
			'heading'    => esc_html__( 'Pagination', 'businextcoin' ),
			'type'       => 'dropdown',
			'param_name' => 'carousel_pagination',
			'value'      => Businextcoin_VC::get_slider_dots(),
			'std'        => '',
			'dependency' => array(
				'element' => 'style',
				'value'   => array(
					'carousel',
					'full-wide-slider',
				),
			),
		),
		array(
			'group'      => $carousel_tab,
			'heading'    => esc_html__( 'Gutter', 'businextcoin' ),
			'type'       => 'number',
			'param_name' => 'carousel_gutter',
			'std'        => 30,
			'min'        => 0,
			'max'        => 50,
			'step'       => 1,
			'suffix'     => 'px',
			'dependency' => array(
				'element' => 'style',
				'value'   => array(
					'carousel',
				),
			),
		),
		array(
			'group'       => $carousel_tab,
			'heading'     => esc_html__( 'Items Display', 'businextcoin' ),
			'type'        => 'number_responsive',
			'param_name'  => 'carousel_items_display',
			'min'         => 1,
			'max'         => 10,
			'suffix'      => 'item (s)',
			'media_query' => array(
				'lg' => 3,
				'md' => 3,
				'sm' => 2,
				'xs' => 1,
			),
			'dependency'  => array(
				'element' => 'style',
				'value'   => 'carousel',
			),
		),
		array(
			'group'      => esc_html__( 'Data Settings', 'businextcoin' ),
			'type'       => 'hidden',
			'param_name' => 'main_query',
			'std'        => '',
		),
		array(
			'group'       => esc_html__( 'Data Settings', 'businextcoin' ),
			'heading'     => esc_html__( 'Items per page', 'businextcoin' ),
			'description' => esc_html__( 'Number of items to show per page.', 'businextcoin' ),
			'type'        => 'number',
			'param_name'  => 'number',
			'std'         => 9,
			'min'         => 1,
			'max'         => 100,
			'step'        => 1,
		),
		array(
			'group'              => esc_html__( 'Data Settings', 'businextcoin' ),
			'heading'            => esc_html__( 'Narrow data source', 'businextcoin' ),
			'description'        => esc_html__( 'Enter categories, tags or custom taxonomies.', 'businextcoin' ),
			'type'               => 'autocomplete',
			'param_name'         => 'taxonomies',
			'settings'           => array(
				'multiple'       => true,
				'min_length'     => 1,
				'groups'         => true,
				// In UI show results grouped by groups, default false.
				'unique_values'  => true,
				// In UI show results except selected. NB! You should manually check values in backend, default false.
				'display_inline' => true,
				// In UI show results inline view, default false (each value in own line).
				'delay'          => 500,
				// delay for search. default 500.
				'auto_focus'     => true,
				// auto focus input, default true.
			),
			'param_holder_class' => 'vc_not-for-custom',
		),
		array(
			'group'       => esc_html__( 'Data Settings', 'businextcoin' ),
			'heading'     => esc_html__( 'Order by', 'businextcoin' ),
			'type'        => 'dropdown',
			'param_name'  => 'orderby',
			'value'       => array(
				esc_html__( 'Date', 'businextcoin' )                  => 'date',
				esc_html__( 'Post ID', 'businextcoin' )               => 'ID',
				esc_html__( 'Author', 'businextcoin' )                => 'author',
				esc_html__( 'Title', 'businextcoin' )                 => 'title',
				esc_html__( 'Last modified date', 'businextcoin' )    => 'modified',
				esc_html__( 'Post/page parent ID', 'businextcoin' )   => 'parent',
				esc_html__( 'Number of comments', 'businextcoin' )    => 'comment_count',
				esc_html__( 'Menu order/Page Order', 'businextcoin' ) => 'menu_order',
				esc_html__( 'Meta value', 'businextcoin' )            => 'meta_value',
				esc_html__( 'Meta value number', 'businextcoin' )     => 'meta_value_num',
				esc_html__( 'Random order', 'businextcoin' )          => 'rand',
			),
			'description' => esc_html__( 'Select order type. If "Meta value" or "Meta value Number" is chosen then meta key is required.', 'businextcoin' ),
			'std'         => 'date',
		),
		array(
			'group'       => esc_html__( 'Data Settings', 'businextcoin' ),
			'heading'     => esc_html__( 'Sort order', 'businextcoin' ),
			'type'        => 'dropdown',
			'param_name'  => 'order',
			'value'       => array(
				esc_html__( 'Descending', 'businextcoin' ) => 'DESC',
				esc_html__( 'Ascending', 'businextcoin' )  => 'ASC',
			),
			'description' => esc_html__( 'Select sorting order.', 'businextcoin' ),
			'std'         => 'DESC',
		),
		array(
			'group'       => esc_html__( 'Data Settings', 'businextcoin' ),
			'heading'     => esc_html__( 'Meta key', 'businextcoin' ),
			'description' => esc_html__( 'Input meta key for grid ordering.', 'businextcoin' ),
			'type'        => 'textfield',
			'param_name'  => 'meta_key',
			'dependency'  => array(
				'element' => 'orderby',
				'value'   => array(
					'meta_value',
					'meta_value_num',
				),
			),
		),
		array(
			'group'      => $styling_tab,
			'heading'    => esc_html__( 'Custom Styling Enable', 'businextcoin' ),
			'type'       => 'checkbox',
			'param_name' => 'custom_styling_enable',
			'value'      => array( esc_html__( 'Yes', 'businextcoin' ) => '1' ),
		),
		array(
			'group'       => $styling_tab,
			'heading'     => esc_html__( 'Overlay Title Font Size', 'businextcoin' ),
			'type'        => 'number_responsive',
			'param_name'  => 'overlay_title_font_size',
			'min'         => 8,
			'suffix'      => 'px',
			'media_query' => array(
				'lg' => '',
				'md' => '',
				'sm' => '',
				'xs' => '',
			),
		),
		array(
			'group'       => $styling_tab,
			'heading'     => esc_html__( 'Image Rounded', 'businextcoin' ),
			'type'        => 'textfield',
			'param_name'  => 'image_rounded',
			'description' => esc_html__( 'Input a valid radius. Fox Ex: 10px. Leave blank to use default.', 'businextcoin' ),
		),
		array(
			'group'      => esc_html__( 'Filter', 'businextcoin' ),
			'heading'    => esc_html__( 'Filter Enable', 'businextcoin' ),
			'type'       => 'checkbox',
			'param_name' => 'filter_enable',
			'value'      => array( esc_html__( 'Enable', 'businextcoin' ) => '1' ),
		),
		array(
			'group'      => esc_html__( 'Filter', 'businextcoin' ),
			'heading'    => esc_html__( 'Filter Counter', 'businextcoin' ),
			'type'       => 'checkbox',
			'param_name' => 'filter_counter',
			'value'      => array( esc_html__( 'Enable', 'businextcoin' ) => '1' ),
		),
		array(
			'group'       => esc_html__( 'Filter', 'businextcoin' ),
			'heading'     => esc_html__( 'Filter Grid Wrapper', 'businextcoin' ),
			'description' => esc_html__( 'Wrap filter into grid container.', 'businextcoin' ),
			'type'        => 'checkbox',
			'param_name'  => 'filter_wrap',
			'value'       => array( esc_html__( 'Enable', 'businextcoin' ) => '1' ),
		),
		array(
			'group'      => esc_html__( 'Filter', 'businextcoin' ),
			'heading'    => esc_html__( 'Filter Align', 'businextcoin' ),
			'type'       => 'dropdown',
			'param_name' => 'filter_align',
			'value'      => array(
				esc_html__( 'Left', 'businextcoin' )   => 'left',
				esc_html__( 'Center', 'businextcoin' ) => 'center',
				esc_html__( 'Right', 'businextcoin' )  => 'right',
			),
			'std'        => 'center',
		),
		array(
			'group'      => esc_html__( 'Pagination', 'businextcoin' ),
			'heading'    => esc_html__( 'Pagination', 'businextcoin' ),
			'type'       => 'dropdown',
			'param_name' => 'pagination',
			'value'      => array(
				esc_html__( 'No Pagination', 'businextcoin' ) => '',
				esc_html__( 'Pagination', 'businextcoin' )    => 'pagination',
				esc_html__( 'Button', 'businextcoin' )        => 'loadmore',
				esc_html__( 'Custom Button', 'businextcoin' ) => 'loadmore_alt',
				esc_html__( 'Infinite', 'businextcoin' )      => 'infinite',
			),
			'std'        => '',
		),
		array(
			'group'      => esc_html__( 'Pagination', 'businextcoin' ),
			'heading'    => esc_html__( 'Pagination Align', 'businextcoin' ),
			'type'       => 'dropdown',
			'param_name' => 'pagination_align',
			'value'      => array(
				esc_html__( 'Left', 'businextcoin' )   => 'left',
				esc_html__( 'Center', 'businextcoin' ) => 'center',
				esc_html__( 'Right', 'businextcoin' )  => 'right',
			),
			'std'        => 'left',
			'dependency' => array(
				'element' => 'pagination',
				'value'   => array( 'pagination', 'infinite', 'loadmore', 'loadmore_alt' ),
			),
		),
		array(
			'group'       => esc_html__( 'Pagination', 'businextcoin' ),
			'heading'     => esc_html__( 'Button ID', 'businextcoin' ),
			'description' => esc_html__( 'Input id of custom button to load more posts when click. For EX: #product-load-more-btn', 'businextcoin' ),
			'type'        => 'el_id',
			'param_name'  => 'pagination_custom_button_id',
			'dependency'  => array(
				'element' => 'pagination',
				'value'   => 'loadmore_alt',
			),
		),
		array(
			'group'      => esc_html__( 'Pagination', 'businextcoin' ),
			'heading'    => esc_html__( 'Pagination Button Text', 'businextcoin' ),
			'type'       => 'textfield',
			'param_name' => 'pagination_button_text',
			'std'        => esc_html__( 'Load More', 'businextcoin' ),
			'dependency' => array(
				'element' => 'pagination',
				'value'   => 'loadmore',
			),
		),
	), Businextcoin_VC::get_vc_spacing_tab() ),
) );

