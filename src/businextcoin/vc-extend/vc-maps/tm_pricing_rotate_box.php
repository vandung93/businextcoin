<?php

class WPBakeryShortCode_TM_Pricing_Rotate_Box extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		global $businextcoin_shortcode_lg_css;
		$tmp = '';

		if ( isset( $atts['image'] ) && $atts['image'] !== '' ) {
			$full_image_size = wp_get_attachment_image_url( $atts['image'], 'full' );
			$image_url       = Businextcoin_Helper::aq_resize( array(
				'url'    => $full_image_size,
				'width'  => 480,
				'height' => 480,
				'crop'   => true,
			) );

			$tmp .= "background-image: url( {$image_url} );";
		}

		if ( $tmp !== '' ) {
			$businextcoin_shortcode_lg_css .= "$selector .front { $tmp }";
		}

		Businextcoin_VC::get_vc_spacing_css( $selector, $atts );
	}
}

vc_map( array(
	'name'                      => esc_html__( 'Pricing Rotate Box', 'businextcoin' ),
	'base'                      => 'tm_pricing_rotate_box',
	'category'                  => BUSINEXTCOIN_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-pricing',
	'allowed_container_element' => 'vc_row',
	'params'                    => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'businextcoin' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( '01', 'businextcoin' ) => '01',
			),
			'std'         => '01',
		),
		array(
			'heading'    => esc_html__( 'Image', 'businextcoin' ),
			'type'       => 'attach_image',
			'param_name' => 'image',
		),
		array(
			'heading'     => esc_html__( 'Title', 'businextcoin' ),
			'type'        => 'textfield',
			'admin_label' => true,
			'param_name'  => 'title',
		),
		array(
			'heading'          => esc_html__( 'Currency', 'businextcoin' ),
			'type'             => 'textfield',
			'param_name'       => 'currency',
			'value'            => '$',
			'edit_field_class' => 'vc_col-sm-4',
		),
		array(
			'heading'          => esc_html__( 'Price', 'businextcoin' ),
			'type'             => 'textfield',
			'param_name'       => 'price',
			'edit_field_class' => 'vc_col-sm-4',
		),
		array(
			'heading'          => esc_html__( 'Period', 'businextcoin' ),
			'type'             => 'textfield',
			'param_name'       => 'period',
			'value'            => 'per monthly',
			'edit_field_class' => 'vc_col-sm-4',
		),
		array(
			'type'       => 'vc_link',
			'heading'    => esc_html__( 'Button', 'businextcoin' ),
			'param_name' => 'button',
		),
		Businextcoin_VC::get_animation_field(),
		Businextcoin_VC::extra_class_field(),
		array(
			'group'      => esc_html__( 'Items', 'businextcoin' ),
			'heading'    => esc_html__( 'Items', 'businextcoin' ),
			'type'       => 'param_group',
			'param_name' => 'items',
			'params'     => array(
				array(
					'heading'     => esc_html__( 'Text', 'businextcoin' ),
					'type'        => 'textfield',
					'param_name'  => 'text',
					'admin_label' => true,
				),
			),
		),
	), Businextcoin_VC::get_vc_spacing_tab() ),
) );
