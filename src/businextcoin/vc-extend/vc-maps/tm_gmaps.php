<?php

class WPBakeryShortCode_TM_Gmaps extends WPBakeryShortCode {

	public function convertAttributesToNewMarker( $atts ) {
		if ( isset( $atts['markers'] ) && strlen( $atts['markers'] ) > 0 ) {
			$markers = vc_param_group_parse_atts( $atts['markers'] );

			if ( ! is_array( $markers ) ) {
				$temp         = explode( ',', $atts['markers'] );
				$paramMarkers = array();

				foreach ( $temp as $marker ) {
					$data = explode( '|', $marker );

					$newMarker            = array();
					$newMarker['address'] = isset( $data[0] ) ? $data[0] : '';
					$newMarker['icon']    = isset( $data[1] ) ? $data[1] : '';
					$newMarker['title']   = isset( $data[2] ) ? $data[2] : '';
					$newMarker['info']    = isset( $data[3] ) ? $data[3] : '';

					$paramMarkers[] = $newMarker;
				}

				$atts['markers'] = urlencode( json_encode( $paramMarkers ) );

			}

			return $atts;
		}
	}
}

vc_map( array(
	'name'     => esc_html__( 'Google Maps', 'businextcoin' ),
	'base'     => 'tm_gmaps',
	'icon'     => 'insight-i insight-i-map',
	'category' => BUSINEXTCOIN_VC_SHORTCODE_CATEGORY,
	'params'   => array(
		array(
			'heading'     => esc_html__( 'Height', 'businextcoin' ),
			'description' => esc_html__( 'Enter map height (in pixels or %)', 'businextcoin' ),
			'type'        => 'textfield',
			'param_name'  => 'map_height',
			'value'       => '480',
		),
		array(
			'heading'     => esc_html__( 'Width', 'businextcoin' ),
			'description' => esc_html__( 'Enter map width (in pixels or %)', 'businextcoin' ),
			'type'        => 'textfield',
			'param_name'  => 'map_width',
			'value'       => '100%',
		),
		array(
			'heading'    => esc_html__( 'Button', 'businextcoin' ),
			'type'       => 'vc_link',
			'param_name' => 'button',
			'value'      => esc_html__( 'Button', 'businextcoin' ),
		),
		array(
			'heading'     => esc_html__( 'Zoom Level', 'businextcoin' ),
			'description' => esc_html__( 'Map zoom level', 'businextcoin' ),
			'type'        => 'number',
			'param_name'  => 'zoom',
			'value'       => 16,
			'max'         => 17,
			'min'         => 0,
		),
		array(
			'type'       => 'checkbox',
			'param_name' => 'zoom_enable',
			'value'      => array(
				esc_html__( 'Enable mouse scroll wheel zoom', 'businextcoin' ) => 'yes',
			),
		),
		array(
			'heading'     => esc_html__( 'Map Type', 'businextcoin' ),
			'description' => esc_html__( 'Choose a map type', 'businextcoin' ),
			'type'        => 'dropdown',
			'admin_label' => true,
			'param_name'  => 'map_type',
			'value'       => array(
				esc_html__( 'Roadmap', 'businextcoin' )   => 'roadmap',
				esc_html__( 'Satellite', 'businextcoin' ) => 'satellite',
				esc_html__( 'Hybrid', 'businextcoin' )    => 'hybrid',
				esc_html__( 'Terrain', 'businextcoin' )   => 'terrain',
			),
		),
		array(
			'heading'     => esc_html__( 'Map Style', 'businextcoin' ),
			'description' => esc_html__( 'Choose a map style. This approach changes the style of the Roadmap types (base imagery in terrain and satellite views is not affected, but roads, labels, etc. respect styling rules)', 'businextcoin' ),
			'type'        => 'image_radio',
			'admin_label' => true,
			'param_name'  => 'map_style',
			'value'       => array(
				'grayscale'               => array(
					'url'   => BUSINEXTCOIN_THEME_IMAGE_URI . '/maps/greyscale.png',
					'title' => esc_attr__( 'Grayscale', 'businextcoin' ),
				),
				'subtle_grayscale'        => array(
					'url'   => BUSINEXTCOIN_THEME_IMAGE_URI . '/maps/subtle-grayscale.png',
					'title' => esc_attr__( 'Subtle Grayscale', 'businextcoin' ),
				),
				'apple_paps_esque'        => array(
					'url'   => BUSINEXTCOIN_THEME_IMAGE_URI . '/maps/apple-maps-esque.png',
					'title' => esc_attr__( 'Apple Maps-esque', 'businextcoin' ),
				),
				'pale_dawn'               => array(
					'url'   => BUSINEXTCOIN_THEME_IMAGE_URI . '/maps/pale-dawn.png',
					'title' => esc_attr__( 'Pale Dawn', 'businextcoin' ),
				),
				'midnight_commander'      => array(
					'url'   => BUSINEXTCOIN_THEME_IMAGE_URI . '/maps/midnight-commander.png',
					'title' => esc_attr__( 'Midnight Commander', 'businextcoin' ),
				),
				'blue_water'              => array(
					'url'   => BUSINEXTCOIN_THEME_IMAGE_URI . '/maps/blue-water.png',
					'title' => esc_attr__( 'Blue Water', 'businextcoin' ),
				),
				'retro'                   => array(
					'url'   => BUSINEXTCOIN_THEME_IMAGE_URI . '/maps/retro.png',
					'title' => esc_attr__( 'Retro', 'businextcoin' ),
				),
				'paper'                   => array(
					'url'   => BUSINEXTCOIN_THEME_IMAGE_URI . '/maps/paper.png',
					'title' => esc_attr__( 'Paper', 'businextcoin' ),
				),
				'ultra_light_with_labels' => array(
					'url'   => BUSINEXTCOIN_THEME_IMAGE_URI . '/maps/ultra-light-with-labels.png',
					'title' => esc_attr__( 'Ultra Light with Labels', 'businextcoin' ),
				),
				'shades_of_grey'          => array(
					'url'   => BUSINEXTCOIN_THEME_IMAGE_URI . '/maps/shades-of-grey.png',
					'title' => esc_attr__( 'Shades Of Grey', 'businextcoin' ),
				),
			),
			'std'         => 'ultra_light_with_labels',
		),
		array(
			'type'       => 'checkbox',
			'param_name' => 'overlay_enable',
			'value'      => array(
				esc_html__( 'Use overlay instead of marker items', 'businextcoin' ) => '1',
			),
		),
		array(
			'heading'    => esc_html__( 'Overlay Style', 'businextcoin' ),
			'type'       => 'dropdown',
			'param_name' => 'overlay_style',
			'value'      => array(
				esc_html__( 'Style 01', 'businextcoin' ) => '01',
				esc_html__( 'Style 02', 'businextcoin' ) => '02',
			),
			'std'        => '01',
		),
		array(
			'group'       => esc_html__( 'Markers', 'businextcoin' ),
			'heading'     => esc_html__( 'Markers', 'businextcoin' ),
			'description' => esc_html__( 'You can add multiple markers to the map', 'businextcoin' ),
			'type'        => 'param_group',
			'param_name'  => 'markers',
			'value'       => urlencode( json_encode( array(
				array(
					'address' => '40.7590615,-73.969231',
				),
			) ) ),
			'params'      => array(
				array(
					'heading'     => esc_html__( 'Address or Coordinate', 'businextcoin' ),
					'description' => sprintf( wp_kses( __( 'Enter address or coordinate. Find coordinates using the name and/or address of the place using <a href="%s" target="_blank">this simple tool here.</a>', 'businextcoin' ), array(
						'a' => array(
							'href'   => array(),
							'target' => array(),
						),
					) ), esc_url( 'http://universimmedia.pagesperso-orange.fr/geo/loc.htm' ) ),
					'type'        => 'textfield',
					'param_name'  => 'address',
					'admin_label' => true,
				),
				array(
					'heading'     => esc_html__( 'Marker icon', 'businextcoin' ),
					'description' => esc_html__( 'Choose a image for marker address', 'businextcoin' ),
					'type'        => 'attach_image',
					'param_name'  => 'icon',
				),
				array(
					'heading'    => esc_html__( 'Marker Title', 'businextcoin' ),
					'type'       => 'textfield',
					'param_name' => 'title',
				),
				array(
					'heading'     => esc_html__( 'Marker Information', 'businextcoin' ),
					'description' => esc_html__( 'Content for info window', 'businextcoin' ),
					'type'        => 'textarea',
					'param_name'  => 'info',
				),
			),
		),
		array(
			'heading'     => esc_html__( 'Google Maps API Key (optional)', 'businextcoin' ),
			'description' => sprintf( wp_kses( __( 'Follow <a href="%s" target="_blank">this link</a> and click <strong>GET A KEY</strong> button. If you leave it empty, the API Key will be put in by default from our key.', 'businextcoin' ), array(
				'a'      => array(
					'href'   => array(),
					'target' => array(),
				),
				'strong' => array(),
			) ), esc_url( 'https://developers.google.com/maps/documentation/javascript/get-api-key#get-an-api-key' ) ),
			'type'        => 'textfield',
			'param_name'  => 'api_key',
		),
	),
) );
