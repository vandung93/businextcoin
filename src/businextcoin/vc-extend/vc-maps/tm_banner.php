<?php

class WPBakeryShortCode_TM_Banner extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		Businextcoin_VC::get_vc_spacing_css( $selector, $atts );
	}
}

vc_map( array(
	'name'     => esc_html__( 'Banner', 'businextcoin' ),
	'base'     => 'tm_banner',
	'category' => BUSINEXTCOIN_VC_SHORTCODE_CATEGORY,
	'icon'     => 'insight-i insight-i-product-categories',
	'params'   => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'businextcoin' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( '01', 'businextcoin' ) => '1',
				esc_html__( '02', 'businextcoin' ) => '2',
			),
			'std'         => '1',
		),
		array(
			'heading'    => esc_html__( 'Image', 'businextcoin' ),
			'type'       => 'attach_image',
			'param_name' => 'image',
		),
		array(
			'heading'    => esc_html__( 'Text', 'businextcoin' ),
			'type'       => 'textarea',
			'param_name' => 'text',
		),
		array(
			'heading'    => esc_html__( 'Button', 'businextcoin' ),
			'type'       => 'vc_link',
			'param_name' => 'button',
		),
		Businextcoin_VC::extra_class_field(),
	), Businextcoin_VC::get_vc_spacing_tab() ),
) );
