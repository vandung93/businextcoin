<?php

class WPBakeryShortCode_TM_Attribute_List extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		Businextcoin_VC::get_vc_spacing_css( $selector, $atts );
	}
}

vc_map( array(
	'name'     => esc_html__( 'Attribute List', 'businextcoin' ),
	'base'     => 'tm_attribute_list',
	'category' => BUSINEXTCOIN_VC_SHORTCODE_CATEGORY,
	'icon'     => 'insight-i insight-i-portfoliogrid',
	'params'   => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'businextcoin' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Style 01', 'businextcoin' ) => '01',
				esc_html__( 'Style 02', 'businextcoin' ) => '02',
				esc_html__( 'Style 03', 'businextcoin' ) => '03',
			),
			'std'         => '01',
		),
		array(
			'heading'     => esc_html__( 'Skin', 'businextcoin' ),
			'type'        => 'dropdown',
			'param_name'  => 'skin',
			'value'       => array(
				esc_html__( 'Dark', 'businextcoin' )  => 'dark',
				esc_html__( 'Light', 'businextcoin' ) => 'light',
			),
			'admin_label' => true,
			'std'         => 'light',
		),
		Businextcoin_VC::extra_class_field(),
		array(
			'group'      => esc_html__( 'Attributes', 'businextcoin' ),
			'heading'    => esc_html__( 'Attributes', 'businextcoin' ),
			'type'       => 'param_group',
			'param_name' => 'attributes',
			'params'     => array(
				array(
					'heading'     => esc_html__( 'Name', 'businextcoin' ),
					'type'        => 'textfield',
					'param_name'  => 'name',
					'admin_label' => true,
				),
				array(
					'heading'    => esc_html__( 'Value', 'businextcoin' ),
					'type'       => 'textarea',
					'param_name' => 'value',
				),
			),
		),
	), Businextcoin_VC::get_vc_spacing_tab(), Businextcoin_VC::get_custom_style_tab() ),
) );

