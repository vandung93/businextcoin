<?php

class WPBakeryShortCode_TM_Twitter extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		global $businextcoin_shortcode_lg_css;

		$icon_tmp = Businextcoin_Helper::get_shortcode_css_color_inherit( 'color', $atts['icon_color'], $atts['custom_icon_color'] );
		$text_tmp = Businextcoin_Helper::get_shortcode_css_color_inherit( 'color', $atts['text_color'], $atts['custom_text_color'] );

		if ( $icon_tmp !== '' ) {
			$businextcoin_shortcode_lg_css .= "$selector .tweet:before{ $icon_tmp }";
		}

		if ( $text_tmp !== '' ) {
			$businextcoin_shortcode_lg_css .= "$selector .tweet{ $text_tmp }";
		}

		Businextcoin_VC::get_vc_spacing_css( $selector, $atts );
	}
}

$slider_tab  = esc_html__( 'Slider Settings', 'businextcoin' );
$styling_tab = esc_html__( 'Styling', 'businextcoin' );

vc_map( array(
	'name'                      => esc_html__( 'Twitter', 'businextcoin' ),
	'base'                      => 'tm_twitter',
	'category'                  => BUSINEXTCOIN_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-twitter',
	'allowed_container_element' => 'vc_row',
	'params'                    => array_merge( array(
		array(
			'heading'     => esc_html__( 'Widget title', 'businextcoin' ),
			'description' => esc_html__( 'What text use as a widget title.', 'businextcoin' ),
			'type'        => 'textfield',
			'param_name'  => 'widget_title',
		),
		array(
			'type'        => 'dropdown',
			'heading'     => esc_html__( 'Style', 'businextcoin' ),
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'List', 'businextcoin' )         => 'list',
				esc_html__( 'Slider', 'businextcoin' )       => 'slider',
				esc_html__( 'Slider Quote', 'businextcoin' ) => 'slider-quote',
			),
			'std'         => 'slider-quote',
		),
		array(
			'heading'    => esc_html__( 'Consumer Key', 'businextcoin' ),
			'type'       => 'textfield',
			'param_name' => 'consumer_key',
		),
		array(
			'heading'    => esc_html__( 'Consumer Secret', 'businextcoin' ),
			'type'       => 'textfield',
			'param_name' => 'consumer_secret',
		),
		array(
			'heading'    => esc_html__( 'Access Token', 'businextcoin' ),
			'type'       => 'textfield',
			'param_name' => 'access_token',
		),
		array(
			'heading'    => esc_html__( 'Access Token Secret', 'businextcoin' ),
			'type'       => 'textfield',
			'param_name' => 'access_token_secret',
		),
		array(
			'heading'    => esc_html__( 'Twitter Username', 'businextcoin' ),
			'type'       => 'textfield',
			'param_name' => 'username',
		),
		array(
			'heading'    => esc_html__( 'Number of tweets', 'businextcoin' ),
			'type'       => 'number',
			'param_name' => 'number_items',
		),
		array(
			'heading'    => esc_html__( 'Heading', 'businextcoin' ),
			'type'       => 'textfield',
			'param_name' => 'heading',
			'std'        => esc_html__( 'From Twitter', 'businextcoin' ),
		),
		array(
			'heading'    => esc_html__( 'Show date.', 'businextcoin' ),
			'type'       => 'checkbox',
			'param_name' => 'show_date',
			'value'      => array(
				esc_html__( 'Yes', 'businextcoin' ) => '1',
			),
		),
		Businextcoin_VC::extra_class_field(),
		array(
			'group'       => $slider_tab,
			'heading'     => esc_html__( 'Speed', 'businextcoin' ),
			'description' => esc_html__( 'Duration of transition between slides (in ms), ex: 1000. Leave blank to use default.', 'businextcoin' ),
			'type'        => 'number',
			'suffix'      => 'ms',
			'param_name'  => 'carousel_speed',
			'dependency'  => array(
				'element' => 'style',
				'value'   => array(
					'slider',
					'slider-quote',
				),
			),
		),
		array(
			'group'       => $slider_tab,
			'heading'     => esc_html__( 'Auto Play', 'businextcoin' ),
			'description' => esc_html__( 'Delay between transitions (in ms), ex: 3000. Leave blank to disabled.', 'businextcoin' ),
			'type'        => 'number',
			'suffix'      => 'ms',
			'param_name'  => 'carousel_auto_play',
			'dependency'  => array(
				'element' => 'style',
				'value'   => array(
					'slider',
					'slider-quote',
				),
			),
		),
		array(
			'group'      => $slider_tab,
			'heading'    => esc_html__( 'Navigation', 'businextcoin' ),
			'type'       => 'dropdown',
			'param_name' => 'carousel_nav',
			'value'      => Businextcoin_VC::get_slider_navs(),
			'std'        => '',
			'dependency' => array(
				'element' => 'style',
				'value'   => array(
					'slider',
					'slider-quote',
				),
			),
		),
		Businextcoin_VC::extra_id_field( array(
			'group'      => $slider_tab,
			'heading'    => esc_html__( 'Slider Button ID', 'businextcoin' ),
			'param_name' => 'slider_button_id',
			'dependency' => array(
				'element' => 'carousel_nav',
				'value'   => array(
					'custom',
				),
			),
		) ),
		array(
			'group'      => $slider_tab,
			'heading'    => esc_html__( 'Pagination', 'businextcoin' ),
			'type'       => 'dropdown',
			'param_name' => 'carousel_pagination',
			'value'      => Businextcoin_VC::get_slider_dots(),
			'std'        => '',
			'dependency' => array(
				'element' => 'style',
				'value'   => array(
					'slider',
					'slider-quote',
				),
			),
		),
		array(
			'group'            => $styling_tab,
			'heading'          => esc_html__( 'Icon Color', 'businextcoin' ),
			'type'             => 'dropdown',
			'param_name'       => 'icon_color',
			'value'            => array(
				esc_html__( 'Default Color', 'businextcoin' )   => '',
				esc_html__( 'Primary Color', 'businextcoin' )   => 'primary',
				esc_html__( 'Secondary Color', 'businextcoin' ) => 'secondary',
				esc_html__( 'Custom Color', 'businextcoin' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $styling_tab,
			'heading'          => esc_html__( 'Custom Icon Color', 'businextcoin' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_icon_color',
			'dependency'       => array(
				'element' => 'icon_color',
				'value'   => 'custom',
			),
			'std'              => '#999',
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $styling_tab,
			'heading'          => esc_html__( 'Text Color', 'businextcoin' ),
			'type'             => 'dropdown',
			'param_name'       => 'text_color',
			'value'            => array(
				esc_html__( 'Default Color', 'businextcoin' )   => '',
				esc_html__( 'Primary Color', 'businextcoin' )   => 'primary',
				esc_html__( 'Secondary Color', 'businextcoin' ) => 'secondary',
				esc_html__( 'Custom Color', 'businextcoin' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $styling_tab,
			'heading'          => esc_html__( 'Custom Text Color', 'businextcoin' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_text_color',
			'dependency'       => array(
				'element' => 'text_color',
				'value'   => array( 'custom' ),
			),
			'std'              => '#999',
			'edit_field_class' => 'vc_col-sm-6',
		),
	), Businextcoin_VC::get_vc_spacing_tab() ),
) );
