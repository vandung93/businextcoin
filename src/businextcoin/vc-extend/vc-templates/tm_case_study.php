<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}
$post_type  = 'case_study';
$style      = $el_class = $order = $animation = $filter_wrap = $filter_enable = $filter_type = $filter_align = $filter_counter = $pagination_align = $pagination_button_text = '';
$gutter     = 0;
$main_query = '';

$atts   = vc_map_get_attributes( $this->getShortcode(), $atts );
$css_id = uniqid( 'tm-case-study-' );
$this->get_inline_css( "#$css_id", $atts );
Businextcoin_VC::get_shortcode_custom_css( "#$css_id", $atts );
extract( $atts );

$businextcoin_post_args = array(
	'post_type'      => $post_type,
	'posts_per_page' => $number,
	'orderby'        => $orderby,
	'order'          => $order,
	'paged'          => 1,
	'post_status'    => 'publish',
);

if ( in_array( $orderby, array( 'meta_value', 'meta_value_num' ), true ) ) {
	$businextcoin_post_args['meta_key'] = $meta_key;
}

if ( get_query_var( 'paged' ) ) {
	$businextcoin_post_args['paged'] = get_query_var( 'paged' );
} elseif ( get_query_var( 'page' ) ) {
	$businextcoin_post_args['paged'] = get_query_var( 'page' );
}

$businextcoin_post_args = Businextcoin_VC::get_tax_query_of_taxonomies( $businextcoin_post_args, $taxonomies );

if ( $main_query === '1' ) {
	global $wp_query;
	$businextcoin_query = $wp_query;
} else {
	$businextcoin_query = new WP_Query( $businextcoin_post_args );
}

$el_class = $this->getExtraClass( $el_class );

$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'tm-case-study ' . $el_class, $this->settings['base'], $atts );
$css_class .= " style-$style";

if ( $filter_wrap === '1' ) {
	$css_class .= ' filter-wrap';
}

$grid_classes = 'tm-grid';

if ( in_array( $style, array( 'grid', 'grid-caption' ), true ) ) {
	$grid_classes .= ' modern-grid';
}

$grid_classes .= Businextcoin_Helper::get_grid_animation_classes( $animation );
?>
<?php if ( $businextcoin_query->have_posts() ) : ?>
	<div class="tm-grid-wrapper <?php echo esc_attr( trim( $css_class ) ); ?>" id="<?php echo esc_attr( $css_id ); ?>"
		<?php if ( $pagination !== '' && $businextcoin_query->found_posts > $number ) : ?>
			data-pagination="<?php echo esc_attr( $pagination ); ?>"
		<?php endif; ?>

		 data-filter-type="<?php echo esc_attr( $filter_type ); ?>"
	>
		<?php
		$count = $businextcoin_query->post_count;

		$tm_grid_query                  = $businextcoin_post_args;
		$tm_grid_query['action']        = "{$post_type}_infinite_load";
		$tm_grid_query['max_num_pages'] = $businextcoin_query->max_num_pages;
		$tm_grid_query['found_posts']   = $businextcoin_query->found_posts;
		$tm_grid_query['taxonomies']    = $taxonomies;
		$tm_grid_query['style']         = $style;
		$tm_grid_query['image_size']    = $image_size;
		$tm_grid_query['pagination']    = $pagination;
		$tm_grid_query['count']         = $count;
		$tm_grid_query                  = htmlspecialchars( wp_json_encode( $tm_grid_query ) );
		?>

		<?php Businextcoin_Templates::grid_filters( $post_type, $filter_enable, $filter_align, $filter_counter, $filter_wrap, $businextcoin_query->found_posts ); ?>

		<input type="hidden" class="tm-grid-query" <?php echo 'value="' . $tm_grid_query . '"'; ?>/>

		<div class="<?php echo esc_attr( $grid_classes ); ?>">

			<?php
			set_query_var( 'businextcoin_query', $businextcoin_query );
			set_query_var( 'count', $count );
			set_query_var( 'image_size', $image_size );

			get_template_part( 'loop/shortcodes/case-study/style', $style );
			?>

		</div>

		<?php Businextcoin_Templates::grid_pagination( $businextcoin_query, $number, $pagination, $pagination_align, $pagination_button_text ); ?>

	</div>
<?php endif; ?>
<?php wp_reset_postdata();
