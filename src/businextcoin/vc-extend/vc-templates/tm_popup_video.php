<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}
$style = $el_class = $video = $video_text = $poster = $image_size = $image_size_width = $image_size_height = '';

$atts   = vc_map_get_attributes( $this->getShortcode(), $atts );
$css_id = uniqid( 'tm-popup-video-' );
$this->get_inline_css( "#$css_id", $atts );
Businextcoin_VC::get_shortcode_custom_css( "#$css_id", $atts );
extract( $atts );

$el_class  = $this->getExtraClass( $el_class );
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'tm-popup-video ' . $el_class, $this->settings['base'], $atts );
$css_class .= " style-$style";

$css_class .= Businextcoin_Helper::get_animation_classes();

wp_enqueue_style( 'lightgallery' );
wp_enqueue_script( 'lightgallery' );
?>
<?php if ( $video !== '' ) : ?>
	<div class="<?php echo esc_attr( trim( $css_class ) ); ?>" id="<?php echo esc_attr( $css_id ); ?>">

		<?php if ( in_array( $style, array( 'poster-02', 'poster-03' ) ) ): ?>
			<div class="video-mark">
				<div class="wave-pulse wave-pulse-1"></div>
				<div class="wave-pulse wave-pulse-2"></div>
				<div class="wave-pulse wave-pulse-3"></div>
			</div>
		<?php endif; ?>

		<a href="<?php echo esc_url( $video ); ?>">
			<?php if ( in_array( $style, array( 'poster-01' ) ) ) { ?>
				<div class="video-poster">
					<?php
					Businextcoin_Helper::get_attachment_by_id( $poster, $image_size, $image_size_width, $image_size_height );
					?>
				</div>
				<div class="video-overlay">
					<div class="video-button">
						<div class="video-play">
							<i class="ion-ios-play"></i>
						</div>

						<?php if ( $video_text !== '' ) : ?>
							<div class="video-text">
								<?php echo esc_html( $video_text ); ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
			<?php } elseif ( in_array( $style, array( 'poster-02', 'poster-03' ) ) ) { ?>
				<div class="video-poster">
					<?php
					Businextcoin_Helper::get_attachment_by_id( $poster, $image_size, $image_size_width, $image_size_height );
					?>
				</div>
				<div class="video-overlay">
					<div class="video-button">
						<div class="video-play">
							<i class="ion-ios-play"></i>
						</div>
					</div>
				</div>
			<?php } else { ?>
				<div class="video-play">
					<i class="ion-ios-play"></i>
				</div>
			<?php } ?>
		</a>
	</div>
<?php endif;
