<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}
$el_class = $style = $wrap_style = '';

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$el_class  = $this->getExtraClass( $el_class );
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'tm-loan-form ' . $el_class, $this->settings['base'], $atts );

$css_class .= " style-$style";

$css_class .= Businextcoin_Helper::get_animation_classes();

wp_enqueue_style( 'jquery-ui-style' );
wp_enqueue_script( 'jquery-ui-slider' );
wp_enqueue_script( 'loan-form' );
?>
<div class="<?php echo esc_attr( trim( $css_class ) ); ?>">
	<div class="form-wrap">
		<div class="form-item">
			<input type="text" name="mount" value="10,000" placeholder="10,000" id="overdraft-amount-input"/>
		</div>

		<div class="form-submit">
			<button type="submit" id="overdraft-submit"><?php esc_html_e( 'Next Step', 'businextcoin' ); ?></button>
		</div>
	</div>
</div>
