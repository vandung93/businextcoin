<?php
extract( $businextcoin_shortcode_atts );
?>

<div class="content-header">
	<div class="photo">
		<?php
		Businextcoin_Helper::get_lazy_load_image_by_id( $photo, array(
			'width'  => 160,
			'height' => 160,
			'crop'   => true,
			'echo'   => true,
		) );
		?>
	</div>

	<div class="content-main">
		<h3 class="name">
			<?php
			if ( $profile != '' ) {
				echo '<a href="' . esc_attr( $profile ) . '">';
				echo esc_html( $name );
				echo '</a>';
			} else {
				echo esc_html( $name );
			}
			?>
		</h3>

		<?php if ( $position !== '' ) : ?>
			<div class="position"><?php echo esc_html( $position ); ?></div>
		<?php endif; ?>
	</div>
</div>

<div class="content-body">
	<?php if ( $desc !== '' ) : ?>
		<div class="description">
			<?php echo wp_kses( $desc, 'businextcoin-default' ); ?>
		</div>
	<?php endif; ?>

	<?php Businextcoin_Templates::get_team_member_social_networks_template( $social_networks, $tooltip_enable, $tooltip_position, $tooltip_skin ); ?>
</div>
