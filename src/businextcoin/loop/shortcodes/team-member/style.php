<?php
extract( $businextcoin_shortcode_atts );
?>
<div class="photo">
	<?php
	Businextcoin_Helper::get_lazy_load_image_by_id( $photo, array(
		'width'  => 160,
		'height' => 160,
		'crop'   => true,
		'echo'   => true,
	) );
	?>
</div>
<div class="info">
	<h3 class="name">
		<?php
		if ( $profile != '' ) {
			echo '<a href="' . esc_attr( $profile ) . '">';
			echo esc_html( $name );
			echo '</a>';
		} else {
			echo esc_html( $name );
		}
		?>
	</h3>

	<?php if ( $position !== '' ) : ?>
		<div class="position"><?php echo esc_html( $position ); ?></div>
	<?php endif; ?>

	<?php Businextcoin_Templates::get_team_member_social_networks_template( $social_networks, $tooltip_enable, $tooltip_position, $tooltip_skin ); ?>
</div>

<div class="overlay">
	<div class="overlay-outer">
		<div class="overlay-inner">
			<h3 class="name">
				<?php
				if ( $profile != '' ) {
					echo '<a href="' . esc_attr( $profile ) . '">';
					echo esc_html( $name );
					echo '</a>';
				} else {
					echo esc_html( $name );
				}
				?>
			</h3>

			<?php if ( $position !== '' ) : ?>
				<div class="position"><?php echo esc_html( $position ); ?></div>
			<?php endif; ?>

			<?php if ( $desc !== '' ) : ?>
				<div class="description">
					<?php echo wp_kses( $desc, 'businextcoin-default' ); ?>
				</div>
			<?php endif; ?>

			<?php Businextcoin_Templates::get_team_member_social_networks_template( $social_networks, $tooltip_enable, $tooltip_position, $tooltip_skin ); ?>
		</div>
	</div>
</div>
