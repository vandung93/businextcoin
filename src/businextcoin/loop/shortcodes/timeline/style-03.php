<?php
$filter_array = array();
foreach ( $items as $key => $row ) {
	$filter_array[] = $row['datetime'];
}

array_multisort( $filter_array, SORT_ASC, $items );
?>
<ul class="tm-animation-queue" data-animation-delay="400">
	<?php foreach ( $items as $item ) { ?>
		<li class="item">
			<div class="dot"></div>
			<div class="content-wrap">
				<div class="content-body">
					<?php if ( isset( $item['image'] ) ) : ?>
						<div class="photo">
							<?php
							Businextcoin_Helper::get_lazy_load_image_by_id( $item['image'], array(
								'width'  => 460,
								'height' => 250,
								'crop'   => true,
								'echo'   => true,
							) );
							?>
						</div>
					<?php endif; ?>

					<?php if ( isset( $item['title'] ) ) : ?>
						<div class="content-header">
							<h6 class="heading"><?php echo esc_html( $item['title'] ); ?></h6>
						</div>
					<?php endif; ?>

					<?php if ( isset( $item['text'] ) ) : ?>
						<div class="text">
							<?php echo wp_kses( $item['text'], 'businextcoin-default' ); ?>
						</div>
					<?php endif; ?>
				</div>
			</div>

			<?php if ( isset( $item['datetime'] ) ): ?>
				<?php
				$month = date( 'd - F', strtotime( $item['datetime'] ) );
				$year  = date( 'Y', strtotime( $item['datetime'] ) );
				?>
				<div class="date-wrap">
					<div class="month">
						<?php echo esc_html( $month ); ?>
					</div>
					<div class="year">
						<?php echo esc_html( $year ); ?>
					</div>
				</div>
			<?php endif; ?>
		</li>
	<?php } ?>
</ul>
