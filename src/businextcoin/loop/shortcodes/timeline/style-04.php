<?php
$filter_array = array();
foreach ( $items as $key => $row ) {
	$filter_array[] = $row['datetime'];
}

array_multisort( $filter_array, SORT_ASC, $items );
?>
<div class="center-line"></div>
<div class="tm-swiper nav-style-3"
     data-lg-items="auto"
     data-lg-gutter="30"
     data-nav="1"
     data-autoplay="0"
>
	<div class="swiper-container">
		<div class="swiper-wrapper">
			<?php foreach ( $items as $item ) { ?>
				<div class="swiper-slide">
					<div class="item">
						<div class="dot"></div>
						<div class="content-wrap">
							<div class="content-body">
								<?php if ( isset( $item['image'] ) ) : ?>
									<div class="photo">
										<?php
										Businextcoin_Helper::get_lazy_load_image_by_id( $item['image'], array(
											'width'  => 460,
											'height' => 250,
											'crop'   => true,
											'echo'   => true,
										) );
										?>
									</div>
								<?php endif; ?>

								<div class="content-header">
									<?php
									$icon_type = isset( $item['icon_type'] ) ? $item['icon_type'] : 'none';
									$icon      = isset( $item["icon_$icon_type"] ) ? $item["icon_$icon_type"] : '';
									?>
									<?php if ( $icon !== '' ) { ?>
										<?php
										$_args = array(
											'type' => $icon_type,
											'icon' => $icon,
										);

										Businextcoin_Helper::get_vc_icon_template( $_args );
										?>
									<?php } ?>

									<?php if ( isset( $item['title'] ) ) : ?>
										<div class="heading-wrap">
											<h6 class="heading"><?php echo esc_html( $item['title'] ); ?></h6>
										</div>
									<?php endif; ?>
								</div>

								<?php if ( isset( $item['text'] ) ) : ?>
									<div class="text">
										<?php echo wp_kses( $item['text'], 'businextcoin-default' ); ?>
									</div>
								<?php endif; ?>
							</div>
						</div>

						<div class="date-wrap">
							<?php if ( isset( $item['datetime'] ) ): ?>
								<?php
								$month = date( 'd - F', strtotime( $item['datetime'] ) );
								$year  = date( 'Y', strtotime( $item['datetime'] ) );
								?>
								<div class="date-wrap">
									<h6 class="month secondary-font">
										<?php echo esc_html( $month ); ?>
									</h6>
									<h6 class="year">
										<?php echo esc_html( $year ); ?>
									</h6>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>

<script>
    (
        function ( $ ) {
            function calculateHeightOfTimeLine() {
                var maxHeight = 0;
                var timeline  = $( '.tm-timeline.style-04' );
                timeline.find( '.content-wrap' ).each( function () {
                    if ( $( this ).outerHeight() > maxHeight ) {
                        maxHeight = $( this ).outerHeight();
                    }
                } );

                timeline.find( '.item' ).css( {
                    'height': maxHeight * 2 + 170
                } );
            }

            $( document ).ready( function ( $ ) {
                calculateHeightOfTimeLine();
            } );

            $( 'window' ).on( 'resize', function () {
                calculateHeightOfTimeLine();
            } );

        }( jQuery )
    );
</script>
