<?php
$filter_array = array();
foreach ( $items as $key => $row ) {
	$filter_array[] = $row['datetime'];
}

array_multisort( $filter_array, SORT_ASC, $items );
?>
<div class="center-line"></div>
<div class="tm-swiper nav-style-3"
     data-lg-items="auto"
     data-nav="1"
     data-autoplay="0"
>
	<div class="swiper-container">
		<div class="swiper-wrapper">
			<?php foreach ( $items as $item ) { ?>
				<div class="swiper-slide">
					<div class="item">
						<div class="dot"></div>
						<div class="content-wrap">
							<div class="content-body">
								<?php if ( isset( $item['image'] ) ) : ?>
									<div class="photo">
										<?php
										Businextcoin_Helper::get_lazy_load_image_by_id( $item['image'], array(
											'width'  => 460,
											'height' => 250,
											'crop'   => true,
											'echo'   => true,
										) );
										?>
									</div>
								<?php endif; ?>

								<?php
								$icon_type = isset( $item['icon_type'] ) ? $item['icon_type'] : 'none';
								$icon      = isset( $item["icon_$icon_type"] ) ? $item["icon_$icon_type"] : '';
								?>
								<?php if ( $icon !== '' ) { ?>
									<?php
									$_args = array(
										'type' => $icon_type,
										'icon' => $icon,
									);

									Businextcoin_Helper::get_vc_icon_template( $_args );
									?>
								<?php } ?>

								<?php if ( isset( $item['datetime'] ) ): ?>
									<div class="date">
										<?php echo esc_html( date( 'm.d.Y', strtotime( $item['datetime'] ) ) ); ?>
									</div>
								<?php endif; ?>

								<?php if ( isset( $item['title'] ) ) : ?>
									<h6 class="heading"><?php echo esc_html( $item['title'] ); ?></h6>
								<?php endif; ?>

								<?php if ( isset( $item['text'] ) ) : ?>
									<div class="text">
										<?php echo wp_kses( $item['text'], 'businextcoin-default' ); ?>
									</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>

<script>
    (
        function ( $ ) {
            function calculateHeightOfTimeLine() {
                var maxHeight = 0;
                var timeline  = $( '.tm-timeline.style-05' );
                timeline.find( '.content-wrap' ).each( function () {
                    if ( $( this ).outerHeight() > maxHeight ) {
                        maxHeight = $( this ).outerHeight();
                    }
                } );

                timeline.find( '.item' ).css( {
                    'height': (
                                  maxHeight + 34
                              ) * 2
                } );
            }

            $( document ).ready( function ( $ ) {
                calculateHeightOfTimeLine();
            } );

            $( 'window' ).on( 'resize', function () {
                calculateHeightOfTimeLine();
            } );

        }( jQuery )
    );
</script>
