<?php
foreach ( $images as $image ) {
	$classes = array( 'gallery-item grid-item' );

	$image_full = Businextcoin_Helper::get_attachment_info( $image );

	if ( $image_full === false ) {
		continue;
	}

	$_width  = 480;
	$_height = 480;
	if ( $image_size !== '' ) {
		$_sizes  = explode( 'x', $image_size );
		$_width  = $_sizes[0];
		$_height = $_sizes[1];
	}

	$image_url = Businextcoin_Helper::aq_resize( array(
		'url'    => $image_full['src'],
		'width'  => $_width,
		'height' => $_height,
		'crop'   => true,
	) );

	$_sub_html = '';
	if ( $image_full['title'] !== '' ) {
		$_sub_html .= "<h4>{$image_full['title']}</h4>";
	}

	if ( $image_full['caption'] !== '' ) {
		$_sub_html .= "<p>{$image_full['caption']}</p>";
	}
	?>
	<div class="<?php echo esc_attr( implode( ' ', $classes ) ); ?>">
		<a href="<?php echo esc_url( $image_full['src'] ); ?>" class="zoom"
		   data-sub-html="<?php echo esc_attr( $_sub_html ); ?>">
			<img src="<?php echo esc_url( $image_url ); ?>" alt="<?php esc_attr_e( 'Gallery Image', 'businextcoin' ); ?>">
			<div class="overlay">
				<div><span class="ion-ios-plus-empty"></span></div>
			</div>
		</a>
	</div>
	<?php
}
