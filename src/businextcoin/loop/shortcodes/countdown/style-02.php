<?php
extract( $businextcoin_shortcode_atts );

wp_enqueue_script( 'time-circle' );
wp_enqueue_script( 'businextcoin-countdown' );

$circle_bg     = 'rgba(0, 0, 0, 0.3)';
$primary_color = Businextcoin::setting( 'primary_color' );
if ( $skin === 'light' ) {
	$circle_bg = 'rgba(255, 255, 255, 0.2)';
}

$days    = isset( $days ) && $days !== '' ? $days : esc_html__( 'Days', 'businextcoin' );
$hours   = isset( $hours ) && $hours !== '' ? $hours : esc_html__( 'Hours', 'businextcoin' );
$minutes = isset( $minutes ) && $minutes !== '' ? $minutes : esc_html__( 'Minutes', 'businextcoin' );
$seconds = isset( $seconds ) && $seconds !== '' ? $seconds : esc_html__( 'Seconds', 'businextcoin' );
?>

<div class="countdown"
	<?php ?>
	 data-animation="time-circle"
	 data-date="<?php echo esc_attr( $datetime ); ?>"
	 data-circle-background="rgba(255, 255, 255, 0.3)"
	 data-time-color="<?php echo esc_attr( $primary_color ); ?>"
	 data-days-text="<?php echo esc_attr( $days ); ?>"
	 data-hours-text="<?php echo esc_attr( $hours ); ?>"
	 data-minites-text="<?php echo esc_attr( $minutes ); ?>"
	 data-seconds-text="<?php echo esc_attr( $seconds ); ?>"
>
</div>
