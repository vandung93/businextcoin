<?php
while ( $businextcoin_query->have_posts() ) :
	$businextcoin_query->the_post();
	$classes = array( 'grid-item', 'post-item' );
	?>
	<div <?php post_class( implode( ' ', $classes ) ); ?>>

		<?php get_template_part( 'loop/blog/format' ); ?>

		<div class="post-info">

			<?php get_template_part( 'loop/blog/title' ); ?>

			<div class="post-meta">

				<div class="post-date">
					<span class="ion-clock meta-icon"></span>
					<?php echo get_the_date(); ?>
				</div>

				<?php get_template_part( 'loop/blog/category' ); ?>

				<div class="post-author-meta">
					<?php echo esc_html__( 'Post by :', 'businextcoin' ) . ' '; ?><a
						href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><?php the_author(); ?></a>
				</div>

				<div class="post-comments-number">
					<span class="ion-ios-chatboxes-outline meta-icon"></span>
					<?php
					$comment_count = get_comments_number();
					if ( $comment_count > 0 ) {
						$comment_count = str_pad( get_comments_number(), 2, '0', STR_PAD_LEFT );
					}

					$comment_count .= $comment_count > 1 ? esc_html__( ' Comments', 'businextcoin' ) : esc_html__( ' Comment', 'businextcoin' );
					?>
					<a href="<?php echo esc_url( get_the_permalink() . '#comments' ); ?>"
					   class="smooth-scroll-link"><?php echo esc_html( $comment_count ); ?></a>
				</div>

				<?php get_template_part( 'loop/blog/sticky' ); ?>
			</div>

			<div class="post-excerpt">
				<?php Businextcoin_Templates::excerpt( array(
					'limit' => 100,
					'type'  => 'word',
				) ); ?>
			</div>

			<?php get_template_part( 'loop/blog/readmore' ); ?>
		</div>
	</div>
<?php endwhile;
