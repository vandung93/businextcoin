<?php
while ( $businextcoin_query->have_posts() ) :
	$businextcoin_query->the_post();
	$classes = array( 'post-item grid-item' );

	$post_options = unserialize( get_post_meta( get_the_ID(), 'insight_post_options', true ) );
	$_media_dark  = Businextcoin_Helper::get_the_post_meta( $post_options, 'post_media_dark', '' );
	$_media_light = Businextcoin_Helper::get_the_post_meta( $post_options, 'post_media_light', '' );
	?>
	<div <?php post_class( implode( ' ', $classes ) ); ?>>

		<div class="post-item-wrap">
			<div class="tm-rotate-box">
				<div class="flipper to-left">
					<div class="thumb-wrap">
						<div class="box front">
							<div class="inner">
								<?php if ( $_media_light !== '' ): ?>
									<div class="media">
										<img src="<?php echo esc_url( $_media_light ); ?>" alt="<?php esc_attr_e( 'Media Image', 'businextcoin' ); ?>"/>
									</div>
								<?php endif; ?>

								<div class="post-info">
									<?php get_template_part( 'loop/blog/title' ); ?>
								</div>
							</div>
						</div>
						<div class="box back">
							<div class="inner">
								<?php if ( $_media_light !== '' ): ?>
									<div class="media">
										<img src="<?php echo esc_url( $_media_light ); ?>" alt="<?php esc_attr_e( 'Media Image', 'businextcoin' ); ?>"/>
									</div>
								<?php endif; ?>

								<div class="post-info">
									<?php get_template_part( 'loop/blog/title' ); ?>

									<div class="post-read-more">
										<a href="<?php the_permalink(); ?>">
										<span class="btn-text">
											<?php esc_html_e( 'Read More', 'businextcoin' ); ?>
										</span>
											<span class="btn-icon ion-arrow-right-c"></span>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
<?php endwhile;
