<?php
while ( $businextcoin_query->have_posts() ) :
	$businextcoin_query->the_post();
	$classes = array( 'post-item swiper-slide' );

	$post_options = unserialize( get_post_meta( get_the_ID(), 'insight_post_options', true ) );
	$_media_dark  = Businextcoin_Helper::get_the_post_meta( $post_options, 'post_media_dark', '' );
	$_media_light = Businextcoin_Helper::get_the_post_meta( $post_options, 'post_media_light', '' );
	?>
	<div <?php post_class( implode( ' ', $classes ) ); ?>>
		<div class="swiper-item">
			<div class="post-item-wrap">
				<div class="post-feature-wrap">
					<?php get_template_part( 'loop/blog-classic-03/format' ); ?>
				</div>

				<?php if ( $skin === 'light' && $_media_light !== '' ) { ?>
					<div class="media">
						<img src="<?php echo esc_url( $_media_light ); ?>"
						     alt="<?php esc_attr_e( 'Slide Image', 'businextcoin' ); ?>"/>
					</div>
				<?php } elseif ( $skin === 'dark' && $_media_dark !== '' ) { ?>
					<div class="media">
						<img src="<?php echo esc_url( $_media_dark ); ?>"
						     alt="<?php esc_attr_e( 'Slide Image', 'businextcoin' ); ?>"/>
					</div>
				<?php } ?>

				<div class="post-info">

					<?php get_template_part( 'loop/blog/title' ); ?>

					<div class="post-excerpt">
						<?php Businextcoin_Templates::excerpt( array(
							'limit' => 100,
							'type'  => 'character',
						) ); ?>
					</div>

					<a href="<?php the_permalink(); ?>" class="post-read-more">
						<span class="button-text"><?php esc_html_e( 'Read More', 'businextcoin' ); ?></span>
						<span class="button-icon ion-arrow-right-c"></span>
					</a>
				</div>
			</div>
		</div>
	</div>
<?php endwhile;
