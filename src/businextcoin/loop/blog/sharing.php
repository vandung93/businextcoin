<?php
$social_sharing = Businextcoin::setting( 'social_sharing_item_enable' );
if ( ! empty( $social_sharing ) ) {
	?>
	<div class="post-share">
		<div class="post-share-toggle">
			<span class="ion-android-share-alt"></span>
			<div class="post-share-list">
				<?php Businextcoin_Templates::get_sharing_list(); ?>
			</div>
		</div>
	</div>
	<?php
}
