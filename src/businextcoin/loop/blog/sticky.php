<?php if ( is_sticky() ): ?>
	<div class="post-sticky">
		<span class="ion-ios-bolt meta-icon"></span>
		<?php esc_html_e( 'Sticky', 'businextcoin' ); ?>
	</div>
<?php endif;
