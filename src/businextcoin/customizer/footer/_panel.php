<?php
$panel    = 'footer';
$priority = 1;

Businextcoin_Kirki::add_section( 'footer', array(
	'title'    => esc_html__( 'General', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'footer_01', array(
	'title'    => esc_html__( 'Style 01', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'footer_02', array(
	'title'    => esc_html__( 'Style 02', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'footer_03', array(
	'title'    => esc_html__( 'Style 03', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'footer_04', array(
	'title'    => esc_html__( 'Style 04', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'footer_simple', array(
	'title'    => esc_html__( 'Footer Simple', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
