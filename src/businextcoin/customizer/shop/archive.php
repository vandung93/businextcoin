<?php
$section  = 'shop_archive';
$priority = 1;
$prefix   = 'shop_archive_';

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'shop_archive_new_days',
	'label'       => esc_html__( 'New Badge (Days)', 'businextcoin' ),
	'description' => esc_html__( 'If the product was published within the newness time frame display the new badge.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '90',
	'choices'     => array(
		'0'  => esc_html__( 'None', 'businextcoin' ),
		'1'  => esc_html__( '1 day', 'businextcoin' ),
		'2'  => esc_html__( '2 days', 'businextcoin' ),
		'3'  => esc_html__( '3 days', 'businextcoin' ),
		'4'  => esc_html__( '4 days', 'businextcoin' ),
		'5'  => esc_html__( '5 days', 'businextcoin' ),
		'6'  => esc_html__( '6 days', 'businextcoin' ),
		'7'  => esc_html__( '7 days', 'businextcoin' ),
		'8'  => esc_html__( '8 days', 'businextcoin' ),
		'9'  => esc_html__( '9 days', 'businextcoin' ),
		'10' => esc_html__( '10 days', 'businextcoin' ),
		'15' => esc_html__( '15 days', 'businextcoin' ),
		'20' => esc_html__( '20 days', 'businextcoin' ),
		'25' => esc_html__( '25 days', 'businextcoin' ),
		'30' => esc_html__( '30 days', 'businextcoin' ),
		'60' => esc_html__( '60 days', 'businextcoin' ),
		'90' => esc_html__( '90 days', 'businextcoin' ),
	),
) );

/*Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'shop_archive_quick_view',
	'label'       => esc_html__( 'Quick View', 'businextcoin' ),
	'description' => esc_html__( 'Turn on to display quick view button', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'businextcoin' ),
		'1' => esc_html__( 'On', 'businextcoin' ),
	),
) );*/

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'shop_archive_compare',
	'label'       => esc_html__( 'Compare', 'businextcoin' ),
	'description' => esc_html__( 'Turn on to display compare button', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'businextcoin' ),
		'1' => esc_html__( 'On', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'shop_archive_wishlist',
	'label'       => esc_html__( 'Wishlist', 'businextcoin' ),
	'description' => esc_html__( 'Turn on to display love button', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'businextcoin' ),
		'1' => esc_html__( 'On', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'number',
	'settings'    => 'shop_archive_number_item',
	'label'       => esc_html__( 'Number items', 'businextcoin' ),
	'description' => esc_html__( 'Controls the number of products display on shop archive page', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 8,
	'choices'     => array(
		'min'  => 1,
		'max'  => 30,
		'step' => 1,
	),
) );
