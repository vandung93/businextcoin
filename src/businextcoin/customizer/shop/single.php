<?php
$section  = 'shop_single';
$priority = 1;
$prefix   = 'single_product_';

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_product_categories_enable',
	'label'       => esc_html__( 'Categories', 'businextcoin' ),
	'description' => esc_html__( 'Turn on to display the categories.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'businextcoin' ),
		'1' => esc_html__( 'On', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_product_tags_enable',
	'label'       => esc_html__( 'Tags', 'businextcoin' ),
	'description' => esc_html__( 'Turn on to display the tags.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'businextcoin' ),
		'1' => esc_html__( 'On', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_product_sharing_enable',
	'label'       => esc_html__( 'Sharing', 'businextcoin' ),
	'description' => esc_html__( 'Turn on to display the sharing.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'businextcoin' ),
		'1' => esc_html__( 'On', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_product_up_sells_enable',
	'label'       => esc_html__( 'Up-sells products', 'businextcoin' ),
	'description' => esc_html__( 'Turn on to display the up-sells products section.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'businextcoin' ),
		'1' => esc_html__( 'On', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_product_related_enable',
	'label'       => esc_html__( 'Related products', 'businextcoin' ),
	'description' => esc_html__( 'Turn on to display the related products section.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'businextcoin' ),
		'1' => esc_html__( 'On', 'businextcoin' ),
	),
) );
