<?php
$panel    = 'shop';
$priority = 1;

Businextcoin_Kirki::add_section( 'shop_archive', array(
	'title'    => esc_html__( 'Shop Archive', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'shop_single', array(
	'title'    => esc_html__( 'Shop Single', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'shopping_cart', array(
	'title'    => esc_html__( 'Shopping Cart', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
