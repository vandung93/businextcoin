<?php
$section  = 'single_service';
$priority = 1;
$prefix   = 'single_service_';

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'select',
	'settings' => 'single_service_style',
	'label'    => esc_html__( 'Style', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '01',
	'choices'  => array(
		'01' => esc_attr__( 'Style 01', 'businextcoin' ),
		'02' => esc_attr__( 'Style 02', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => $prefix . 'comment_enable',
	'label'       => esc_html__( 'Comments', 'businextcoin' ),
	'description' => esc_html__( 'Turn on to display comments on single case study posts.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '0',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'businextcoin' ),
		'1' => esc_html__( 'On', 'businextcoin' ),
	),
) );
