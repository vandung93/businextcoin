<?php
$panel    = 'service';
$priority = 1;

Businextcoin_Kirki::add_section( 'archive_service', array(
	'title'    => esc_html__( 'Service Archive', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'single_service', array(
	'title'    => esc_html__( 'Service Single', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
