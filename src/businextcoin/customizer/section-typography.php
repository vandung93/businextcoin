<?php
$section  = 'typography';
$priority = 1;
$prefix   = 'typography_';

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="desc"><strong class="insight-label insight-label-info">' . esc_html__( 'IMPORTANT NOTE: ', 'businextcoin' ) . '</strong>' . esc_html__( 'This section contains general typography options. Additional typography options for specific areas can be found within other sections. Example: For breadcrumb typography options go to the breadcrumb section.', 'businextcoin' ) . '</div>',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'      => 'kirki_typography',
	'settings'  => 'secondary_font',
	'label'     => esc_html__( 'Secondary Font family', 'businextcoin' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'transport' => 'auto',
	'default'   => array(
		'font-family' => '',
	),
	'choices'   => array(
		'variant' => array(
			'regular',
			'italic',
			'500',
			'500italic',
			'600',
			'600italic',
			'700',
			'700italic',
			'900',
			'900italic',
		),
	),
	'output'    => array(
		array(
			'element' => '
			.tm-heading.modern-with-separator .heading,
			.tm-heading.modern-with-separator-03 .heading,
			.tm-countdown.style-01 .text,
			.secondary-font
			',
		),
	),
) );

/*--------------------------------------------------------------
# Link color
--------------------------------------------------------------*/
Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Link', 'businextcoin' ) . '</div>',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'color',
	'settings'    => 'link_color',
	'label'       => esc_html__( 'Color', 'businextcoin' ),
	'description' => esc_html__( 'Controls the color of all links.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => Businextcoin::HEADING_COLOR,
	'output'      => array(
		array(
			'element'  => '
			a,
			.tm-blog.style-list .post-categories
			',
			'property' => 'color',
		),
		array(
			'element'  => '.tagcloud a:hover',
			'property' => 'color',
			'suffix'   => '!important',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'color',
	'settings'    => 'link_color_hover',
	'label'       => esc_html__( 'Hover Color', 'businextcoin' ),
	'description' => esc_html__( 'Controls the color of all links when hover.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => Businextcoin::PRIMARY_COLOR,
	'output'      => array(
		array(
			'element'  => 'a:hover, a:focus',
			'property' => 'color',
		),
	),
) );

/*--------------------------------------------------------------
# Body Typography
--------------------------------------------------------------*/
Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Body Typography', 'businextcoin' ) . '</div>',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'kirki_typography',
	'settings'    => $prefix . 'body',
	'label'       => esc_html__( 'Font family', 'businextcoin' ),
	'description' => esc_html__( 'These settings control the typography for all body text.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => array(
		'font-family'    => Businextcoin::PRIMARY_FONT,
		'variant'        => 'regular',
		'line-height'    => '1.625',
		'letter-spacing' => '0em',
	),
	'choices'     => array(
		'variant' => array(
			'300',
			'300italic',
			'regular',
			'italic',
			'500',
			'500italic',
			'700',
			'700italic',
			'800',
			'800italic',
			'900',
			'900italic',
		),
	),
	'output'      => array(
		array(
			'element' => 'body, .gmap-marker-wrap',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'color',
	'settings'    => 'body_color',
	'label'       => esc_html__( 'Body Text Color', 'businextcoin' ),
	'description' => esc_html__( 'Controls the color of body text.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#999',
	'output'      => array(
		array(
			'element'  => '
			.top-bar-office-wrapper .office-list a,
			.tm-testimonial,
			body
			',
			'property' => 'color',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'      => 'slider',
	'settings'  => 'body_font_size',
	'label'     => esc_html__( 'Font size', 'businextcoin' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => 16,
	'transport' => 'auto',
	'choices'   => array(
		'min'  => 10,
		'max'  => 50,
		'step' => 1,
	),
	'output'    => array(
		array(
			'element'  => 'body',
			'property' => 'font-size',
			'units'    => 'px',
		),
	),
) );

/*--------------------------------------------------------------
# Heading typography
--------------------------------------------------------------*/
Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Heading Typography', 'businextcoin' ) . '</div>',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'kirki_typography',
	'settings'    => $prefix . 'heading',
	'label'       => esc_html__( 'Font family', 'businextcoin' ),
	'description' => esc_html__( 'These settings control the typography for all heading text.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => array(
		'font-family'    => Businextcoin::SECONDARY_FONT,
		'variant'        => '700',
		'line-height'    => '1.23',
		'letter-spacing' => '0em',
	),
	'choices'     => array(
		'variant' => array(
			'300',
			'300italic',
			'regular',
			'italic',
			'500',
			'500italic',
			'600',
			'600italic',
			'700',
			'700italic',
			'800',
			'800italic',
			'900',
			'900italic',
		),
	),
	'output'      => array(
		array(
			'element' => 'h1,h2,h3,h4,h5,h6,.h1,.h2,.h3,.h4,.h5,.h6,th',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'color',
	'settings'    => 'heading_color',
	'label'       => esc_html__( 'Heading Color', 'businextcoin' ),
	'description' => esc_html__( 'Controls the color of heading.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => Businextcoin::HEADING_COLOR,
	'output'      => array(
		array(
			'element'  => 'h1,h2,h3,h4,h5,h6,.h1,.h2,.h3,.h4,.h5,.h6,caption,th,
			blockquote,
			.heading-color,
			.widget_search .search-submit,
			.widget_product_search .search-submit,
			.comment-nav-links a, .comment-nav-links span,
			.page-pagination a, .page-pagination span,
			.nav-links a:hover,
			.tm-pricing.style-1 .tm-pricing-list,
			.vc_chart.vc_chart .vc_chart-legend li,
			.tm-countdown .number,
			.tm-table caption,
            .tm-card.style-2 .icon,
            .tm-counter.style-02 .number-wrap,
            .tm-pricing.style-2 .tm-pricing-list,
            .tm-social-networks.style-title .item:hover .link-text,
            .portfolio-details-list label,
            .portfolio-share a:hover,
			.woocommerce div.product .woocommerce-tabs ul.tabs li a,
			.woocommerce.single-product #reviews .comment-reply-title,
			.product-sharing-list a:hover
			',
			'property' => 'color',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'slider',
	'settings'    => 'heading_font_sensitive',
	'label'       => esc_html__( 'Font sensitivity', 'businextcoin' ),
	'description' => esc_html__( 'Values below 1 decrease rate of resizing, values above 1 increase rate of resizing.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 0.7,
	'choices'     => array(
		'min'  => 0.5,
		'max'  => 1,
		'step' => 0.05,
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'slider',
	'settings'    => 'h1_font_size',
	'label'       => esc_html__( 'Font size', 'businextcoin' ),
	'description' => esc_html__( 'H1', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 56,
	'transport'   => 'auto',
	'choices'     => array(
		'min'  => 10,
		'max'  => 100,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element'     => 'h1,.h1',
			'property'    => 'font-size',
			'media_query' => '@media (min-width: 1200px)',
			'units'       => 'px',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'slider',
	'settings'    => 'h2_font_size',
	'description' => esc_html__( 'H2', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 36,
	'transport'   => 'auto',
	'choices'     => array(
		'min'  => 10,
		'max'  => 100,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element'     => 'h2,.h2',
			'property'    => 'font-size',
			'media_query' => '@media (min-width: 1200px)',
			'units'       => 'px',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'slider',
	'settings'    => 'h3_font_size',
	'description' => esc_html__( 'H3', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 32,
	'transport'   => 'auto',
	'choices'     => array(
		'min'  => 10,
		'max'  => 100,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element'     => 'h3,.h3',
			'property'    => 'font-size',
			'media_query' => '@media (min-width: 1200px)',
			'units'       => 'px',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'slider',
	'settings'    => 'h4_font_size',
	'description' => esc_html__( 'H4', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 24,
	'transport'   => 'auto',
	'choices'     => array(
		'min'  => 10,
		'max'  => 100,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element'     => 'h4,.h4',
			'property'    => 'font-size',
			'media_query' => '@media (min-width: 1200px)',
			'units'       => 'px',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'slider',
	'settings'    => 'h5_font_size',
	'description' => esc_html__( 'H5', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 20,
	'transport'   => 'auto',
	'choices'     => array(
		'min'  => 10,
		'max'  => 100,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element'     => 'h5,.h5',
			'property'    => 'font-size',
			'media_query' => '@media (min-width: 1200px)',
			'units'       => 'px',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'slider',
	'settings'    => 'h6_font_size',
	'description' => esc_html__( 'H6', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 14,
	'transport'   => 'auto',
	'choices'     => array(
		'min'  => 10,
		'max'  => 100,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element'     => 'h6,.h6',
			'property'    => 'font-size',
			'media_query' => '@media (min-width: 1200px)',
			'units'       => 'px',
		),
	),
) );

/*--------------------------------------------------------------
# Button Color
--------------------------------------------------------------*/
Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Button', 'businextcoin' ) . '</div>',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'button_style',
	'label'    => esc_html__( 'Button Style', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'solid',
	'choices'  => array(
		'solid'    => esc_html__( 'Solid', 'businextcoin' ),
		'gradient' => esc_html__( 'Gradient', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'            => 'multicolor',
	'settings'        => 'button_color',
	'label'           => esc_html__( 'Button Color', 'businextcoin' ),
	'description'     => esc_html__( 'Controls the color of button.', 'businextcoin' ),
	'section'         => $section,
	'priority'        => $priority ++,
	'transport'       => 'auto',
	'choices'         => array(
		'color'      => esc_attr__( 'Color', 'businextcoin' ),
		'background' => esc_attr__( 'Background', 'businextcoin' ),
		'border'     => esc_attr__( 'Border', 'businextcoin' ),
	),
	'default'         => array(
		'color'      => '#111111',
		'background' => Businextcoin::PRIMARY_COLOR,
		'border'     => Businextcoin::PRIMARY_COLOR,
	),
	'output'          => array(
		array(
			'choice'   => 'color',
			'element'  => Businextcoin_Helper::get_button_css_selector(),
			'property' => 'color',
		),
		array(
			'choice'   => 'border',
			'element'  => Businextcoin_Helper::get_button_css_selector(),
			'property' => 'border-color',
		),
		array(
			'choice'   => 'background',
			'element'  => Businextcoin_Helper::get_button_css_selector(),
			'property' => 'background-color',
		),
	),
	'active_callback' => array(
		array(
			'setting'  => 'button_style',
			'operator' => '==',
			'value'    => 'solid',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'            => 'multicolor',
	'settings'        => 'button_hover_color',
	'label'           => esc_html__( 'Button Hover Color', 'businextcoin' ),
	'description'     => esc_html__( 'Controls the color of button when hover.', 'businextcoin' ),
	'section'         => $section,
	'priority'        => $priority ++,
	'transport'       => 'auto',
	'choices'         => array(
		'color'      => esc_attr__( 'Color', 'businextcoin' ),
		'background' => esc_attr__( 'Background', 'businextcoin' ),
		'border'     => esc_attr__( 'Border', 'businextcoin' ),
	),
	'default'         => array(
		'color'      => '#ffffff',
		'background' => Businextcoin::SECONDARY_COLOR,
		'border'     => Businextcoin::SECONDARY_COLOR,
	),
	'output'          => array(
		array(
			'choice'   => 'color',
			'element'  => Businextcoin_Helper::get_button_hover_css_selector(),
			'property' => 'color',
		),
		array(
			'choice'   => 'border',
			'element'  => Businextcoin_Helper::get_button_hover_css_selector(),
			'property' => 'border-color',
		),
		array(
			'choice'   => 'background',
			'element'  => Businextcoin_Helper::get_button_hover_css_selector(),
			'property' => 'background-color',
		),
	),
	'active_callback' => array(
		array(
			'setting'  => 'button_style',
			'operator' => '==',
			'value'    => 'solid',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'            => 'multicolor',
	'settings'        => 'button_gradient_color',
	'label'           => esc_html__( 'Button Gradient Color', 'businextcoin' ),
	'description'     => esc_html__( 'Controls the gradient color of button', 'businextcoin' ),
	'section'         => $section,
	'priority'        => $priority ++,
	'transport'       => 'auto',
	'choices'         => array(
		'color_1'    => esc_attr__( 'Color 1', 'businextcoin' ),
		'color_2'    => esc_attr__( 'Color 2', 'businextcoin' ),
		'text_color' => esc_attr__( 'Text Color', 'businextcoin' ),
	),
	'default'         => array(
		'color_1'    => '#58FFA4',
		'color_2'    => '#0068FF',
		'text_color' => '#fff',
	),
	'active_callback' => array(
		array(
			'setting'  => 'button_style',
			'operator' => '==',
			'value'    => 'gradient',
		),
	),
) );

/*--------------------------------------------------------------
# Form
--------------------------------------------------------------*/
Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Form Input', 'businextcoin' ) . '</div>',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'multicolor',
	'settings'    => 'form_input_color',
	'label'       => esc_html__( 'Form Input Color', 'businextcoin' ),
	'description' => esc_html__( 'Controls the color of form inputs.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'choices'     => array(
		'color'      => esc_attr__( 'Color', 'businextcoin' ),
		'background' => esc_attr__( 'Background', 'businextcoin' ),
		'border'     => esc_attr__( 'Border', 'businextcoin' ),
	),
	'default'     => array(
		'color'      => '#999',
		'background' => '#f7f7f7',
		'border'     => '#eeeeee',
	),
	'output'      => array(
		array(
			'choice'   => 'color',
			'element'  => Businextcoin_Helper::get_form_input_css_selector(),
			'property' => 'color',
		),
		array(
			'choice'   => 'border',
			'element'  => Businextcoin_Helper::get_form_input_css_selector(),
			'property' => 'border-color',
		),
		array(
			'choice'   => 'background',
			'element'  => Businextcoin_Helper::get_form_input_css_selector(),
			'property' => 'background-color',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'multicolor',
	'settings'    => 'form_input_focus_color',
	'label'       => esc_html__( 'Form Input Focus Color', 'businextcoin' ),
	'description' => esc_html__( 'Controls the color of form inputs when focus.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'choices'     => array(
		'color'      => esc_attr__( 'Color', 'businextcoin' ),
		'background' => esc_attr__( 'Background', 'businextcoin' ),
		'border'     => esc_attr__( 'Border', 'businextcoin' ),
	),
	'default'     => array(
		'color'      => Businextcoin::PRIMARY_COLOR,
		'background' => '#ffffff',
		'border'     => Businextcoin::PRIMARY_COLOR,
	),
	'output'      => array(
		array(
			'choice'   => 'color',
			'element'  => Businextcoin_Helper::get_form_input_focus_css_selector(),
			'property' => 'color',
		),
		array(
			'choice'   => 'border',
			'element'  => Businextcoin_Helper::get_form_input_focus_css_selector(),
			'property' => 'border-color',
		),
		array(
			'choice'   => 'background',
			'element'  => Businextcoin_Helper::get_form_input_focus_css_selector(),
			'property' => 'background-color',
		),
	),
) );
