<?php
$panel    = 'top_bar';
$priority = 1;

Businextcoin_Kirki::add_section( 'top_bar', array(
	'title'    => esc_html__( 'General', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'top_bar_style_01', array(
	'title'    => esc_html__( 'Top Bar Style 01', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'top_bar_style_02', array(
	'title'    => esc_html__( 'Top Bar Style 02', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'top_bar_style_03', array(
	'title'    => esc_html__( 'Top Bar Style 03', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'top_bar_style_04', array(
	'title'    => esc_html__( 'Top Bar Style 04', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'top_bar_style_05', array(
	'title'    => esc_html__( 'Top Bar Style 05', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
