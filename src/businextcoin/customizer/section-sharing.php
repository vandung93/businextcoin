<?php
$section  = 'social_sharing';
$priority = 1;
$prefix   = 'social_sharing_';

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'multicheck',
	'settings'    => $prefix . 'item_enable',
	'label'       => esc_attr__( 'Sharing Links', 'businextcoin' ),
	'description' => esc_html__( 'Check to the box to enable social share links.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => array( 'facebook', 'twitter', 'linkedin', 'google_plus' ),
	'choices'     => array(
		'facebook'    => esc_attr__( 'Facebook', 'businextcoin' ),
		'twitter'     => esc_attr__( 'Twitter', 'businextcoin' ),
		'linkedin'    => esc_attr__( 'Linkedin', 'businextcoin' ),
		'google_plus' => esc_attr__( 'Google+', 'businextcoin' ),
		'tumblr'      => esc_attr__( 'Tumblr', 'businextcoin' ),
		'email'       => esc_attr__( 'Email', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'sortable',
	'settings'    => $prefix . 'order',
	'label'       => esc_attr__( 'Order', 'businextcoin' ),
	'description' => esc_html__( 'Controls the order of social share links.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => array(
		'facebook',
		'twitter',
		'google_plus',
		'tumblr',
		'linkedin',
		'email',
	),
	'choices'     => array(
		'facebook'    => esc_attr__( 'Facebook', 'businextcoin' ),
		'twitter'     => esc_attr__( 'Twitter', 'businextcoin' ),
		'google_plus' => esc_attr__( 'Google+', 'businextcoin' ),
		'tumblr'      => esc_attr__( 'Tumblr', 'businextcoin' ),
		'linkedin'    => esc_attr__( 'Linkedin', 'businextcoin' ),
		'email'       => esc_attr__( 'Email', 'businextcoin' ),
	),
) );
