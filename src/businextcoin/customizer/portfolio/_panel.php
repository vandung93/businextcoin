<?php
$panel    = 'portfolio';
$priority = 1;

Businextcoin_Kirki::add_section( 'archive_portfolio', array(
	'title'    => esc_html__( 'Portfolio Archive', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'single_portfolio', array(
	'title'    => esc_html__( 'Portfolio Single', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
