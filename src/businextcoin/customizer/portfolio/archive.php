<?php
$section  = 'archive_portfolio';
$priority = 1;
$prefix   = 'archive_portfolio_';

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'archive_portfolio_style',
	'label'       => esc_html__( 'Portfolio Style', 'businextcoin' ),
	'description' => esc_html__( 'Select portfolio style that display for archive pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'grid',
	'choices'     => array(
		'grid'    => esc_attr__( 'Grid Classic', 'businextcoin' ),
		'metro'   => esc_attr__( 'Grid Metro', 'businextcoin' ),
		'masonry' => esc_attr__( 'Grid Masonry', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'select',
	'settings' => 'archive_portfolio_thumbnail_size',
	'label'    => esc_html__( 'Thumbnail Size', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '480x480',
	'choices'  => array(
		'480x480' => esc_attr__( '480x480', 'businextcoin' ),
		'480x311' => esc_attr__( '480x311', 'businextcoin' ),
		'481x325' => esc_attr__( '481x325', 'businextcoin' ),
		'500x324' => esc_attr__( '500x324', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'number',
	'settings' => 'archive_portfolio_gutter',
	'label'    => esc_html__( 'Gutter', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 30,
	'choices'  => array(
		'min'  => 0,
		'step' => 1,
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'text',
	'settings' => 'archive_portfolio_columns',
	'label'    => esc_html__( 'Columns', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'xs:1;sm:2;md:3;lg:3',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'select',
	'settings' => 'archive_portfolio_overlay_style',
	'label'    => esc_html__( 'Columns', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'faded-light',
	'choices'  => array(
		'none'        => esc_attr__( 'None', 'businextcoin' ),
		'modern'      => esc_attr__( 'Modern', 'businextcoin' ),
		'zoom'        => esc_attr__( 'Image zoom - content below', 'businextcoin' ),
		'zoom2'       => esc_attr__( 'Zoom and Move Up - content below', 'businextcoin' ),
		'faded'       => esc_attr__( 'Faded', 'businextcoin' ),
		'faded-light' => esc_attr__( 'Faded - Light', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'archive_portfolio_animation',
	'label'       => esc_html__( 'CSS Animation', 'businextcoin' ),
	'description' => esc_html__( 'Select type of animation for element to be animated when it "enters" the browsers viewport (Note: works only in modern browsers).', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'scale-up',
	'choices'     => array(
		'none'             => esc_attr__( 'None', 'businextcoin' ),
		'fade-in'          => esc_attr__( 'Fade In', 'businextcoin' ),
		'move-up'          => esc_attr__( 'Move Up', 'businextcoin' ),
		'scale-up'         => esc_attr__( 'Scale Up', 'businextcoin' ),
		'fall-perspective' => esc_attr__( 'Fall Perspective', 'businextcoin' ),
		'fly'              => esc_attr__( 'Fly', 'businextcoin' ),
		'flip'             => esc_attr__( 'Flip', 'businextcoin' ),
		'helix'            => esc_attr__( 'Helix', 'businextcoin' ),
		'pop-up'           => esc_attr__( 'Pop Up', 'businextcoin' ),
	),
) );
