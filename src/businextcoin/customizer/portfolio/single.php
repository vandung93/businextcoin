<?php
$section  = 'single_portfolio';
$priority = 1;
$prefix   = 'single_portfolio_';

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_portfolio_sticky_detail_enable',
	'label'       => esc_html__( 'Sticky Detail Column', 'businextcoin' ),
	'description' => esc_html__( 'Turn on to enable sticky of detail column.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'businextcoin' ),
		'1' => esc_html__( 'On', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'single_portfolio_style',
	'label'       => esc_html__( 'Single Portfolio Style', 'businextcoin' ),
	'description' => esc_html__( 'Select style of all single portfolio post pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'flat',
	'choices'     => array(
		'left_details' => esc_attr__( 'Left Details', 'businextcoin' ),
		'slider'       => esc_attr__( 'Image Slider', 'businextcoin' ),
		'flat'         => esc_attr__( 'Flat', 'businextcoin' ),
		'video'        => esc_attr__( 'Video', 'businextcoin' ),
		'fullscreen'   => esc_attr__( 'Fullscreen', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'portfolio_related_enable',
	'label'       => esc_html__( 'Related Portfolios', 'businextcoin' ),
	'description' => esc_html__( 'Turn on this option to display related portfolio section.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '0',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'businextcoin' ),
		'1' => esc_html__( 'On', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'            => 'text',
	'settings'        => 'portfolio_related_title',
	'label'           => esc_html__( 'Related Title Section', 'businextcoin' ),
	'section'         => $section,
	'priority'        => $priority ++,
	'default'         => esc_html__( 'Related Projects', 'businextcoin' ),
	'active_callback' => array(
		array(
			'setting' => 'portfolio_related_enable',
			'operator' => '==',
			'value'    => '1',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'            => 'multicheck',
	'settings'        => 'portfolio_related_by',
	'label'           => esc_attr__( 'Related By', 'businextcoin' ),
	'section'         => $section,
	'priority'        => $priority ++,
	'default'         => array( 'portfolio_category' ),
	'choices'         => array(
		'portfolio_category' => esc_html__( 'Portfolio Category', 'businextcoin' ),
		'portfolio_tags'     => esc_html__( 'Portfolio Tags', 'businextcoin' ),
	),
	'active_callback' => array(
		array(
			'setting' => 'portfolio_related_enable',
			'operator' => '==',
			'value'    => '1',
		),
	),
) );


Businextcoin_Kirki::add_field( 'theme', array(
	'type'            => 'number',
	'settings'        => 'portfolio_related_number',
	'label'           => esc_html__( 'Number portfolios', 'businextcoin' ),
	'description'     => esc_html__( 'Controls the number of related portfolios', 'businextcoin' ),
	'section'         => $section,
	'priority'        => $priority ++,
	'default'         => 5,
	'choices'         => array(
		'min'  => 3,
		'max'  => 30,
		'step' => 1,
	),
	'active_callback' => array(
		array(
			'setting' => 'portfolio_related_enable',
			'operator' => '==',
			'value'    => '1',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_portfolio_comment_enable',
	'label'       => esc_html__( 'Comments', 'businextcoin' ),
	'description' => esc_html__( 'Turn on to display comments on single portfolio posts.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '0',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'businextcoin' ),
		'1' => esc_html__( 'On', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_portfolio_share_enable',
	'label'       => esc_html__( 'Share', 'businextcoin' ),
	'description' => esc_html__( 'Turn on to display Share list on single portfolio posts.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'businextcoin' ),
		'1' => esc_html__( 'On', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_portfolio_meta_view_enable',
	'label'       => esc_html__( 'View', 'businextcoin' ),
	'description' => esc_html__( 'Turn on to display View on single portfolio posts.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'businextcoin' ),
		'1' => esc_html__( 'On', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_portfolio_meta_like_enable',
	'label'       => esc_html__( 'Like', 'businextcoin' ),
	'description' => esc_html__( 'Turn on to display Like on single portfolio posts.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'businextcoin' ),
		'1' => esc_html__( 'On', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => 'single_portfolio_return_link',
	'label'       => esc_html__( 'Return button url', 'businextcoin' ),
	'description' => esc_html__( 'Controls the url when you click on return button', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '/portfolio',
) );
