<?php
$panel    = 'maintenance';
$priority = 1;

Businextcoin_Kirki::add_section( 'general', array(
	'title'       => esc_html__( 'General', 'businextcoin' ),
	'description' => sprintf( '<div class="desc">
			<strong class="insight-label insight-label-info">%s</strong>
			<p>%s</p>
			<p><span class="insight-label insight-label-info">%s</span></p>
			<p>%s</p>
		</div>', esc_html__( 'IMPORTANT NOTE: ', 'businextcoin' ), esc_html__( 'To active maintenance mode, please add this line to wp-config.php file, before "That\'s all, stop editing! Happy blogging" comment.', 'businextcoin' ), esc_html__( 'define(\'BUSINEXTCOIN_MAINTENANCE\', true);', 'businextcoin' ), esc_html__( 'Then select a maintenance page below.', 'businextcoin' ) ),
	'panel'       => $panel,
	'priority'    => $priority ++,
) );

Businextcoin_Kirki::add_section( 'maintenance', array(
	'title'    => esc_html__( 'Maintenance', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'coming_soon_01', array(
	'title'    => esc_html__( 'Coming Soon 01', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
