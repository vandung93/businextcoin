<?php
$section  = 'maintenance';
$priority = 1;
$prefix   = 'maintenance_';

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'image',
	'settings'    => $prefix . 'single_image',
	'label'       => esc_html__( 'Single Image', 'businextcoin' ),
	'description' => esc_html__( 'Select an image file for right image.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => BUSINEXTCOIN_THEME_IMAGE_URI . '/maintenance-01-image.png',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => $prefix . 'progress_bar',
	'label'       => esc_html__( 'Progress bar', 'businextcoin' ),
	'description' => esc_html__( 'Turn on to show progress bar form in maintenance mode', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Hide', 'businextcoin' ),
		'1' => esc_html__( 'Show', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'slider',
	'settings' => $prefix . 'percent',
	'label'    => esc_attr__( 'Percent Done', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 85,
	'choices'  => array(
		'min'  => '1',
		'max'  => '100',
		'step' => '1',
	),
	'output'   => array(
		array(
			'element'  => '
			.maintenance-progress-labels,
			.maintenance-progress',
			'property' => 'width',
			'units'    => '%',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'text',
	'settings' => $prefix . 'title',
	'label'    => esc_html__( 'Title', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => esc_html__( 'Site maintenance', 'businextcoin' ),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'textarea',
	'settings'    => $prefix . 'text',
	'label'       => esc_html__( 'Text', 'businextcoin' ),
	'description' => esc_html__( 'Controls the text that display below title.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => esc_html__( 'We sincerely apologize for the inconvenience. Our site is currently undergoing scheduled maintenance and upgrades, but will return shortly after.
', 'businextcoin' ),
) );
