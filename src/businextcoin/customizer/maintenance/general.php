<?php
$section  = 'general';
$priority = 1;
$prefix   = 'general_';

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'select',
	'settings' => 'maintenance_page',
	'label'    => esc_html__( 'Page', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '',
	'choices'  => Businextcoin_Maintenance::get_maintenance_pages(),
) );
