<?php
$section            = 'sliders';
$priority           = 1;
$prefix             = 'sliders_';
$revolution_sliders = Businextcoin_Helper::get_list_revslider();

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Search Page', 'businextcoin' ) . '</div>',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'search_page_rev_slider',
	'label'       => esc_html__( 'Revolution Slider', 'businextcoin' ),
	'description' => esc_html__( 'Select the unique name of the slider.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '',
	'choices'     => $revolution_sliders,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'search_page_slider_position',
	'label'    => esc_html__( 'Slider Position', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'below',
	'choices'  => array(
		'above' => esc_html__( 'Above Header', 'businextcoin' ),
		'below' => esc_html__( 'Below Header', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Front Latest Posts Page', 'businextcoin' ) . '</div>',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'home_page_rev_slider',
	'label'       => esc_html__( 'Revolution Slider', 'businextcoin' ),
	'description' => esc_html__( 'Select the unique name of the slider.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '',
	'choices'     => $revolution_sliders,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'home_page_slider_position',
	'label'    => esc_html__( 'Slider Position', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'below',
	'choices'  => array(
		'above' => esc_html__( 'Above Header', 'businextcoin' ),
		'below' => esc_html__( 'Below Header', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Blog Archive', 'businextcoin' ) . '</div>',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'blog_archive_page_rev_slider',
	'label'       => esc_html__( 'Revolution Slider', 'businextcoin' ),
	'description' => esc_html__( 'Select the unique name of the slider.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '',
	'choices'     => $revolution_sliders,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'blog_archive_page_slider_position',
	'label'    => esc_html__( 'Slider Position', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'below',
	'choices'  => array(
		'above' => esc_html__( 'Above Header', 'businextcoin' ),
		'below' => esc_html__( 'Below Header', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Portfolio Archive', 'businextcoin' ) . '</div>',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'portfolio_archive_page_rev_slider',
	'label'       => esc_html__( 'Revolution Slider', 'businextcoin' ),
	'description' => esc_html__( 'Select the unique name of the slider.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '',
	'choices'     => $revolution_sliders,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'portfolio_archive_page_slider_position',
	'label'    => esc_html__( 'Slider Position', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'below',
	'choices'  => array(
		'above' => esc_html__( 'Above Header', 'businextcoin' ),
		'below' => esc_html__( 'Below Header', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Product Archive', 'businextcoin' ) . '</div>',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'product_archive_page_rev_slider',
	'label'       => esc_html__( 'Revolution Slider', 'businextcoin' ),
	'description' => esc_html__( 'Select the unique name of the slider.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '',
	'choices'     => $revolution_sliders,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'product_archive_page_slider_position',
	'label'    => esc_html__( 'Slider Position', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'below',
	'choices'  => array(
		'above' => esc_html__( 'Above Header', 'businextcoin' ),
		'below' => esc_html__( 'Below Header', 'businextcoin' ),
	),
) );
