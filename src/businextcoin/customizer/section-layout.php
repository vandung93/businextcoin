<?php
$section  = 'layout';
$priority = 1;
$prefix   = 'site_';

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => $prefix . 'layout',
	'label'       => esc_html__( 'Layout', 'businextcoin' ),
	'description' => esc_html__( 'Controls the site layout.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'wide',
	'choices'     => array(
		'boxed' => esc_html__( 'Boxed', 'businextcoin' ),
		'wide'  => esc_html__( 'Wide', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'dimension',
	'settings'    => $prefix . 'width',
	'label'       => esc_html__( 'Site Width', 'businextcoin' ),
	'description' => esc_html__( 'Controls the overall site width. Enter value including any valid CSS unit, ex: 1200px.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1200px',
) );
