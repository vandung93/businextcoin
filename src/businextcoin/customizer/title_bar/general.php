<?php
$section  = 'title_bar';
$priority = 1;
$prefix   = 'title_bar_';

$title_bar_stylish = array(
	'default' => esc_html__( 'Default', 'businextcoin' ),
	'none'    => esc_html__( 'None', 'businextcoin' ),
	'01'      => esc_html__( 'Style 01', 'businextcoin' ),
	'02'      => esc_html__( 'Style 02', 'businextcoin' ),
	'03'      => esc_html__( 'Style 03', 'businextcoin' ),
);

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => $prefix . 'layout',
	'label'       => esc_html__( 'Default Title Bar', 'businextcoin' ),
	'description' => esc_html__( 'Select default title bar that displays on all pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '01',
	'choices'     => array(
		'none' => esc_html__( 'None', 'businextcoin' ),
		'01'   => esc_html__( 'Style 01', 'businextcoin' ),
		'02'   => esc_html__( 'Style 02', 'businextcoin' ),
		'03'   => esc_html__( 'Style 03', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => $prefix . 'search_title',
	'label'       => esc_html__( 'Search Heading', 'businextcoin' ),
	'description' => esc_html__( 'Enter text prefix that displays on search results page.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => esc_html__( 'Search results for: ', 'businextcoin' ),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => $prefix . 'home_title',
	'label'       => esc_html__( 'Home Heading', 'businextcoin' ),
	'description' => esc_html__( 'Enter text that displays on front latest posts page.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => esc_html__( 'Blog', 'businextcoin' ),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => $prefix . 'archive_category_title',
	'label'       => esc_html__( 'Archive Category Heading', 'businextcoin' ),
	'description' => esc_html__( 'Enter text prefix that displays on archive category page.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => esc_html__( 'Category: ', 'businextcoin' ),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => $prefix . 'archive_tag_title',
	'label'       => esc_html__( 'Archive Tag Heading', 'businextcoin' ),
	'description' => esc_html__( 'Enter text prefix that displays on archive tag page.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => esc_html__( 'Tag: ', 'businextcoin' ),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => $prefix . 'archive_author_title',
	'label'       => esc_html__( 'Archive Author Heading', 'businextcoin' ),
	'description' => esc_html__( 'Enter text prefix that displays on archive author page.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => esc_html__( 'Author: ', 'businextcoin' ),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => $prefix . 'archive_year_title',
	'label'       => esc_html__( 'Archive Year Heading', 'businextcoin' ),
	'description' => esc_html__( 'Enter text prefix that displays on archive year page.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => esc_html__( 'Year: ', 'businextcoin' ),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => $prefix . 'archive_month_title',
	'label'       => esc_html__( 'Archive Month Heading', 'businextcoin' ),
	'description' => esc_html__( 'Enter text prefix that displays on archive month page.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => esc_html__( 'Month: ', 'businextcoin' ),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => $prefix . 'archive_day_title',
	'label'       => esc_html__( 'Archive Day Heading', 'businextcoin' ),
	'description' => esc_html__( 'Enter text prefix that displays on archive day page.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => esc_html__( 'Day: ', 'businextcoin' ),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => $prefix . 'single_blog_title',
	'label'       => esc_html__( 'Single Blog Heading', 'businextcoin' ),
	'description' => esc_html__( 'Enter text that displays on single blog posts. Leave blank to use post title.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => esc_html__( 'Blog', 'businextcoin' ),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => $prefix . 'single_portfolio_title',
	'label'       => esc_html__( 'Single Portfolio Heading', 'businextcoin' ),
	'description' => esc_html__( 'Enter text that displays on single portfolio pages. Leave blank to use portfolio title.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => esc_html__( 'Portfolio', 'businextcoin' ),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => $prefix . 'single_product_title',
	'label'       => esc_html__( 'Single Product Heading', 'businextcoin' ),
	'description' => esc_html__( 'Enter text that displays on single product pages. Leave blank to use product title.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => esc_html__( 'Shop', 'businextcoin' ),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_page_title_bar_layout',
	'label'       => esc_html__( 'Single Page Title Bar Layout', 'businextcoin' ),
	'description' => esc_html__( 'Select default Title Bar that displays on all single pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'default',
	'choices'     => $title_bar_stylish,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'home_page_title_bar_layout',
	'label'       => esc_html__( 'Home Page Title Bar Layout', 'businextcoin' ),
	'description' => esc_html__( 'Select default Title Bar that displays on front latest posts page.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '02',
	'choices'     => $title_bar_stylish,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'blog_archive_title_bar_layout',
	'label'       => esc_html__( 'Single Blog Page Title Bar Layout', 'businextcoin' ),
	'description' => esc_html__( 'Select default Title Bar that displays on all single blog post pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '02',
	'choices'     => $title_bar_stylish,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_title_bar_layout',
	'label'       => esc_html__( 'Single Blog Page Title Bar Layout', 'businextcoin' ),
	'description' => esc_html__( 'Select default Title Bar that displays on all single blog post pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'default',
	'choices'     => $title_bar_stylish,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_product_title_bar_layout',
	'label'       => esc_html__( 'Single Product Page Title Bar Layout', 'businextcoin' ),
	'description' => esc_html__( 'Select default Title Bar that displays on all single product pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'default',
	'choices'     => $title_bar_stylish,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_portfolio_title_bar_layout',
	'label'       => esc_html__( 'Single Portfolio Page Title Bar Layout', 'businextcoin' ),
	'description' => esc_html__( 'Select default Title Bar that displays on all single profolio pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'default',
	'choices'     => $title_bar_stylish,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_case_study_title_bar_layout',
	'label'       => esc_html__( 'Single Case Study Page Title Bar Layout', 'businextcoin' ),
	'description' => esc_html__( 'Select default Title Bar that displays on all single case study post pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $title_bar_stylish,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_service_title_bar_layout',
	'label'       => esc_html__( 'Single Service Page Title Bar Layout', 'businextcoin' ),
	'description' => esc_html__( 'Select default Title Bar that displays on all single service post pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $title_bar_stylish,
) );
