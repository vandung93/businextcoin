<?php
$panel    = 'title_bar';
$priority = 1;

Businextcoin_Kirki::add_section( 'title_bar', array(
	'title'    => esc_html__( 'General', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'title_bar_01', array(
	'title'    => esc_html__( 'Style 01', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'title_bar_02', array(
	'title'    => esc_html__( 'Style 02', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'title_bar_03', array(
	'title'    => esc_html__( 'Style 03', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
