<?php
$section             = 'sidebars';
$priority            = 1;
$prefix              = 'sidebars_';
$sidebar_positions   = Businextcoin_Helper::get_list_sidebar_positions();
$registered_sidebars = Businextcoin_Helper::get_registered_sidebars();

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => sprintf( '<div class="desc">
			<strong class="insight-label insight-label-info">%s</strong>
			<p>%s</p>
			<p>%s</p>
		</div>', esc_html__( 'IMPORTANT NOTE: ', 'businextcoin' ), esc_html__( 'Sidebar 2 can only be used if sidebar 1 is selected.', 'businextcoin' ), esc_html__( 'Sidebar position option will control the position of sidebar 1. If sidebar 2 is selected, it will display on the opposite side.', 'businextcoin' ) ),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'General Settings', 'businextcoin' ) . '</div>',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'number',
	'settings'    => 'one_sidebar_breakpoint',
	'label'       => esc_html__( 'One Sidebar Breakpoint', 'businextcoin' ),
	'description' => esc_html__( 'Controls the breakpoint when has only one sidebar to make the sidebar 100% width.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'postMessage',
	'default'     => 992,
	'choices'     => array(
		'min'  => 460,
		'max'  => 1300,
		'step' => 10,
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'number',
	'settings'    => 'both_sidebar_breakpoint',
	'label'       => esc_html__( 'Both Sidebars Breakpoint', 'businextcoin' ),
	'description' => esc_html__( 'Controls the breakpoint when has both sidebars to make sidebars 100% width.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'postMessage',
	'default'     => 1199,
	'choices'     => array(
		'min'  => 460,
		'max'  => 1300,
		'step' => 10,
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'sidebars_below_content_mobile',
	'label'       => esc_html__( 'Sidebars Below Content', 'businextcoin' ),
	'description' => esc_html__( 'Move sidebars display after main content on smaller screens.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'No', 'businextcoin' ),
		'1' => esc_html__( 'Yes', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Single Sidebar Layouts', 'businextcoin' ) . '</div>',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'number',
	'settings'    => 'single_sidebar_width',
	'label'       => esc_html__( 'Single Sidebar Width', 'businextcoin' ),
	'description' => esc_html__( 'Controls the width of the sidebar when only one sidebar is present. Input value as % unit. Ex: 33.33333', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '25',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'dimension',
	'settings'    => 'single_sidebar_offset',
	'label'       => esc_html__( 'Single Sidebar Offset', 'businextcoin' ),
	'description' => esc_html__( 'Controls the offset of the sidebar when only one sidebar is present. Enter value including any valid CSS unit. Ex: 70px.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '0',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Dual Sidebar Layouts', 'businextcoin' ) . '</div>',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'number',
	'settings'    => 'dual_sidebar_width',
	'label'       => esc_html__( 'Dual Sidebar Width', 'businextcoin' ),
	'description' => esc_html__( 'Controls the width of sidebars when dual sidebars are present. Enter value including any valid CSS unit. Ex: 33.33333.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '25',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'dimension',
	'settings'    => 'dual_sidebar_offset',
	'label'       => esc_html__( 'Dual Sidebar Offset', 'businextcoin' ),
	'description' => esc_html__( 'Controls the offset of sidebars when dual sidebars are present. Enter value including any valid CSS unit. Ex: 70px.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '0',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Pages', 'businextcoin' ) . '</div>',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'page_sidebar_1',
	'label'       => esc_html__( 'Sidebar 1', 'businextcoin' ),
	'description' => esc_html__( 'Select sidebar 1 that will display on all pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $registered_sidebars,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'page_sidebar_2',
	'label'       => esc_html__( 'Sidebar 2', 'businextcoin' ),
	'description' => esc_html__( 'Select sidebar 2 that will display on all pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $registered_sidebars,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'page_sidebar_position',
	'label'    => esc_html__( 'Sidebar Position', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'right',
	'choices'  => $sidebar_positions,
) );


Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Search Page', 'businextcoin' ) . '</div>',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'search_page_sidebar_1',
	'label'       => esc_html__( 'Sidebar 1', 'businextcoin' ),
	'description' => esc_html__( 'Select sidebar 1 that will display on search results page.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'blog_sidebar',
	'choices'     => $registered_sidebars,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'search_page_sidebar_2',
	'label'       => esc_html__( 'Sidebar 2', 'businextcoin' ),
	'description' => esc_html__( 'Select sidebar 2 that will display on search results page.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $registered_sidebars,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'search_page_sidebar_position',
	'label'    => esc_html__( 'Sidebar Position', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'right',
	'choices'  => $sidebar_positions,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Front Latest Posts Page', 'businextcoin' ) . '</div>',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'home_page_sidebar_1',
	'label'       => esc_html__( 'Sidebar 1', 'businextcoin' ),
	'description' => esc_html__( 'Select sidebar 1 that will display on front latest posts page.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'blog_sidebar',
	'choices'     => $registered_sidebars,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'home_page_sidebar_2',
	'label'       => esc_html__( 'Sidebar 2', 'businextcoin' ),
	'description' => esc_html__( 'Select sidebar 2 that will display on front latest posts page.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $registered_sidebars,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'home_page_sidebar_position',
	'label'    => esc_html__( 'Sidebar Position', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'right',
	'choices'  => $sidebar_positions,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Blog Posts', 'businextcoin' ) . '</div>',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'post_page_sidebar_1',
	'label'       => esc_html__( 'Sidebar 1', 'businextcoin' ),
	'description' => esc_html__( 'Select sidebar 1 that will display on single blog post pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'blog_sidebar',
	'choices'     => $registered_sidebars,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'post_page_sidebar_2',
	'label'       => esc_html__( 'Sidebar 2', 'businextcoin' ),
	'description' => esc_html__( 'Select sidebar 2 that will display on single blog post pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $registered_sidebars,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'post_page_sidebar_position',
	'label'    => esc_html__( 'Sidebar Position', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'right',
	'choices'  => $sidebar_positions,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Blog Archive', 'businextcoin' ) . '</div>',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'blog_archive_page_sidebar_1',
	'label'       => esc_html__( 'Sidebar 1', 'businextcoin' ),
	'description' => esc_html__( 'Select sidebar 1 that will display on blog archive pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'blog_sidebar',
	'choices'     => $registered_sidebars,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'blog_archive_page_sidebar_2',
	'label'       => esc_html__( 'Sidebar 2', 'businextcoin' ),
	'description' => esc_html__( 'Select sidebar 2 that will display on blog archive pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $registered_sidebars,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'blog_archive_page_sidebar_position',
	'label'    => esc_html__( 'Sidebar Position', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'right',
	'choices'  => $sidebar_positions,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Portfolio Posts', 'businextcoin' ) . '</div>',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'portfolio_page_sidebar_1',
	'label'       => esc_html__( 'Sidebar 1', 'businextcoin' ),
	'description' => esc_html__( 'Select sidebar 1 that will display on single portfolio pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $registered_sidebars,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'portfolio_page_sidebar_2',
	'label'       => esc_html__( 'Sidebar 2', 'businextcoin' ),
	'description' => esc_html__( 'Select sidebar 2 that will display on single portfolio pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $registered_sidebars,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'portfolio_page_sidebar_position',
	'label'    => esc_html__( 'Sidebar Position', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'right',
	'choices'  => $sidebar_positions,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Portfolio Archive', 'businextcoin' ) . '</div>',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'portfolio_archive_page_sidebar_1',
	'label'       => esc_html__( 'Sidebar 1', 'businextcoin' ),
	'description' => esc_html__( 'Select sidebar 1 that will display on portfolio archive pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $registered_sidebars,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'portfolio_archive_page_sidebar_2',
	'label'       => esc_html__( 'Sidebar 2', 'businextcoin' ),
	'description' => esc_html__( 'Select sidebar 2 that will display on portfolio archive pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $registered_sidebars,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'portfolio_archive_page_sidebar_position',
	'label'    => esc_html__( 'Sidebar Position', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'right',
	'choices'  => $sidebar_positions,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Single Product', 'businextcoin' ) . '</div>',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'product_page_sidebar_1',
	'label'       => esc_html__( 'Sidebar 1', 'businextcoin' ),
	'description' => esc_html__( 'Select sidebar 1 that will display on single product pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'shop_sidebar',
	'choices'     => $registered_sidebars,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'product_page_sidebar_2',
	'label'       => esc_html__( 'Sidebar 2', 'businextcoin' ),
	'description' => esc_html__( 'Select sidebar 2 that will display on single product pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $registered_sidebars,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'product_page_sidebar_position',
	'label'    => esc_html__( 'Sidebar Position', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'right',
	'choices'  => $sidebar_positions,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Product Archive', 'businextcoin' ) . '</div>',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'product_archive_page_sidebar_1',
	'label'       => esc_html__( 'Sidebar 1', 'businextcoin' ),
	'description' => esc_html__( 'Select sidebar 1 that will display on product archive pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'shop_sidebar',
	'choices'     => $registered_sidebars,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'product_archive_page_sidebar_2',
	'label'       => esc_html__( 'Sidebar 2', 'businextcoin' ),
	'description' => esc_html__( 'Select sidebar 2 that will display on product archive pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $registered_sidebars,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'product_archive_page_sidebar_position',
	'label'    => esc_html__( 'Sidebar Position', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'left',
	'choices'  => $sidebar_positions,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Single Case Study', 'businextcoin' ) . '</div>',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'case_study_page_sidebar_1',
	'label'       => esc_html__( 'Sidebar 1', 'businextcoin' ),
	'description' => esc_html__( 'Select sidebar 1 that will display on single case study pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'case_study_sidebar',
	'choices'     => $registered_sidebars,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'case_study_page_sidebar_2',
	'label'       => esc_html__( 'Sidebar 2', 'businextcoin' ),
	'description' => esc_html__( 'Select sidebar 2 that will display on single case study pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $registered_sidebars,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'case_study_page_sidebar_position',
	'label'    => esc_html__( 'Sidebar Position', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'left',
	'choices'  => $sidebar_positions,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Case Study Archive', 'businextcoin' ) . '</div>',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'case_study_archive_page_sidebar_1',
	'label'       => esc_html__( 'Sidebar 1', 'businextcoin' ),
	'description' => esc_html__( 'Select sidebar 1 that will display on case study archive pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $registered_sidebars,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'case_study_archive_page_sidebar_2',
	'label'       => esc_html__( 'Sidebar 2', 'businextcoin' ),
	'description' => esc_html__( 'Select sidebar 2 that will display on case study archive pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $registered_sidebars,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'case_study_archive_page_sidebar_position',
	'label'    => esc_html__( 'Sidebar Position', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'left',
	'choices'  => $sidebar_positions,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Single Service', 'businextcoin' ) . '</div>',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'service_page_sidebar_1',
	'label'       => esc_html__( 'Sidebar 1', 'businextcoin' ),
	'description' => esc_html__( 'Select sidebar 1 that will display on single service pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $registered_sidebars,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'service_page_sidebar_2',
	'label'       => esc_html__( 'Sidebar 2', 'businextcoin' ),
	'description' => esc_html__( 'Select sidebar 2 that will display on single service pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $registered_sidebars,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'service_page_sidebar_position',
	'label'    => esc_html__( 'Sidebar Position', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'left',
	'choices'  => $sidebar_positions,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Service Archive', 'businextcoin' ) . '</div>',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'service_archive_page_sidebar_1',
	'label'       => esc_html__( 'Sidebar 1', 'businextcoin' ),
	'description' => esc_html__( 'Select sidebar 1 that will display on service archive pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $registered_sidebars,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'service_archive_page_sidebar_2',
	'label'       => esc_html__( 'Sidebar 2', 'businextcoin' ),
	'description' => esc_html__( 'Select sidebar 2 that will display on service archive pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $registered_sidebars,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'service_archive_page_sidebar_position',
	'label'    => esc_html__( 'Sidebar Position', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'left',
	'choices'  => $sidebar_positions,
) );
