<?php
$section  = 'color_';
$priority = 1;
$prefix   = 'color_';

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'site_skin',
	'label'    => esc_html__( 'Site Skin', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'light',
	'choices'  => array(
		'light' => esc_html__( 'Light', 'businextcoin' ),
		'dark'  => esc_html__( 'Dark', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'      => 'color',
	'settings'  => 'primary_color',
	'label'     => esc_html__( 'Primary Color', 'businextcoin' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'transport' => 'auto',
	'default'   => Businextcoin::PRIMARY_COLOR,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'      => 'color',
	'settings'  => 'secondary_color',
	'label'     => esc_html__( 'Secondary Color', 'businextcoin' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'transport' => 'auto',
	'default'   => Businextcoin::SECONDARY_COLOR,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'      => 'color-alpha',
	'settings'  => 'box_content_background_color',
	'label'     => esc_html__( 'Box Content Background Color', 'businextcoin' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'transport' => 'auto',
	'default'   => '#f7f7f7',
	'output'    => array(
		array(
			'element'  => '
				td,
				th,
				blockquote,
				hr,
				pre,
				.wp-caption-text,
				.comment-nav-links li .current,
				.page-pagination li .current,
				.entry-author,
				.tagcloud a,
				.widget_categories li,
				.widget_product_categories li
			',
			'property' => 'background-color',
		),
	),
) );
