<?php
$panel    = 'search';
$priority = 1;

Businextcoin_Kirki::add_section( 'search_page', array(
	'title'    => esc_html__( 'Search Page', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'search_popup', array(
	'title'    => esc_html__( 'Search Popup', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
