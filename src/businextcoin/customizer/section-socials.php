<?php
$section  = 'socials';
$priority = 1;
$prefix   = 'social_';

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'social_link_target',
	'label'    => esc_html__( 'Open link in a new tab.', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '1',
	'choices'  => array(
		'0' => esc_html__( 'No', 'businextcoin' ),
		'1' => esc_html__( 'Yes', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'      => 'repeater',
	'settings'  => 'social_link',
	'section'   => $section,
	'priority'  => $priority ++,
	'choices'   => array(
		'labels' => array(
			'add-new-row' => esc_html__( 'Add new social network', 'businextcoin' ),
		),
	),
	'row_label' => array(
		'type'  => 'field',
		'field' => 'tooltip',
	),
	'default'   => array(
		array(
			'tooltip'    => esc_html__( 'Facebook', 'businextcoin' ),
			'icon_class' => 'ion-social-facebook',
			'link_url'   => 'https://facebook.com',
		),
		array(
			'tooltip'    => esc_html__( 'Twitter', 'businextcoin' ),
			'icon_class' => 'ion-social-twitter',
			'link_url'   => 'https://twitter.com',
		),
		array(
			'tooltip'    => esc_html__( 'Vimeo', 'businextcoin' ),
			'icon_class' => 'ion-social-vimeo',
			'link_url'   => 'https://www.vimeo.com',
		),
		array(
			'tooltip'    => esc_html__( 'Linkedin', 'businextcoin' ),
			'icon_class' => 'ion-social-linkedin',
			'link_url'   => 'https://www.linkedin.com',
		),
	),
	'fields'    => array(
		'tooltip'    => array(
			'type'        => 'text',
			'label'       => esc_html__( 'Tooltip', 'businextcoin' ),
			'description' => esc_html__( 'Enter your hint text for your icon', 'businextcoin' ),
			'default'     => '',
		),
		'icon_class' => array(
			'type'        => 'text',
			'label'       => esc_html__( 'Icon Class', 'businextcoin' ),
			'description' => esc_html__( 'This will be the icon class for your link', 'businextcoin' ),
			'default'     => '',
		),
		'link_url'   => array(
			'type'        => 'text',
			'label'       => esc_html__( 'Link URL', 'businextcoin' ),
			'description' => esc_html__( 'This will be the link URL', 'businextcoin' ),
			'default'     => '',
		),
	),
) );
