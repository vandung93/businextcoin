<?php
$section  = 'error404_page';
$priority = 1;
$prefix   = 'error404_page_';

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'logo',
	'label'    => esc_html__( 'Logo', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'light',
	'choices'  => array(
		'light' => esc_html__( 'Light', 'businextcoin' ),
		'dark'  => esc_html__( 'Dark', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => 'error404_page_title',
	'label'       => esc_html__( 'Title', 'businextcoin' ),
	'description' => esc_html__( 'Controls the title that display on error 404 page.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => esc_html__( 'Sorry, we couldn\'t find the page you\'re looking for.', 'businextcoin' ),
) );
