<?php
$panel    = 'navigation';
$priority = 1;

Businextcoin_Kirki::add_section( 'navigation', array(
	'title'    => esc_html__( 'Desktop Menu', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'navigation_minimal', array(
	'title'    => esc_html__( 'Off Canvas Menu', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'navigation_mobile', array(
	'title'    => esc_html__( 'Mobile Menu', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
