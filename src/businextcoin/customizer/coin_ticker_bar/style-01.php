<?php
$section  = 'coin_ticker_bar_style_01';
$priority = 1;
$prefix   = 'coin_ticker_bar_style_01_';

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'kirki_typography',
	'settings'    => $prefix . 'typography',
	'label'       => esc_html__( 'Typography', 'businextcoin' ),
	'description' => esc_html__( 'These settings control the typography of texts of top bar section.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => array(
		'font-family'    => '',
		'variant'        => 'normal',
		'line-height'    => '1.35',
		'letter-spacing' => '0',
	),
	'output'      => array(
		array(
			'element' => '.coin-ticker-bar-01, .coin-ticker-bar-01 a',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'      => 'slider',
	'settings'  => $prefix . 'font_size',
	'label'     => esc_html__( 'Font size', 'businextcoin' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => 14,
	'transport' => 'auto',
	'choices'   => array(
		'min'  => 10,
		'max'  => 50,
		'step' => 1,
	),
	'output'    => array(
		array(
			'element'  => '.coin-ticker-bar-01, .coin-ticker-bar-01 a',
			'property' => 'font-size',
			'units'    => 'px',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'bg_color',
	'label'       => esc_html__( 'Background', 'businextcoin' ),
	'description' => esc_html__( 'Controls the background color of top bar.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#000',
	'output'      => array(
		array(
			'element'  => '.coin-ticker-bar-01',
			'property' => 'background-color',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'      => 'slider',
	'settings'  => $prefix . 'border_width',
	'label'     => esc_html__( 'Border Bottom Width', 'businextcoin' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => 1,
	'transport' => 'auto',
	'choices'   => array(
		'min'  => 0,
		'max'  => 50,
		'step' => 1,
	),
	'output'    => array(
		array(
			'element'  => '.coin-ticker-bar-01',
			'property' => 'border-bottom-width',
			'units'    => 'px',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'border_color',
	'label'       => esc_html__( 'Border Color', 'businextcoin' ),
	'description' => esc_html__( 'Controls the border color of top bar.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#2E2E2E',
	'output'      => array(
		array(
			'element'  => '.coin-ticker-bar-01',
			'property' => 'border-color',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'      => 'slider',
	'settings'  => $prefix . 'min_height',
	'label'     => esc_html__( 'Min Height', 'businextcoin' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => 50,
	'transport' => 'auto',
	'choices'   => array(
		'min'  => 0,
		'max'  => 500,
		'step' => 10,
	),
	'output'    => array(
		array(
			'element'  => '.coin-ticker-bar-01',
			'property' => 'min-height',
			'units'    => 'px',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'color',
	'settings'    => $prefix . 'text_color',
	'label'       => esc_html__( 'Text', 'businextcoin' ),
	'description' => esc_html__( 'Controls the color of text on top bar.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#fff',
	'output'      => array(
		array(
			'element'  => '
			.coin-ticker-bar-01,
			.coin-ticker-bar-01 .tickercontainer span.price,
			.coin-ticker-bar-01 .tickercontainer span.name
			',
			'property' => 'color',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'color',
	'settings'    => $prefix . 'link_color',
	'label'       => esc_html__( 'Link Color', 'businextcoin' ),
	'description' => esc_html__( 'Controls the color of links on top bar.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#fff',
	'output'      => array(
		array(
			'element'  => '
			.coin-ticker-bar-01 a,
			
			',
			'property' => 'color',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'color',
	'settings'    => $prefix . 'link_hover_color',
	'label'       => esc_html__( 'Link Hover Color', 'businextcoin' ),
	'description' => esc_html__( 'Controls the color when hover of links on top bar.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => 'rgba(255, 255, 255, 0.5)',
	'output'      => array(
		array(
			'element'  => '.coin-ticker-bar-01 a:hover, .coin-ticker-bar-01 a:focus',
			'property' => 'color',
		),
	),
) );
