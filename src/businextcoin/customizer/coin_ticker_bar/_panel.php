<?php
$panel    = 'coin_ticker_bar';
$priority = 1;

Businextcoin_Kirki::add_section( 'coin_ticker_bar', array(
	'title'    => esc_html__( 'General', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'coin_ticker_bar_style_01', array(
	'title'    => esc_html__( 'Coin Ticker Style 01', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
