<?php
$section  = 'coin_ticker_bar';
$priority = 1;
$prefix   = 'coin_ticker_bar_';

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'global_coin_ticker_bar',
	'label'    => esc_html__( 'Default Coin Ticker Bar Type', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'none',
	'choices'  => Businextcoin_CCPW::get_coin_ticker_bar_list(),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'select',
	'settings' => 'crypto_currency_widget',
	'label'    => esc_html__( 'Crypto Currency Widget', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'coin-ticker',
	'choices'  => Businextcoin_CCPW::get_list_crypto_currency_price_shortcode(),
) );
