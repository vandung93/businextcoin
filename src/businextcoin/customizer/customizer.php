<?php
/**
 * Theme Customizer
 *
 * @package Businextcoin
 * @since   1.0
 */

/**
 * Setup configuration
 */
Businextcoin_Kirki::add_config( 'theme', array(
	'option_type' => 'theme_mod',
	'capability'  => 'edit_theme_options',
) );

/**
 * Add sections
 */
$priority = 1;

Businextcoin_Kirki::add_section( 'layout', array(
	'title'    => esc_html__( 'Layout', 'businextcoin' ),
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'color_', array(
	'title'    => esc_html__( 'Colors', 'businextcoin' ),
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'background', array(
	'title'    => esc_html__( 'Background', 'businextcoin' ),
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'typography', array(
	'title'    => esc_html__( 'Typography', 'businextcoin' ),
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_panel( 'coin_ticker_bar', array(
	'title'    => esc_html__( 'Coin Ticker Bar', 'businextcoin' ),
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_panel( 'notification_bar', array(
	'title'    => esc_html__( 'Notification Bar', 'businextcoin' ),
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_panel( 'top_bar', array(
	'title'    => esc_html__( 'Top bar', 'businextcoin' ),
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_panel( 'header', array(
	'title'    => esc_html__( 'Header', 'businextcoin' ),
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'logo', array(
	'title'    => esc_html__( 'Logo', 'businextcoin' ),
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_panel( 'navigation', array(
	'title'    => esc_html__( 'Navigation', 'businextcoin' ),
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'sliders', array(
	'title'    => esc_html__( 'Sliders', 'businextcoin' ),
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_panel( 'title_bar', array(
	'title'    => esc_html__( 'Page Title Bar', 'businextcoin' ),
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'sidebars', array(
	'title'    => esc_html__( 'Sidebars', 'businextcoin' ),
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_panel( 'footer', array(
	'title'    => esc_html__( 'Footer', 'businextcoin' ),
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_panel( 'blog', array(
	'title'    => esc_html__( 'Blog', 'businextcoin' ),
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_panel( 'portfolio', array(
	'title'    => esc_html__( 'Portfolio', 'businextcoin' ),
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_panel( 'shop', array(
	'title'    => esc_html__( 'Shop', 'businextcoin' ),
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_panel( 'case_study', array(
	'title'    => esc_html__( 'Case Study', 'businextcoin' ),
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_panel( 'service', array(
	'title'    => esc_html__( 'Service', 'businextcoin' ),
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'socials', array(
	'title'    => esc_html__( 'Social Networks', 'businextcoin' ),
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'social_sharing', array(
	'title'    => esc_html__( 'Social Sharing', 'businextcoin' ),
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_panel( 'search', array(
	'title'    => esc_html__( 'Search', 'businextcoin' ),
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'error404_page', array(
	'title'    => esc_html__( 'Error 404 Page', 'businextcoin' ),
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_panel( 'maintenance', array(
	'title'    => esc_html__( 'Maintenance', 'businextcoin' ),
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_panel( 'shortcode', array(
	'title'    => esc_html__( 'Shortcodes', 'businextcoin' ),
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_panel( 'advanced', array(
	'title'    => esc_html__( 'Advanced', 'businextcoin' ),
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_panel( 'notices', array(
	'title'    => esc_html__( 'Notices', 'businextcoin' ),
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'custom_code', array(
	'title'    => esc_html__( 'Custom Code', 'businextcoin' ),
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'settings_preset', array(
	'title'    => esc_html__( 'Settings Preset', 'businextcoin' ),
	'priority' => $priority ++,
) );

/**
 * Load panel & section files
 */
$files = array(
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/section-color.php',

	BUSINEXTCOIN_CUSTOMIZER_DIR . '/coin_ticker_bar/_panel.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/coin_ticker_bar/general.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/coin_ticker_bar/style-01.php',

	BUSINEXTCOIN_CUSTOMIZER_DIR . '/notification_bar/_panel.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/notification_bar/general.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/notification_bar/style-01.php',

	BUSINEXTCOIN_CUSTOMIZER_DIR . '/top_bar/_panel.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/top_bar/general.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/top_bar/style-01.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/top_bar/style-02.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/top_bar/style-03.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/top_bar/style-04.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/top_bar/style-05.php',

	BUSINEXTCOIN_CUSTOMIZER_DIR . '/header/_panel.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/header/general.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/header/sticky.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/header/style-01.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/header/style-02.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/header/style-03.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/header/style-04.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/header/style-05.php',

	BUSINEXTCOIN_CUSTOMIZER_DIR . '/navigation/_panel.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/navigation/desktop-menu.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/navigation/off-canvas-menu.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/navigation/mobile-menu.php',

	BUSINEXTCOIN_CUSTOMIZER_DIR . '/section-sliders.php',

	BUSINEXTCOIN_CUSTOMIZER_DIR . '/title_bar/_panel.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/title_bar/general.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/title_bar/style-01.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/title_bar/style-02.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/title_bar/style-03.php',

	BUSINEXTCOIN_CUSTOMIZER_DIR . '/footer/_panel.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/footer/general.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/footer/simple.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/footer/style-01.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/footer/style-02.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/footer/style-03.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/footer/style-04.php',

	BUSINEXTCOIN_CUSTOMIZER_DIR . '/advanced/_panel.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/advanced/advanced.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/advanced/pre-loader.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/advanced/light-gallery.php',

	BUSINEXTCOIN_CUSTOMIZER_DIR . '/section-notices.php',

	BUSINEXTCOIN_CUSTOMIZER_DIR . '/shortcode/_panel.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/shortcode/animation.php',

	BUSINEXTCOIN_CUSTOMIZER_DIR . '/section-background.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/section-custom.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/section-error404.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/section-layout.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/section-logo.php',

	BUSINEXTCOIN_CUSTOMIZER_DIR . '/blog/_panel.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/blog/archive.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/blog/single.php',

	BUSINEXTCOIN_CUSTOMIZER_DIR . '/portfolio/_panel.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/portfolio/archive.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/portfolio/single.php',

	BUSINEXTCOIN_CUSTOMIZER_DIR . '/case-study/_panel.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/case-study/archive.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/case-study/single.php',

	BUSINEXTCOIN_CUSTOMIZER_DIR . '/service/_panel.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/service/archive.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/service/single.php',

	BUSINEXTCOIN_CUSTOMIZER_DIR . '/shop/_panel.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/shop/archive.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/shop/single.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/shop/cart.php',

	BUSINEXTCOIN_CUSTOMIZER_DIR . '/search/_panel.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/search/search-page.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/search/search-popup.php',

	BUSINEXTCOIN_CUSTOMIZER_DIR . '/maintenance/_panel.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/maintenance/general.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/maintenance/maintenance.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/maintenance/coming-soon-01.php',

	BUSINEXTCOIN_CUSTOMIZER_DIR . '/section-sharing.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/section-sidebars.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/section-socials.php',
	BUSINEXTCOIN_CUSTOMIZER_DIR . '/section-typography.php',

	BUSINEXTCOIN_CUSTOMIZER_DIR . '/section-preset.php',
);

Businextcoin::require_files( $files );
