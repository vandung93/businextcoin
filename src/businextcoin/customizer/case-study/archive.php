<?php
$section  = 'archive_case_study';
$priority = 1;
$prefix   = 'archive_case_study_';

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'select',
	'settings' => 'archive_case_study_style',
	'label'    => esc_html__( 'Style', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'grid',
	'choices'  => array(
		'grid'         => esc_attr__( 'Grid', 'businextcoin' ),
		'grid-caption' => esc_attr__( 'Grid Caption', 'businextcoin' ),
		'modern-list'  => esc_attr__( 'Modern List', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'select',
	'settings' => 'archive_case_study_thumbnail_size',
	'label'    => esc_html__( 'Thumbnail Size', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '370x250',
	'choices'  => array(
		'480x480' => esc_attr__( '480x480', 'businextcoin' ),
		'370x250' => esc_attr__( '370x250', 'businextcoin' ),
		'570x385' => esc_attr__( '570x385', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'number',
	'settings' => 'archive_case_study_gutter',
	'label'    => esc_html__( 'Gutter', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 30,
	'choices'  => array(
		'min'  => 0,
		'step' => 1,
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'text',
	'settings' => 'archive_case_study_columns',
	'label'    => esc_html__( 'Columns', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'xs:1;sm:2;md:3;lg:3',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'archive_case_study_animation',
	'label'       => esc_html__( 'CSS Animation', 'businextcoin' ),
	'description' => esc_html__( 'Select type of animation for element to be animated when it "enters" the browsers viewport (Note: works only in modern browsers).', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'move-up',
	'choices'     => array(
		'none'             => esc_attr__( 'None', 'businextcoin' ),
		'fade-in'          => esc_attr__( 'Fade In', 'businextcoin' ),
		'move-up'          => esc_attr__( 'Move Up', 'businextcoin' ),
		'scale-up'         => esc_attr__( 'Scale Up', 'businextcoin' ),
		'fall-perspective' => esc_attr__( 'Fall Perspective', 'businextcoin' ),
		'fly'              => esc_attr__( 'Fly', 'businextcoin' ),
		'flip'             => esc_attr__( 'Flip', 'businextcoin' ),
		'helix'            => esc_attr__( 'Helix', 'businextcoin' ),
		'pop-up'           => esc_attr__( 'Pop Up', 'businextcoin' ),
	),
) );
