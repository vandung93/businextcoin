<?php
$section  = 'single_case_study';
$priority = 1;
$prefix   = 'single_case_study_';

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'background',
	'settings'    => $prefix . 'banner_background',
	'label'       => esc_html__( 'Banner Background', 'businextcoin' ),
	'description' => esc_html__( 'Controls the background of banner.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => array(
		'background-color'      => '#222',
		'background-image'      => BUSINEXTCOIN_THEME_IMAGE_URI . '/case-study-banner.jpg',
		'background-repeat'     => 'no-repeat',
		'background-size'       => 'cover',
		'background-attachment' => 'scroll',
		'background-position'   => 'center center',
	),
	'output'      => array(
		array(
			'element' => '.single-case_study .entry-banner',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => $prefix . 'comment_enable',
	'label'       => esc_html__( 'Comments', 'businextcoin' ),
	'description' => esc_html__( 'Turn on to display comments on single case study posts.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '0',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'businextcoin' ),
		'1' => esc_html__( 'On', 'businextcoin' ),
	),
) );
