<?php
$panel    = 'case_study';
$priority = 1;

Businextcoin_Kirki::add_section( 'archive_case_study', array(
	'title'    => esc_html__( 'Case Study Archive', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'single_case_study', array(
	'title'    => esc_html__( 'Case Study Single', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
