<?php
$section  = 'blog_archive';
$priority = 1;
$prefix   = 'blog_archive_';

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => $prefix . 'style',
	'label'       => esc_html__( 'Blog Style', 'businextcoin' ),
	'description' => esc_html__( 'Select blog style that display for archive pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'1' => esc_html__( 'Large Image', 'businextcoin' ),
		'2' => esc_html__( 'Grid Classic', 'businextcoin' ),
		'3' => esc_html__( 'Grid Masonry', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => $prefix . 'columns',
	'label'       => esc_html__( 'Grid Layout Columns', 'businextcoin' ),
	'description' => esc_html__( 'Select columns for blog.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '2',
	'choices'     => array(
		'2' => esc_html__( '2 Columns', 'businextcoin' ),
		'3' => esc_html__( '3 Columns', 'businextcoin' ),
		'4' => esc_html__( '4 Columns', 'businextcoin' ),
	),
) );
