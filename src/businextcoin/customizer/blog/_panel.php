<?php
$panel    = 'blog';
$priority = 1;

/*Businextcoin_Kirki::add_section( 'blog_archive', array(
	'title'    => esc_html__( 'Blog Archive', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );*/

Businextcoin_Kirki::add_section( 'blog_single', array(
	'title'    => esc_html__( 'Blog Single Post', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
