<?php
$section  = 'shortcode_animation';
$priority = 1;
$prefix   = 'shortcode_animation_';

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => $prefix . 'enable',
	'label'       => esc_html__( 'Mobile Animation', 'businextcoin' ),
	'description' => esc_html__( 'Controls the css animations on mobile & tablet.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'desktop',
	'choices'     => array(
		'none'    => esc_html__( 'None', 'businextcoin' ),
		'mobile'  => esc_html__( 'Only Mobile', 'businextcoin' ),
		'desktop' => esc_html__( 'Only Desktop', 'businextcoin' ),
		'both'    => esc_html__( 'Both', 'businextcoin' ),
	),
) );
