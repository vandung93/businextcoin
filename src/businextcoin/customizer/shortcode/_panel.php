<?php
$panel    = 'shortcode';
$priority = 1;

Businextcoin_Kirki::add_section( 'shortcode_animation', array(
	'title'    => esc_html__( 'CSS Animation', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
