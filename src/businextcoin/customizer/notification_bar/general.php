<?php
$section  = 'top_bar';
$priority = 1;
$prefix   = 'top_bar_';

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'global_notification_bar',
	'label'    => esc_html__( 'Default Notification Bar', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'none',
	'choices'  => Businextcoin_Helper::get_notification_bar_list(),
) );

