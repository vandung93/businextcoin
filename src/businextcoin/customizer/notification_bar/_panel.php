<?php
$panel    = 'notification_bar';
$priority = 1;

Businextcoin_Kirki::add_section( 'notification_bar', array(
	'title'    => esc_html__( 'General', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'notification_bar_style_01', array(
	'title'    => esc_html__( 'Notification Style 01', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
