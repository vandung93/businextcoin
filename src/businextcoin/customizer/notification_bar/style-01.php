<?php
$section  = 'notification_bar_style_01';
$priority = 1;
$prefix   = 'notification_bar_style_01_';

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'text',
	'settings' => $prefix . 'text',
	'label'    => esc_html__( 'Text', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => esc_html__( 'Pre-sale has finished. Beta admissions are closed - next pool will be after the crowdsale concludes', 'businextcoin' ),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'      => 'repeater',
	'settings'  => $prefix . 'links',
	'label'     => esc_html__( 'Links', 'businextcoin' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'choices'   => array(
		'labels' => array(
			'add-new-row' => esc_html__( 'Add new link', 'businextcoin' ),
		),
	),
	'row_label' => array(
		'type'  => 'field',
		'field' => 'text',
	),
	'default'   => array(
		array(
			'text' => esc_html__( 'Join Community', 'businextcoin' ),
			'url'  => '#',
		),
		array(
			'text' => esc_html__( 'Read Whitepaper', 'businextcoin' ),
			'url'  => '#',
		),
	),
	'fields'    => array(
		'text' => array(
			'type'    => 'text',
			'label'   => esc_html__( 'Title', 'businextcoin' ),
			'default' => '',
		),
		'url'  => array(
			'type'    => 'text',
			'label'   => esc_html__( 'Url', 'businextcoin' ),
			'default' => '',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'      => 'slider',
	'settings'  => $prefix . 'padding_top',
	'label'     => esc_html__( 'Padding top', 'businextcoin' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => 0,
	'transport' => 'auto',
	'choices'   => array(
		'min'  => 0,
		'max'  => 200,
		'step' => 1,
	),
	'output'    => array(
		array(
			'element'  => '.notification-bar-01',
			'property' => 'padding-top',
			'units'    => 'px',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'      => 'slider',
	'settings'  => $prefix . 'padding_bottom',
	'label'     => esc_html__( 'Padding bottom', 'businextcoin' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => 0,
	'transport' => 'auto',
	'choices'   => array(
		'min'  => 0,
		'max'  => 200,
		'step' => 1,
	),
	'output'    => array(
		array(
			'element'  => '.notification-bar-01',
			'property' => 'padding-bottom',
			'units'    => 'px',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'kirki_typography',
	'settings'    => $prefix . 'typography',
	'label'       => esc_html__( 'Typography', 'businextcoin' ),
	'description' => esc_html__( 'These settings control the typography of texts of top bar section.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => array(
		'font-family'    => '',
		'variant'        => 'normal',
		'line-height'    => '1.35',
		'letter-spacing' => '0',
	),
	'output'      => array(
		array(
			'element' => '.notification-bar-01, .notification-bar-01 a',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'      => 'slider',
	'settings'  => $prefix . 'font_size',
	'label'     => esc_html__( 'Font size', 'businextcoin' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => 14,
	'transport' => 'auto',
	'choices'   => array(
		'min'  => 10,
		'max'  => 50,
		'step' => 1,
	),
	'output'    => array(
		array(
			'element'  => '.notification-bar-01, .notification-bar-01 a',
			'property' => 'font-size',
			'units'    => 'px',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'bg_type',
	'label'    => esc_html__( 'Background Type', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '',
	'choices'  => array(
		''         => esc_attr( 'Solid', 'businextcoin' ),
		'gradient' => esc_attr( 'gradient', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'            => 'color-alpha',
	'settings'        => $prefix . 'bg_color',
	'label'           => esc_html__( 'Background', 'businextcoin' ),
	'description'     => esc_html__( 'Controls the background color of top bar.', 'businextcoin' ),
	'section'         => $section,
	'priority'        => $priority ++,
	'transport'       => 'auto',
	'default'         => '#F65D20',
	'output'          => array(
		array(
			'element'  => '.notification-bar-01',
			'property' => 'background-color',
		),
	),
	'active_callback' => array(
		array(
			'setting'  => 'bg_type',
			'operator' => '==',
			'value'    => '',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'            => 'multicolor',
	'settings'        => $prefix . 'bg_gradient_color',
	'label'           => esc_html__( 'Button Gradient Color', 'businextcoin' ),
	'section'         => $section,
	'priority'        => $priority ++,
	'transport'       => 'auto',
	'choices'         => array(
		'color_1' => esc_attr__( 'Color 1', 'businextcoin' ),
		'color_2' => esc_attr__( 'Color 2', 'businextcoin' ),
	),
	'default'         => array(
		'color_1' => '#A349EA',
		'color_2' => '#6A23D1',
	),
	'active_callback' => array(
		array(
			'setting'  => 'bg_type',
			'operator' => '==',
			'value'    => 'gradient',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'      => 'slider',
	'settings'  => $prefix . 'border_width',
	'label'     => esc_html__( 'Border Bottom Width', 'businextcoin' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => 1,
	'transport' => 'auto',
	'choices'   => array(
		'min'  => 0,
		'max'  => 50,
		'step' => 1,
	),
	'output'    => array(
		array(
			'element'  => '.notification-bar-01',
			'property' => 'border-bottom-width',
			'units'    => 'px',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'      => 'slider',
	'settings'  => $prefix . 'min_height',
	'label'     => esc_html__( 'Min Height', 'businextcoin' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => 50,
	'transport' => 'auto',
	'choices'   => array(
		'min'  => 0,
		'max'  => 500,
		'step' => 10,
	),
	'output'    => array(
		array(
			'element'  => '.notification-bar-01',
			'property' => 'min-height',
			'units'    => 'px',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'border_color',
	'label'       => esc_html__( 'Border Bottom Color', 'businextcoin' ),
	'description' => esc_html__( 'Controls the border bottom color of top bar.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#eee',
	'output'      => array(
		array(
			'element'  => '.notification-bar-01',
			'property' => 'border-bottom-color',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'color',
	'settings'    => $prefix . 'text_color',
	'label'       => esc_html__( 'Text', 'businextcoin' ),
	'description' => esc_html__( 'Controls the color of text on top bar.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#fff',
	'output'      => array(
		array(
			'element'  => '.notification-bar-01',
			'property' => 'color',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'color',
	'settings'    => $prefix . 'link_color',
	'label'       => esc_html__( 'Link Color', 'businextcoin' ),
	'description' => esc_html__( 'Controls the color of links on top bar.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#fff',
	'output'      => array(
		array(
			'element'  => '.notification-bar-01 a',
			'property' => 'color',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'color',
	'settings'    => $prefix . 'link_hover_color',
	'label'       => esc_html__( 'Link Hover Color', 'businextcoin' ),
	'description' => esc_html__( 'Controls the color when hover of links on top bar.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => 'rgba(255, 255, 255, 0.5)',
	'output'      => array(
		array(
			'element'  => '.notification-bar-01 a:hover, .notification-bar-01 a:focus',
			'property' => 'color',
		),
	),
) );
