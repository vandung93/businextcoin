<?php
$section  = 'header';
$priority = 1;
$prefix   = 'header_';

$headers = Businextcoin_Helper::get_header_list( true );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'global_header',
	'label'       => esc_html__( 'Default Header', 'businextcoin' ),
	'description' => esc_html__( 'Select default header type for your site.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '01',
	'choices'     => Businextcoin_Helper::get_header_list(),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'single_page_header_type',
	'label'       => esc_html__( 'Single Page', 'businextcoin' ),
	'description' => esc_html__( 'Select default header type that displays on all single pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '',
	'choices'     => $headers,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'blog_archive_header_type',
	'label'       => esc_html__( 'Blog Archive', 'businextcoin' ),
	'description' => esc_html__( 'Select header type that displays on blog archive pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '02',
	'choices'     => $headers,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'single_post_header_type',
	'label'       => esc_html__( 'Single Blog', 'businextcoin' ),
	'description' => esc_html__( 'Select default header type that displays on all single blog post pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '',
	'choices'     => $headers,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'single_product_header_type',
	'label'       => esc_html__( 'Single Product', 'businextcoin' ),
	'description' => esc_html__( 'Select default header type that displays on all single product pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '',
	'choices'     => $headers,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'single_portfolio_header_type',
	'label'       => esc_html__( 'Single Portfolio', 'businextcoin' ),
	'description' => esc_html__( 'Select default header type that displays on all single portfolio pages.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '',
	'choices'     => $headers,
) );
