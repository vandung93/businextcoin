<?php
$panel    = 'header';
$priority = 1;

Businextcoin_Kirki::add_section( 'header', array(
	'title'    => esc_html__( 'General', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'header_sticky', array(
	'title'    => esc_html__( 'Header Sticky', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'header_style_01', array(
	'title'    => esc_html__( 'Header Style 01', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'header_style_02', array(
	'title'    => esc_html__( 'Header Style 02', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'header_style_03', array(
	'title'    => esc_html__( 'Header Style 03', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'header_style_04', array(
	'title'    => esc_html__( 'Header Style 04', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'header_style_05', array(
	'title'    => esc_html__( 'Header Style 05', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
