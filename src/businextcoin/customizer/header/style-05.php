<?php
$section  = 'header_style_05';
$priority = 1;
$prefix   = 'header_style_05_';

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'overlay',
	'label'    => esc_html__( 'Header Overlay', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '1',
	'choices'  => array(
		'0' => esc_html__( 'No', 'businextcoin' ),
		'1' => esc_html__( 'Yes', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'logo',
	'label'    => esc_html__( 'Logo', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'light',
	'choices'  => array(
		'light' => esc_html__( 'Light', 'businextcoin' ),
		'dark'  => esc_html__( 'Dark', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'search_enable',
	'label'    => esc_html__( 'Search', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '0',
	'choices'  => array(
		'0' => esc_html__( 'Hide', 'businextcoin' ),
		'1' => esc_html__( 'Show', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'cart_enable',
	'label'    => esc_html__( 'Mini Cart', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '0',
	'choices'  => array(
		'0'             => esc_html__( 'Hide', 'businextcoin' ),
		'1'             => esc_html__( 'Show', 'businextcoin' ),
		'hide_on_empty' => esc_html__( 'Hide On Empty', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'language_switcher_enable',
	'label'    => esc_html__( 'Language Switcher', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '0',
	'choices'  => array(
		'0' => esc_html__( 'Hide', 'businextcoin' ),
		'1' => esc_html__( 'Show', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'social_networks_enable',
	'label'    => esc_html__( 'Social Networks', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '0',
	'choices'  => array(
		'0' => esc_html__( 'Hide', 'businextcoin' ),
		'1' => esc_html__( 'Show', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'      => 'slider',
	'settings'  => $prefix . 'border_width',
	'label'     => esc_html__( 'Border Bottom Width', 'businextcoin' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => 0,
	'transport' => 'auto',
	'choices'   => array(
		'min'  => 0,
		'max'  => 50,
		'step' => 1,
	),
	'output'    => array(
		array(
			'element'  => '.header-05 .page-header-inner',
			'property' => 'border-bottom-width',
			'units'    => 'px',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'border_color',
	'label'       => esc_html__( 'Border Color', 'businextcoin' ),
	'description' => esc_html__( 'Controls the border color.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => 'rgba(0, 0, 0, 0)',
	'output'      => array(
		array(
			'element'  => '.header-05 .page-header-inner',
			'property' => 'border-color',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => $prefix . 'box_shadow',
	'label'       => esc_html__( 'Box Shadow', 'businextcoin' ),
	'description' => esc_html__( 'Input box shadow for header. For ex: 0 0 5px #ccc', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'output'      => array(
		array(
			'element'  => '.header-05 .page-header-inner',
			'property' => 'box-shadow',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'background',
	'settings'    => $prefix . 'background',
	'label'       => esc_html__( 'Background', 'businextcoin' ),
	'description' => esc_html__( 'Controls the background of header.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => array(
		'background-color'      => 'rgba(0, 0, 0, 0)',
		'background-image'      => '',
		'background-repeat'     => 'no-repeat',
		'background-size'       => 'cover',
		'background-attachment' => 'scroll',
		'background-position'   => 'center center',
	),
	'output'      => array(
		array(
			'element' => '.header-05 .page-header-inner',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'header_icon_color',
	'label'       => esc_html__( 'Icon Color', 'businextcoin' ),
	'description' => esc_html__( 'Controls the color of icons on header.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#fff',
	'output'      => array(
		array(
			'element'  => '
			.header-05 .header-social-networks a,
			.header-05 .js-wpml-ls-item-toggle,
			.header-05 .page-open-mobile-menu i,
			.header-05 .popup-search-wrap i,
			.header-05 .mini-cart .mini-cart-icon',
			'property' => 'color',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'header_icon_hover_color',
	'label'       => esc_html__( 'Icon Hover Color', 'businextcoin' ),
	'description' => esc_html__( 'Controls the color when hover of icons on header.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#57FCA6',
	'output'      => array(
		array(
			'element'  => '
			.header-05 .header-social-networks a:hover,
			.header-05 .wpml-ls-current-language:hover .wpml-ls-item-toggle,
			.header-05 .popup-search-wrap:hover i,
			.header-05 .mini-cart .mini-cart-icon:hover,
			.header-05 .page-open-mobile-menu:hover i
			',
			'property' => 'color',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'cart_badge_background_color',
	'label'       => esc_html__( 'Cart Badge Background Color', 'businextcoin' ),
	'description' => esc_html__( 'Controls the background color of cart badge.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#57FCA6',
	'output'      => array(
		array(
			'element'  => '.header-05 .mini-cart .mini-cart-icon:after',
			'property' => 'background-color',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'cart_badge_color',
	'label'       => esc_html__( 'Cart Badge Color', 'businextcoin' ),
	'description' => esc_html__( 'Controls the color of cart badge.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#fff',
	'output'      => array(
		array(
			'element'  => '.header-05 .mini-cart .mini-cart-icon:after',
			'property' => 'color',
		),
	),
) );

/*--------------------------------------------------------------
# Navigation
--------------------------------------------------------------*/

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Main Menu Level 1', 'businextcoin' ) . '</div>',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'      => 'spacing',
	'settings'  => $prefix . 'navigation_margin',
	'label'     => esc_html__( 'Menu Margin', 'businextcoin' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'transport' => 'auto',
	'default'   => array(
		'top'    => '0px',
		'bottom' => '0px',
		'left'   => '0px',
		'right'  => '0px',
	),
	'output'    => array(
		array(
			'element'  => array(
				'.desktop-menu .header-05 .menu__container',
			),
			'property' => 'margin',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'      => 'spacing',
	'settings'  => $prefix . 'navigation_item_padding',
	'label'     => esc_html__( 'Item Padding', 'businextcoin' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => array(
		'top'    => '44px',
		'bottom' => '44px',
		'left'   => '17px',
		'right'  => '17px',
	),
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => array(
				'.desktop-menu .header-05 .menu--primary .menu__container > li > a',
			),
			'property' => 'padding',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'      => 'spacing',
	'settings'  => $prefix . 'navigation_item_margin',
	'label'     => esc_html__( 'Item Margin', 'businextcoin' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => array(
		'top'    => '0px',
		'bottom' => '0px',
		'left'   => '0px',
		'right'  => '0px',
	),
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => array(
				'.desktop-menu .header-05  .menu--primary .menu__container > li',
			),
			'property' => 'margin',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'kirki_typography',
	'settings'    => $prefix . 'navigation_typography',
	'label'       => esc_html__( 'Typography', 'businextcoin' ),
	'description' => esc_html__( 'These settings control the typography for menu items.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => array(
		'font-family'    => '',
		'variant'        => '700',
		'line-height'    => '1.26',
		'letter-spacing' => '',
		'text-transform' => '',
	),
	'output'      => array(
		array(
			'element' => '.header-05 .menu--primary a',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'slider',
	'settings'    => $prefix . 'navigation_item_font_size',
	'label'       => esc_html__( 'Font Size', 'businextcoin' ),
	'description' => esc_html__( 'Controls the font size for main menu items.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 15,
	'transport'   => 'auto',
	'choices'     => array(
		'min'  => 10,
		'max'  => 50,
		'step' => 1,
	),
	'output'      => array(
		array(
			'element'  => '.header-05 .menu--primary a',
			'property' => 'font-size',
			'units'    => 'px',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'navigation_link_color',
	'label'       => esc_html__( 'Color', 'businextcoin' ),
	'description' => esc_html__( 'Controls the color for main menu items.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#fff',
	'output'      => array(
		array(
			'element'  => '
			.header-05 .menu--primary a
			',
			'property' => 'color',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'navigation_link_hover_color',
	'label'       => esc_html__( 'Hover Color', 'businextcoin' ),
	'description' => esc_html__( 'Controls the color when hover for main menu items.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#57FCA6',
	'output'      => array(
		array(
			'element'  => '
            .header-05 .menu--primary li:hover > a,
            .header-05 .menu--primary > ul > li > a:hover,
            .header-05 .menu--primary > ul > li > a:focus,
            .header-05 .menu--primary .current-menu-ancestor > a,
            .header-05 .menu--primary .current-menu-item > a',
			'property' => 'color',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'navigation_link_background_color',
	'label'       => esc_html__( 'Background Color', 'businextcoin' ),
	'description' => esc_html__( 'Controls the background color for main menu items.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '',
	'output'      => array(
		array(
			'element'  => '.header-05 .menu--primary .menu__container > li > a',
			'property' => 'background-color',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'navigation_link_hover_background_color',
	'label'       => esc_html__( 'Hover Background Color', 'businextcoin' ),
	'description' => esc_html__( 'Controls the background color when hover for main menu items.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '',
	'output'      => array(
		array(
			'element'  => '
            .header-05 .menu--primary .menu__container > li > a:hover,
            .header-05 .menu--primary .menu__container > li.current-menu-item > a',
			'property' => 'background-color',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Button', 'businextcoin' ) . '</div>',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'text',
	'settings' => $prefix . 'button_text',
	'label'    => esc_html__( 'Button Text', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => esc_html__( 'Sign Up', 'businextcoin' ),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'text',
	'settings' => $prefix . 'button_link',
	'label'    => esc_html__( 'Button link', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '#',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'button_link_target',
	'label'    => esc_html__( 'Open link in a new tab.', 'businextcoin' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '0',
	'choices'  => array(
		'0' => esc_html__( 'No', 'businextcoin' ),
		'1' => esc_html__( 'Yes', 'businextcoin' ),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'multicolor',
	'settings'    => $prefix . 'button_color',
	'label'       => esc_html__( 'Button Color', 'businextcoin' ),
	'description' => esc_html__( 'Controls the color of button.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'choices'     => array(
		'color'      => esc_attr__( 'Color', 'businextcoin' ),
		'background' => esc_attr__( 'Background', 'businextcoin' ),
		'border'     => esc_attr__( 'Border', 'businextcoin' ),
	),
	'default'     => array(
		'color'      => '#57FCA6',
		'background' => 'rgba(255, 255, 255, 0)',
		'border'     => 'rgba(255, 255, 255, 0.1)',
	),
	'output'      => array(
		array(
			'choice'   => 'color',
			'element'  => '.header-05 .tm-button',
			'property' => 'color',
		),
		array(
			'choice'   => 'border',
			'element'  => '.header-05 .tm-button',
			'property' => 'border-color',
		),
		array(
			'choice'   => 'background',
			'element'  => '.header-05 .tm-button',
			'property' => 'background-color',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'multicolor',
	'settings'    => $prefix . 'button_hover_color',
	'label'       => esc_html__( 'Button Hover Color', 'businextcoin' ),
	'description' => esc_html__( 'Controls the color of button when hover.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'choices'     => array(
		'color'      => esc_attr__( 'Color', 'businextcoin' ),
		'background' => esc_attr__( 'Background', 'businextcoin' ),
		'border'     => esc_attr__( 'Border', 'businextcoin' ),
	),
	'default'     => array(
		'color'      => '#fff',
		'background' => '#57FCA6',
		'border'     => '#57FCA6',
	),
	'output'      => array(
		array(
			'choice'   => 'color',
			'element'  => '.header-05 .tm-button:hover',
			'property' => 'color',
		),
		array(
			'choice'   => 'border',
			'element'  => '.header-05 .tm-button:hover',
			'property' => 'border-color',
		),
		array(
			'choice'   => 'background',
			'element'  => '.header-05 .tm-button:hover',
			'property' => 'background-color',
		),
	),
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Header Sticky', 'businextcoin' ) . '</div>',
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'background',
	'settings'    => $prefix . 'sticky_background',
	'label'       => esc_html__( 'Background', 'businextcoin' ),
	'description' => esc_html__( 'Controls the background of header when sticky.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => array(
		'background-color'      => 'rgba(0, 0, 0, 0.6)',
		'background-image'      => '',
		'background-repeat'     => 'no-repeat',
		'background-size'       => 'cover',
		'background-attachment' => 'scroll',
		'background-position'   => 'center center',
	),
	'output'      => array(
		array(
			'element' => '.header-05.headroom--not-top .page-header-inner',
		),
	),
) );
