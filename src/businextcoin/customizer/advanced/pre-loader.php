<?php
$section  = 'pre_loader';
$priority = 1;
$prefix   = 'pre_loader_';

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'toggle',
	'settings'    => $prefix . 'enable',
	'label'       => esc_html__( 'Preloader', 'businextcoin' ),
	'description' => esc_html__( 'Turn on to enable preloader.', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 0,
) );

Businextcoin_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'background_color',
	'label'       => esc_html__( 'Background Color', 'businextcoin' ),
	'description' => esc_html__( 'Controls the background color for pre loader', 'businextcoin' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => 'rgba(0, 0, 0, .95)',
	'output'      => array(
		array(
			'element'  => '.page-loading',
			'property' => 'background-color',
		),
	),
) );
