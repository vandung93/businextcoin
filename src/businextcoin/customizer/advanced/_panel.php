<?php
$panel    = 'advanced';
$priority = 1;

Businextcoin_Kirki::add_section( 'advanced', array(
	'title'    => esc_html__( 'Advanced', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'pre_loader', array(
	'title'    => esc_html__( 'Pre Loader', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Businextcoin_Kirki::add_section( 'light_gallery', array(
	'title'    => esc_html__( 'Light Gallery', 'businextcoin' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
