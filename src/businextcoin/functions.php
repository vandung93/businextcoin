<?php
/**
 * Define constant
 */
$theme = wp_get_theme();

if ( ! empty( $theme['Template'] ) ) {
	$theme = wp_get_theme( $theme['Template'] );
}

if ( ! defined( 'DS' ) ) {
	define( 'DS', DIRECTORY_SEPARATOR );
}

define( 'BUSINEXTCOIN_THEME_NAME', $theme['Name'] );
define( 'BUSINEXTCOIN_THEME_VERSION', $theme['Version'] );
define( 'BUSINEXTCOIN_THEME_DIR', get_template_directory() );
define( 'BUSINEXTCOIN_THEME_URI', get_template_directory_uri() );
define( 'BUSINEXTCOIN_THEME_IMAGE_DIR', get_template_directory() . DS . 'assets' . DS . 'images' );
define( 'BUSINEXTCOIN_THEME_IMAGE_URI', get_template_directory_uri() . DS . 'assets' . DS . 'images' );
define( 'BUSINEXTCOIN_CHILD_THEME_URI', get_stylesheet_directory_uri() );
define( 'BUSINEXTCOIN_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'BUSINEXTCOIN_FRAMEWORK_DIR', get_template_directory() . DS . 'framework' );
define( 'BUSINEXTCOIN_CUSTOMIZER_DIR', BUSINEXTCOIN_THEME_DIR . DS . 'customizer' );
define( 'BUSINEXTCOIN_WIDGETS_DIR', BUSINEXTCOIN_THEME_DIR . DS . 'widgets' );
define( 'BUSINEXTCOIN_VC_MAPS_DIR', BUSINEXTCOIN_THEME_DIR . DS . 'vc-extend' . DS . 'vc-maps' );
define( 'BUSINEXTCOIN_VC_PARAMS_DIR', BUSINEXTCOIN_THEME_DIR . DS . 'vc-extend' . DS . 'vc-params' );
define( 'BUSINEXTCOIN_VC_SHORTCODE_CATEGORY', esc_html__( 'By', 'businextcoin' ) . ' ' . BUSINEXTCOIN_THEME_NAME );
define( 'BUSINEXTCOIN_PROTOCOL', is_ssl() ? 'https' : 'http' );

require_once BUSINEXTCOIN_FRAMEWORK_DIR . '/class-static.php';

$files = array(
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-init.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-global.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-actions-filters.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-admin.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-compatible.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-customize.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-enqueue.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-functions.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-helper.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-minify.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-color.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-maintenance.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-import.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-instagram.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-kirki.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-metabox.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-plugins.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-post-like.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-query.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-custom-css.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-security.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-templates.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-aqua-resizer.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-visual-composer.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-vc-icon-ion.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-vc-icon-flat.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-walker-nav-menu.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-widget.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-widgets.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-footer.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-post-type-blog.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-post-type-portfolio.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-post-type-service.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-post-type-case_study.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-post-type-ccpw.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-woo.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/class-walker-nav-menu-extra-items.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/tgm-plugin-activation.php',
	BUSINEXTCOIN_FRAMEWORK_DIR . '/tgm-plugin-registration.php',
);

/**
 * Load Framework.
 */
Businextcoin::require_files( $files );

/**
 * Init the theme
 */
Businextcoin_Init::instance();
