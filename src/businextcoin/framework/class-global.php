<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Initialize Global Variables
 */
if ( ! class_exists( 'Businextcoin_Global' ) ) {
	class Businextcoin_Global {

		protected static $instance = null;
		protected static $slider = '';
		protected static $slider_position = 'below';
		protected static $coin_ticker_bar_type = '01';
		protected static $notification_bar_type = '01';
		protected static $top_bar_type = '01';
		protected static $header_type = '01';
		protected static $title_bar_type = '01';
		protected static $sidebar_1 = '';
		protected static $sidebar_2 = '';
		protected static $sidebar_position = '';
		protected static $sidebar_status = 'none';
		protected static $footer_type = '';

		function __construct() {
			add_action( 'wp', array( $this, 'init_global_variable' ) );
		}

		public static function instance() {
			if ( null === self::$instance ) {
				self::$instance = new self();
			}

			return self::$instance;
		}

		function init_global_variable() {
			global $businextcoin_page_options;
			if ( is_singular( 'portfolio' ) ) {
				$businextcoin_page_options = unserialize( get_post_meta( get_the_ID(), 'insight_portfolio_options', true ) );
			} elseif ( is_singular( 'post' ) ) {
				$businextcoin_page_options = unserialize( get_post_meta( get_the_ID(), 'insight_post_options', true ) );
			} elseif ( is_singular( 'page' ) ) {
				$businextcoin_page_options = unserialize( get_post_meta( get_the_ID(), 'insight_page_options', true ) );
			} elseif ( is_singular( 'product' ) ) {
				$businextcoin_page_options = unserialize( get_post_meta( get_the_ID(), 'insight_product_options', true ) );
			} elseif ( is_singular( 'case_study' ) ) {
				$businextcoin_page_options = unserialize( get_post_meta( get_the_ID(), 'insight_case_study_options', true ) );
			} elseif ( is_singular( 'service' ) ) {
				$businextcoin_page_options = unserialize( get_post_meta( get_the_ID(), 'insight_service_options', true ) );
			}
			if ( function_exists( 'is_shop' ) && is_shop() ) {
				// Get page id of shop.
				$page_id                = wc_get_page_id( 'shop' );
				$businextcoin_page_options = unserialize( get_post_meta( $page_id, 'insight_page_options', true ) );
			}

			$this->set_slider();
			$this->set_coin_ticker_bar_type();
			$this->set_notification_bar_type();
			$this->set_top_bar_type();
			$this->set_header_type();
			$this->set_title_bar_type();
			$this->set_sidebars();
			$this->set_footer_type();
		}

		function set_slider() {
			$alias    = Businextcoin_Helper::get_post_meta( 'revolution_slider', '' );
			$position = Businextcoin_Helper::get_post_meta( 'slider_position', '' );

			if ( $alias === '' ) {
				if ( is_search() && ! is_post_type_archive( 'product' ) ) {
					$alias    = Businextcoin::setting( 'search_page_rev_slider' );
					$position = Businextcoin::setting( 'search_page_slider_position' );
				} elseif ( is_post_type_archive( 'product' ) || ( function_exists( 'is_product_taxonomy' ) && is_product_taxonomy() ) ) {
					$alias    = Businextcoin::setting( 'product_archive_page_rev_slider' );
					$position = Businextcoin::setting( 'product_archive_page_slider_position' );
				} elseif ( is_post_type_archive( 'portfolio' ) || Businextcoin_Portfolio::is_taxonomy() ) {
					$alias    = Businextcoin::setting( 'portfolio_archive_page_rev_slider' );
					$position = Businextcoin::setting( 'portfolio_archive_page_slider_position' );
				} elseif ( is_archive() ) {
					$alias    = Businextcoin::setting( 'blog_archive_page_rev_slider' );
					$position = Businextcoin::setting( 'blog_archive_page_slider_position' );
				} elseif ( is_home() ) {
					$alias    = Businextcoin::setting( 'home_page_rev_slider' );
					$position = Businextcoin::setting( 'home_page_slider_position' );
				}
			}

			self::$slider          = $alias;
			self::$slider_position = $position;
		}

		function get_slider_alias() {
			return self::$slider;
		}

		function get_slider_position() {
			return self::$slider_position;
		}

		function set_coin_ticker_bar_type() {
			$type = Businextcoin_Helper::get_post_meta( 'coin_ticker_bar_type', '' );

			if ( $type === '' ) {
				$type = Businextcoin::setting( 'global_coin_ticker_bar' );
			}

			self::$coin_ticker_bar_type = $type;
		}

		function get_coin_ticker_bar_type() {
			return self::$coin_ticker_bar_type;
		}

		function set_notification_bar_type() {
			$type = Businextcoin_Helper::get_post_meta( 'notification_bar_type', '' );

			if ( $type === '' ) {
				$type = Businextcoin::setting( 'global_notification_bar' );
			}

			self::$notification_bar_type = $type;
		}

		function get_notification_bar_type() {
			return self::$notification_bar_type;
		}

		function set_top_bar_type() {
			$type = Businextcoin_Helper::get_post_meta( 'top_bar_type', '' );

			if ( $type === '' ) {
				$type = Businextcoin::setting( 'global_top_bar' );
			}

			self::$top_bar_type = $type;
		}

		function get_top_bar_type() {
			return self::$top_bar_type;
		}

		function set_header_type() {
			$header_type = Businextcoin_Helper::get_post_meta( 'header_type', '' );
			if ( $header_type === '' ) {
				if ( is_singular( 'post' ) ) {
					$header_type = Businextcoin::setting( 'single_post_header_type' );
				} elseif ( is_singular( 'portfolio' ) ) {
					$header_type = Businextcoin::setting( 'single_portfolio_header_type' );
				} elseif ( is_singular( 'product' ) ) {
					$header_type = Businextcoin::setting( 'single_product_header_type' );
				} elseif ( is_singular( 'page' ) ) {
					$header_type = Businextcoin::setting( 'single_page_header_type' );
				} elseif ( is_archive() ) {
					$header_type = Businextcoin::setting( 'blog_archive_header_type' );
				} else {
					$header_type = Businextcoin::setting( 'global_header' );
				}
			}

			if ( $header_type === '' ) {
				$header_type = Businextcoin::setting( 'global_header' );
			}

			self::$header_type = $header_type;
		}

		function get_header_type() {
			return self::$header_type;
		}

		function set_title_bar_type() {
			$title_bar_layout = Businextcoin_Helper::get_post_meta( 'page_title_bar_layout', 'default' );

			if ( $title_bar_layout === 'default' ) {
				if ( is_singular( 'post' ) ) {
					$title_bar_layout = Businextcoin::setting( 'single_post_title_bar_layout' );
				} elseif ( is_singular( 'page' ) ) {
					$title_bar_layout = Businextcoin::setting( 'single_page_title_bar_layout' );
				} elseif ( is_singular( 'product' ) ) {
					$title_bar_layout = Businextcoin::setting( 'single_product_title_bar_layout' );
				} elseif ( is_singular( 'portfolio' ) ) {
					$title_bar_layout = Businextcoin::setting( 'single_portfolio_title_bar_layout' );
				} elseif ( is_singular( 'case_study' ) ) {
					$title_bar_layout = Businextcoin::setting( 'single_case_study_title_bar_layout' );
				} elseif ( is_singular( 'service' ) ) {
					$title_bar_layout = Businextcoin::setting( 'single_service_title_bar_layout' );
				} elseif ( is_archive() ) {
					$title_bar_layout = Businextcoin::setting( 'blog_archive_title_bar_layout' );
				} elseif ( is_home() ) {
					$title_bar_layout = Businextcoin::setting( 'home_page_title_bar_layout' );
				} else {
					$title_bar_layout = Businextcoin::setting( 'title_bar_layout' );
				}

				if ( $title_bar_layout === 'default' ) {
					$title_bar_layout = Businextcoin::setting( 'title_bar_layout' );
				}
			}

			self::$title_bar_type = $title_bar_layout;
		}

		function get_title_bar_type() {
			return self::$title_bar_type;
		}

		function set_sidebars() {
			if ( is_search() && ! is_post_type_archive( 'product' ) ) {
				$page_sidebar1    = Businextcoin::setting( 'search_page_sidebar_1' );
				$page_sidebar2    = Businextcoin::setting( 'search_page_sidebar_2' );
				$sidebar_position = Businextcoin::setting( 'search_page_sidebar_position' );
			} elseif ( is_post_type_archive( 'product' ) || ( function_exists( 'is_product_taxonomy' ) && is_product_taxonomy() ) ) {
				$page_sidebar1    = Businextcoin::setting( 'product_archive_page_sidebar_1' );
				$page_sidebar2    = Businextcoin::setting( 'product_archive_page_sidebar_2' );
				$sidebar_position = Businextcoin::setting( 'product_archive_page_sidebar_position' );
			} elseif ( is_post_type_archive( 'portfolio' ) || Businextcoin_Portfolio::is_taxonomy() ) {
				$page_sidebar1    = Businextcoin::setting( 'portfolio_archive_page_sidebar_1' );
				$page_sidebar2    = Businextcoin::setting( 'portfolio_archive_page_sidebar_2' );
				$sidebar_position = Businextcoin::setting( 'portfolio_archive_page_sidebar_position' );
			} elseif ( is_post_type_archive( 'case_study' ) || is_tax( get_object_taxonomies( 'case_study' ) ) ) {
				$page_sidebar1    = Businextcoin::setting( 'case_study_archive_page_sidebar_1' );
				$page_sidebar2    = Businextcoin::setting( 'case_study_archive_page_sidebar_2' );
				$sidebar_position = Businextcoin::setting( 'case_study_archive_page_sidebar_position' );
			} elseif ( is_post_type_archive( 'service' ) || is_tax( get_object_taxonomies( 'service' ) ) ) {
				$page_sidebar1    = Businextcoin::setting( 'service_archive_page_sidebar_1' );
				$page_sidebar2    = Businextcoin::setting( 'service_archive_page_sidebar_2' );
				$sidebar_position = Businextcoin::setting( 'service_archive_page_sidebar_position' );
			} elseif ( is_archive() ) {
				$page_sidebar1    = Businextcoin::setting( 'blog_archive_page_sidebar_1' );
				$page_sidebar2    = Businextcoin::setting( 'blog_archive_page_sidebar_2' );
				$sidebar_position = Businextcoin::setting( 'blog_archive_page_sidebar_position' );
			} elseif ( is_home() ) {
				$page_sidebar1    = Businextcoin::setting( 'home_page_sidebar_1' );
				$page_sidebar2    = Businextcoin::setting( 'home_page_sidebar_2' );
				$sidebar_position = Businextcoin::setting( 'home_page_sidebar_position' );
			} elseif ( is_singular( 'post' ) ) {
				$page_sidebar1    = Businextcoin_Helper::get_post_meta( 'page_sidebar_1', 'default' );
				$page_sidebar2    = Businextcoin_Helper::get_post_meta( 'page_sidebar_2', 'default' );
				$sidebar_position = Businextcoin_Helper::get_post_meta( 'page_sidebar_position', 'default' );

				if ( $page_sidebar1 === 'default' ) {
					$page_sidebar1 = Businextcoin::setting( 'post_page_sidebar_1' );
				}

				if ( $page_sidebar2 === 'default' ) {
					$page_sidebar2 = Businextcoin::setting( 'post_page_sidebar_2' );
				}

				if ( $sidebar_position === 'default' ) {
					$sidebar_position = Businextcoin::setting( 'post_page_sidebar_position' );
				}

			} elseif ( is_singular( 'portfolio' ) ) {
				$page_sidebar1    = Businextcoin_Helper::get_post_meta( 'page_sidebar_1', 'default' );
				$page_sidebar2    = Businextcoin_Helper::get_post_meta( 'page_sidebar_2', 'default' );
				$sidebar_position = Businextcoin_Helper::get_post_meta( 'page_sidebar_position', 'default' );

				if ( $page_sidebar1 === 'default' ) {
					$page_sidebar1 = Businextcoin::setting( 'portfolio_page_sidebar_1' );
				}

				if ( $page_sidebar2 === 'default' ) {
					$page_sidebar2 = Businextcoin::setting( 'portfolio_page_sidebar_2' );
				}

				if ( $sidebar_position === 'default' ) {
					$sidebar_position = Businextcoin::setting( 'portfolio_page_sidebar_position' );
				}
			} elseif ( is_singular( 'case_study' ) ) {
				$page_sidebar1    = Businextcoin_Helper::get_post_meta( 'page_sidebar_1', 'default' );
				$page_sidebar2    = Businextcoin_Helper::get_post_meta( 'page_sidebar_2', 'default' );
				$sidebar_position = Businextcoin_Helper::get_post_meta( 'page_sidebar_position', 'default' );

				if ( $page_sidebar1 === 'default' ) {
					$page_sidebar1 = Businextcoin::setting( 'case_study_page_sidebar_1' );
				}

				if ( $page_sidebar2 === 'default' ) {
					$page_sidebar2 = Businextcoin::setting( 'case_study_page_sidebar_2' );
				}

				if ( $sidebar_position === 'default' ) {
					$sidebar_position = Businextcoin::setting( 'case_study_page_sidebar_position' );
				}
			} elseif ( is_singular( 'service' ) ) {
				$page_sidebar1    = Businextcoin_Helper::get_post_meta( 'page_sidebar_1', 'default' );
				$page_sidebar2    = Businextcoin_Helper::get_post_meta( 'page_sidebar_2', 'default' );
				$sidebar_position = Businextcoin_Helper::get_post_meta( 'page_sidebar_position', 'default' );

				if ( $page_sidebar1 === 'default' ) {
					$page_sidebar1 = Businextcoin::setting( 'service_page_sidebar_1' );
				}

				if ( $page_sidebar2 === 'default' ) {
					$page_sidebar2 = Businextcoin::setting( 'service_page_sidebar_2' );
				}

				if ( $sidebar_position === 'default' ) {
					$sidebar_position = Businextcoin::setting( 'service_page_sidebar_position' );
				}
			} elseif ( is_singular( 'product' ) ) {
				$page_sidebar1    = Businextcoin_Helper::get_post_meta( 'page_sidebar_1', 'default' );
				$page_sidebar2    = Businextcoin_Helper::get_post_meta( 'page_sidebar_2', 'default' );
				$sidebar_position = Businextcoin_Helper::get_post_meta( 'page_sidebar_position', 'default' );

				if ( $page_sidebar1 === 'default' ) {
					$page_sidebar1 = Businextcoin::setting( 'product_page_sidebar_1' );
				}

				if ( $page_sidebar2 === 'default' ) {
					$page_sidebar2 = Businextcoin::setting( 'product_page_sidebar_2' );
				}

				if ( $sidebar_position === 'default' ) {
					$sidebar_position = Businextcoin::setting( 'product_page_sidebar_position' );
				}

			} else {
				$page_sidebar1    = Businextcoin_Helper::get_post_meta( 'page_sidebar_1', 'default' );
				$page_sidebar2    = Businextcoin_Helper::get_post_meta( 'page_sidebar_2', 'default' );
				$sidebar_position = Businextcoin_Helper::get_post_meta( 'page_sidebar_position', 'default' );

				if ( $page_sidebar1 === 'default' ) {
					$page_sidebar1 = Businextcoin::setting( 'page_sidebar_1' );
				}

				if ( $page_sidebar2 === 'default' ) {
					$page_sidebar2 = Businextcoin::setting( 'page_sidebar_2' );
				}

				if ( $sidebar_position === 'default' ) {
					$sidebar_position = Businextcoin::setting( 'page_sidebar_position' );
				}
			}

			if ( ! is_active_sidebar( $page_sidebar1 ) ) {
				$page_sidebar1 = 'none';
			}

			if ( ! is_active_sidebar( $page_sidebar2 ) ) {
				$page_sidebar2 = 'none';
			}

			self::$sidebar_1        = $page_sidebar1;
			self::$sidebar_2        = $page_sidebar2;
			self::$sidebar_position = $sidebar_position;

			if ( $page_sidebar1 !== 'none' || $page_sidebar2 !== 'none' ) {
				self::$sidebar_status = 'one';
			}

			if ( $page_sidebar1 !== 'none' && $page_sidebar2 !== 'none' ) {
				self::$sidebar_status = 'both';
			}
		}

		function get_sidebar_1() {
			return self::$sidebar_1;
		}

		function get_sidebar_2() {
			return self::$sidebar_2;
		}

		function get_sidebar_position() {
			return self::$sidebar_position;
		}

		function get_sidebar_status() {
			return self::$sidebar_status;
		}

		function set_footer_type() {
			$footer = Businextcoin_Helper::get_post_meta( 'footer_page', 'default' );

			if ( $footer === 'default' ) {
				if ( is_singular( 'service' ) ) {
					$footer = Businextcoin::setting( 'single_service_footer_page' );
				} elseif ( is_singular( 'case_study' ) ) {
					$footer = Businextcoin::setting( 'single_case_study_footer_page' );
				}
			}

			if ( $footer === 'default' ) {
				$footer = Businextcoin::setting( 'footer_page' );
			}

			self::$footer_type = $footer;
		}

		function get_footer_type() {
			return self::$footer_type;
		}
	}

	new Businextcoin_Global();
}
