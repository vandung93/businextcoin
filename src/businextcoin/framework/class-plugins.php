<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Plugin installation and activation for WordPress themes
 */
if ( ! class_exists( 'Businextcoin_Register_Plugins' ) ) {
	class Businextcoin_Register_Plugins {

		public function __construct() {
			add_filter( 'insight_core_tgm_plugins', array( $this, 'register_required_plugins' ) );
		}

		public function register_required_plugins() {
			/*
			 * Array of plugin arrays. Required keys are name and slug.
			 * If the source is NOT from the .org repo, then source is also required.
			 */
			$plugins = array(
				array(
					'name'     => esc_html__( 'Insight Core', 'businextcoin' ),
					'slug'     => 'insight-core',
					'source'   => 'https://www.dropbox.com/s/aqb5sscpop2zbvk/insight-core-1.7.3.zip?dl=1',
					'version'  => '1.7.3',
					'required' => true,
				),
				array(
					'name'     => esc_html__( 'Revolution Slider', 'businextcoin' ),
					'slug'     => 'revslider',
					'source'  => 'https://www.dropbox.com/s/anj8r2m6jtjxfv4/revslider.zip?dl=1',
					'version' => '6.2.22',
					'required' => true,
				),
				array(
					'name'     => esc_html__( 'WPBakery Page Builder', 'businextcoin' ),
					'slug'     => 'js_composer',
					'source'   => 'https://www.dropbox.com/s/bgoxpk8gzm6j9rg/js_composer.zip?dl=1',
					'version'  => '6.3.0',
					'required' => true,
				),
				array(
					'name'    => esc_html__( 'WPBakery Page Builder Clipboard', 'businextcoin' ),
					'slug'    => 'vc_clipboard',
					'source'  => 'https://www.dropbox.com/s/m0icyk8s8npef5p/vc_clipboard.zip?dl=1',
					'version' => '4.5.7',
				),
				array(
					'name' => esc_html__( 'Contact Form 7', 'businextcoin' ),
					'slug' => 'contact-form-7',
				),
				array(
					'name' => esc_html__( 'MailChimp for WordPress', 'businextcoin' ),
					'slug' => 'mailchimp-for-wp',
				),
				array(
					'name' => esc_html__( 'WP-PostViews', 'businextcoin' ),
					'slug' => 'wp-postviews',
				),
				array(
					'name' => esc_html__( 'Cryptocurrency Price Ticker Widget', 'businextcoin' ),
					'slug' => 'cryptocurrency-price-ticker-widget',
				),
				array(
					'name' => esc_html__( 'WooCommerce', 'businextcoin' ),
					'slug' => 'woocommerce',
				),
				array(
					'name'     => esc_html__( 'WPC Smart Compare for WooCommerce', 'businextcoin' ),
					'slug'     => 'woo-smart-compare',
					'required' => false,
				),
				array(
					'name'     => esc_html__( 'WPC Smart Wishlist for WooCommerce', 'businextcoin' ),
					'slug'     => 'woo-smart-wishlist',
					'required' => false,
				),
			);

			return $plugins;
		}

	}

	new Businextcoin_Register_Plugins();
}
