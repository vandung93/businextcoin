<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Setup for customizer of this theme
 */
if ( ! class_exists( 'Businextcoin_Customize' ) ) {
	class Businextcoin_Customize {

		public function __construct() {
			// Build URL for customizer.
			add_filter( 'kirki_values_get_value', array( $this, 'kirki_db_get_theme_mod_value' ), 10, 2 );

			add_filter( 'kirki_theme_stylesheet', array( $this, 'change_inline_stylesheet' ), 10, 3 );

			add_filter( 'kirki_load_fontawesome', '__return_false' );

			// Remove unused native sections and controls.
			add_action( 'customize_register', array( $this, 'remove_customizer_sections' ) );

			add_filter( 'kirki_fonts_standard_fonts', array( $this, 'add_custom_font' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'add_custom_font_css' ) );

			// Load customizer sections when all widgets init.
			add_action( 'init', array( $this, 'load_customizer' ), 99 );

			add_action( 'customize_controls_init', array(
				$this,
				'customize_preview_css',
			) );
		}

		public function change_inline_stylesheet() {
			return 'businextcoin-style';
		}

		function add_custom_font( $standard_fonts ) {
			$fonts['sf_pro_display'] = array(
				'label'    => 'SF Pro Display',
				'variants' => array( 'regular', 'italic', 700 ),
				'stack'    => 'SF Pro Display, sans-serif',
			);

			return $fonts;
		}

		function add_custom_font_css() {
			$typo_fields = Businextcoin_Kirki::get_typography_fields_id();

			if ( ! is_array( $typo_fields ) || empty( $typo_fields ) ) {
				return;
			}

			foreach ( $typo_fields as $field ) {
				$value = Businextcoin::setting( $field );

				if ( is_array( $value ) && isset( $value['font-family'] ) && $value['font-family'] == 'SF Pro Display' ) {
					wp_enqueue_style( 'sf-pro', BUSINEXTCOIN_THEME_URI . '/assets/fonts/sf-pro/font-sf-pro.css', null, null );

					break;
				}
			}
		}

		/**
		 * Add customize preview css
		 */
		public function customize_preview_css() {
			wp_enqueue_style( 'kirki-custom-css', BUSINEXTCOIN_THEME_URI . '/assets/admin/css/customizer.min.css' );
		}

		/**
		 * Load Customizer.
		 */
		public function load_customizer() {
			Businextcoin::require_file( BUSINEXTCOIN_THEME_DIR . '/customizer/customizer.php' );
		}

		/**
		 * Remove unused native sections and controls
		 *
		 * @since 0.9.3
		 *
		 * @param $wp_customize
		 */
		public function remove_customizer_sections( $wp_customize ) {
			$wp_customize->remove_section( 'nav' );
			$wp_customize->remove_section( 'colors' );
			$wp_customize->remove_section( 'background_image' );
			$wp_customize->remove_section( 'header_image' );

			$wp_customize->get_section( 'title_tagline' )->priority = '100';

			$wp_customize->remove_control( 'blogdescription' );
			$wp_customize->remove_control( 'display_header_text' );
		}

		/**
		 * Build URL for customizer
		 *
		 * @param $value
		 * @param $setting
		 *
		 * @return mixed
		 */
		public function kirki_db_get_theme_mod_value( $value, $setting ) {
			static $settings;
			static $count = 1;

			// Make preset in meta box.
			if ( ! is_customize_preview() && $count === 2 ) {
				$presets = apply_filters( 'businextcoin_page_meta_box_presets', array() );
				if ( ! empty( $presets ) ) {
					foreach ( $presets as $preset ) {
						$page_preset_value = Businextcoin_Helper::get_post_meta( $preset, '-1' );
						$_GET[ $preset ]   = $page_preset_value;
					}
				}
			}
			// Setup url.
			if ( is_null( $settings ) && $count == 2 ) {

				$settings = array();

				if ( ! empty( $_GET ) ) {
					foreach ( $_GET as $key => $query_value ) {
						if ( ! empty( Kirki::$fields[ $key ] ) ) {
							$settings[ $key ] = $query_value;

							if ( is_array( Kirki::$fields[ $key ] ) && 'kirki-preset' == Kirki::$fields[ $key ]['type'] && ! empty( Kirki::$fields[ $key ]['choices'] ) && ! empty( Kirki::$fields[ $key ]['choices'][ $query_value ] ) && ! empty( Kirki::$fields[ $key ]['choices'][ $query_value ]['settings'] ) ) {
								foreach ( Kirki::$fields[ $key ]['choices'][ $query_value ]['settings'] as $kirki_setting => $kirki_value ) {
									$settings[ $kirki_setting ] = $kirki_value;
								}
							}
						}
					}
				}
			}

			$count ++;

			if ( isset ( $settings[ $setting ] ) ) {
				return $settings[ $setting ];
			}

			return $value;
		}

	}

	new Businextcoin_Customize();
}
