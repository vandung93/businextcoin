<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Helper functions
 */
class Businextcoin_Helper {

	public static function get_post_meta( $name, $default = false ) {
		global $businextcoin_page_options;
		if ( $businextcoin_page_options != false && isset( $businextcoin_page_options[ $name ] ) ) {
			return $businextcoin_page_options[ $name ];
		}

		return $default;
	}

	public static function get_the_post_meta( $options, $name, $default = false ) {
		if ( $options != false && isset( $options[ $name ] ) ) {
			return $options[ $name ];
		}

		return $default;
	}

	/**
	 * @return array
	 */
	public static function get_list_revslider() {
		global $wpdb;
		$revsliders = array(
			'' => esc_html__( 'Select a slider', 'businextcoin' ),
		);

		if ( function_exists( 'rev_slider_shortcode' ) ) {

			$table_name = $wpdb->prefix . "revslider_sliders";
			$query      = $wpdb->prepare( "SELECT * FROM $table_name WHERE type != %s ORDER BY title ASC", 'template' );
			$results    = $wpdb->get_results( $query );
			if ( ! empty( $results ) ) {
				foreach ( $results as $result ) {
					$revsliders[ $result->alias ] = $result->title;
				}
			}
		}

		return $revsliders;
	}

	/**
	 * @return array|int|WP_Error
	 */
	public static function get_all_menus() {
		$args = array(
			'hide_empty' => true,
		);

		$menus   = get_terms( 'nav_menu', $args );
		$results = array();

		foreach ( $menus as $key => $menu ) {
			$results[ $menu->slug ] = $menu->name;
		}
		$results[''] = esc_html__( 'Default Menu', 'businextcoin' );

		return $results;
	}

	/**
	 * @param bool $default_option
	 *
	 * @return array
	 */
	public static function get_registered_sidebars( $default_option = false, $empty_option = true ) {
		global $wp_registered_sidebars;
		$sidebars = array();
		if ( $empty_option == true ) {
			$sidebars['none'] = esc_html__( 'No Sidebar', 'businextcoin' );
		}
		if ( $default_option == true ) {
			$sidebars['default'] = esc_html__( 'Default', 'businextcoin' );
		}
		foreach ( $wp_registered_sidebars as $sidebar ) {
			$sidebars[ $sidebar['id'] ] = $sidebar['name'];
		}

		return $sidebars;
	}

	/**
	 * Get list sidebar positions
	 *
	 * @return array
	 */
	public static function get_list_sidebar_positions( $default = false ) {
		$positions = array(
			'left'  => esc_html__( 'Left', 'businextcoin' ),
			'right' => esc_html__( 'Right', 'businextcoin' ),
		);


		if ( $default == true ) {
			$positions['default'] = esc_html__( 'Default', 'businextcoin' );
		}

		return $positions;
	}

	/**
	 * Get content of file
	 *
	 * @param string $path
	 *
	 * @return mixed
	 */
	static function get_file_contents( $path = '' ) {
		$content = '';
		if ( $path !== '' ) {
			global $wp_filesystem;

			Businextcoin::require_file( ABSPATH . '/wp-admin/includes/file.php' );
			WP_Filesystem();

			if ( file_exists( $path ) ) {
				$content = $wp_filesystem->get_contents( $path );
			}
		}

		return $content;
	}

	/**
	 * @param $var
	 *
	 * Output anything in debug bar.
	 */
	public static function d( $var ) {
		if ( ! class_exists( 'Debug_Bar' ) || ! function_exists( 'kint_debug_ob' ) ) {
			return;
		}

		ob_start( 'kint_debug_ob' );
		d( $var );
		ob_end_flush();
	}

	public static function strposa( $haystack, $needle, $offset = 0 ) {
		if ( ! is_array( $needle ) ) {
			$needle = array( $needle );
		}
		foreach ( $needle as $query ) {
			if ( strpos( $haystack, $query, $offset ) !== false ) {
				return true;
			} // stop on first true result
		}

		return false;
	}

	/**
	 * Get size information for all currently-registered image sizes.
	 *
	 * @global $_wp_additional_image_sizes
	 * @uses   get_intermediate_image_sizes()
	 * @return array $sizes Data for all currently-registered image sizes.
	 */
	public static function get_image_sizes() {
		global $_wp_additional_image_sizes;

		$sizes = array( 'full' => 'full' );

		foreach ( get_intermediate_image_sizes() as $_size ) {
			if ( in_array( $_size, array( 'thumbnail', 'medium', 'medium_large', 'large' ) ) ) {
				$_size_w                               = get_option( "{$_size}_size_w" );
				$_size_h                               = get_option( "{$_size}_size_h" );
				$sizes["$_size {$_size_w}x{$_size_h}"] = $_size;
			} elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {
				$sizes["$_size {$_wp_additional_image_sizes[ $_size ]['width']}x{$_wp_additional_image_sizes[ $_size ]['height']}"] = $_size;
			}
		}

		return $sizes;
	}

	public static function get_attachment_info( $attachment_id ) {
		$attachment = get_post( $attachment_id );

		if ( $attachment === null ) {
			return false;
		}

		return array(
			'alt'         => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
			'caption'     => $attachment->post_excerpt,
			'description' => $attachment->post_content,
			'href'        => get_permalink( $attachment->ID ),
			'src'         => $attachment->guid,
			'title'       => $attachment->post_title,
		);
	}

	public static function w3c_iframe( $iframe ) {
		$iframe = str_replace( 'frameborder="0"', '', $iframe );
		$iframe = str_replace( 'frameborder="no"', '', $iframe );
		$iframe = str_replace( 'scrolling="no"', '', $iframe );
		$iframe = str_replace( 'gesture="media"', '', $iframe );
		$iframe = str_replace( 'allow="encrypted-media"', '', $iframe );

		return $iframe;
	}

	public static function get_md_media_query() {
		return '@media (max-width: 991px)';
	}

	public static function get_sm_media_query() {
		return '@media (max-width: 767px)';
	}

	public static function get_xs_media_query() {
		return '@media (max-width: 554px)';
	}

	public static function aq_resize( $args = array() ) {
		$defaults = array(
			'url'     => '',
			'width'   => null,
			'height'  => null,
			'crop'    => true,
			'single'  => true,
			'upscale' => false,
			'echo'    => false,
		);

		$args  = wp_parse_args( $args, $defaults );
		$image = aq_resize( $args['url'], $args['width'], $args['height'], $args['crop'], $args['single'], $args['upscale'] );

		if ( $image === false ) {
			$image = $args['url'];
		}

		return $image;
	}


	public static function get_lazy_load_image_by_id( $attachment_id, $args = array() ) {
		$attachment = Businextcoin_Helper::get_attachment_info( $attachment_id );

		if ( $attachment === false ) {
			return false;
		}

		$url = $attachment['src'];
		$alt = $attachment['alt'] !== '' ? $attachment['alt'] : $attachment['title'];

		$args['url'] = $url;
		$args['alt'] = $alt;

		self::get_lazy_load_image( $args );
	}

	public static function get_lazy_load_image( $args = array() ) {
		$defaults = array(
			'url'         => '',
			'width'       => null,
			'height'      => null,
			'crop'        => true,
			'single'      => true,
			'upscale'     => false,
			'echo'        => false,
			'placeholder' => '',
			'src'         => '',
			'alt'         => '',
			'full_size'   => false,
		);

		$args = wp_parse_args( $args, $defaults );

		if ( ! isset( $args['lazy'] ) ) {
			$lazy_load_enable = Businextcoin::setting( 'lazy_image_enable' );

			if ( $lazy_load_enable ) {
				$args['lazy'] = true;
			} else {
				$args['lazy'] = false;
			}
		}

		$image      = false;
		$attributes = array();

		if ( $args['full_size'] === false ) {
			$image = aq_resize( $args['url'], $args['width'], $args['height'], $args['crop'], $args['single'], $args['upscale'] );
		}

		if ( $image === false ) {
			$image = $args['url'];
		}

		$output   = '';
		$_classes = '';

		if ( $args['lazy'] === true ) {
			if ( $args['full_size'] === false ) {
				$placeholder_w = round( $args['width'] / 10 );
				$placeholder_h = $args['height'];

				if ( $args['height'] !== 9999 ) {
					$placeholder_h = round( $args['height'] / 10 );
				}
			} else {
				$placeholder_w = 50;
				$placeholder_h = 9999;
				$args['crop']  = false;
			}

			$placeholder_image = aq_resize( $image, $placeholder_w, $placeholder_h, $args['crop'], $args['single'], $args['upscale'] );

			$attributes[] = 'src="' . $placeholder_image . '"';
			$attributes[] = 'data-src="' . $image . '"';

			if ( $args['width'] !== '' && $args['width'] !== null ) {
				$attributes[] = 'width="' . $args['width'] . '"';
			}

			if ( $args['height'] !== '' && $args['height'] !== null && $args['height'] !== 9999 ) {
				$attributes[] = 'height="' . $args['height'] . '"';
			}

			$_classes .= ' tm-lazy-load';
		} else {
			$attributes[] = 'src="' . $image . '"';
		}

		$attributes[] = 'alt="' . $args['alt'] . '"';

		if ( $_classes !== '' ) {
			$attributes[] = 'class="' . $_classes . '"';
		}

		$output .= '<img ' . implode( ' ', $attributes ) . ' />';

		if ( $args['echo'] === true ) {
			echo '' . $output;
		} else {
			return $output;
		}
	}

	public static function get_attachment_url_by_id( $id = '', $size = 'full', $width = '', $height = '' ) {
		$url = wp_get_attachment_image_url( $id, 'full' );

		if ( $url === false ) {
			return false;
		}

		if ( $size !== 'full' && $size !== 'custom' ) {
			$_sizes = explode( 'x', $size );
			$width  = $_sizes[0];
			$height = $_sizes[1];
		}

		if ( $size !== 'full' && $width !== '' && $height !== '' ) {
			$url = aq_resize( $url, $width, $height, true );
		}

		return $url;
	}

	public static function get_attachment_by_id( $id = '', $size = 'full', $width = '', $height = '', $echo = true ) {
		$image_full = self::get_attachment_info( $id );

		if ( $image_full === false ) {
			return false;
		}

		$url = $image_full['src'];
		$alt = $image_full['alt'] !== '' ? $image_full['alt'] : $image_full['title'];

		if ( $url === false ) {
			return false;
		}

		if ( $size !== 'full' && $size !== 'custom' ) {
			$_sizes = explode( 'x', $size );
			$width  = $_sizes[0];
			$height = $_sizes[1];
		}

		if ( $size !== 'full' && $width !== '' && $height !== '' ) {
			$image = Businextcoin_Helper::get_lazy_load_image( array(
				'url'    => $url,
				'alt'    => $alt,
				'width'  => $width,
				'height' => $height,
				'crop'   => true,
				'echo'   => false,

			) );
		} else {
			$image = Businextcoin_Helper::get_lazy_load_image( array(
				'url'       => $url,
				'alt'       => $alt,
				'echo'      => false,
				'full_size' => true,
			) );
		}

		if ( $echo ) {
			echo '' . $image;
		} else {
			return $image;
		}
	}

	public static function get_animation_list( $args = array() ) {
		return array(
			'none'             => esc_html__( 'None', 'businextcoin' ),
			'fade-in'          => esc_html__( 'Fade In', 'businextcoin' ),
			'move-up'          => esc_html__( 'Move Up', 'businextcoin' ),
			'move-down'        => esc_html__( 'Move Down', 'businextcoin' ),
			'move-left'        => esc_html__( 'Move Left', 'businextcoin' ),
			'move-right'       => esc_html__( 'Move Right', 'businextcoin' ),
			'scale-up'         => esc_html__( 'Scale Up', 'businextcoin' ),
			'fall-perspective' => esc_html__( 'Fall Perspective', 'businextcoin' ),
			'fly'              => esc_html__( 'Fly', 'businextcoin' ),
			'flip'             => esc_html__( 'Flip', 'businextcoin' ),
			'helix'            => esc_html__( 'Helix', 'businextcoin' ),
			'pop-up'           => esc_html__( 'Pop Up', 'businextcoin' ),
		);
	}

	public static function get_animation_classes( $animation = 'move-up' ) {
		$classes = '';
		if ( $animation === '' ) {
			$animation = 'move-up';
		}

		if ( $animation !== 'none' ) {
			$classes .= " tm-animation $animation";
		}

		return $classes;
	}

	public static function get_grid_animation_classes( $animation = 'move-up' ) {
		$classes = '';
		if ( $animation === '' ) {
			$animation = 'move-up';
		}

		if ( $animation !== 'none' ) {
			$classes .= " has-animation $animation";
		}

		return $classes;
	}

	public static function get_css_prefix( $property, $value ) {
		$css = '';
		switch ( $property ) {
			case 'border-radius' :
				$css = "-moz-border-radius: {$value};-webkit-border-radius: {$value};border-radius: {$value};";
				break;

			case 'box-shadow' :
				$css = "-moz-box-shadow: {$value};-webkit-box-shadow: {$value};box-shadow: {$value};";
				break;

			case 'order' :
				$css = "-webkit-order: $value; -moz-order: $value; order: $value;";
				break;
		}

		return $css;
	}

	public static function get_shortcode_color_inherit( $color = '', $custom = '' ) {
		$primary_color   = Businextcoin::setting( 'primary_color' );
		$secondary_color = Businextcoin::setting( 'secondary_color' );

		$_color = '#111111';

		switch ( $color ) {
			case 'primary' :
				$_color = $primary_color;
				break;

			case 'secondary' :
				$_color = $secondary_color;
				break;

			case 'custom' :
				$_color = $custom;
				break;

			default:
				$_color = '#111111';
				break;
		}

		return $_color;
	}

	public static function get_shortcode_css_color_inherit( $attr = 'color', $color = '', $custom = '', $gradient = '' ) {
		$primary_color   = Businextcoin::setting( 'primary_color' );
		$secondary_color = Businextcoin::setting( 'secondary_color' );

		$gradient = isset( $gradient ) ? $gradient : '';

		$css = '';
		switch ( $color ) {
			case 'primary' :
				$css = "$attr: {$primary_color};";
				break;

			case 'secondary' :
				$css = "$attr: {$secondary_color};";
				break;

			case 'custom' :
				$css = "$attr: {$custom};";
				break;

			case 'transparent' :
				$css = "$attr: transparent;";
				break;

			case 'gradient' :
				$css = $gradient;

				if ( $attr === 'color' ) {
					$css .= 'color:transparent;-webkit-background-clip: text;background-clip: text;';
				}

				break;
		}

		return $css;
	}

	public static function get_list_hotspot() {
		$businextcoin_post_args = array(
			'post_type'      => 'points_image',
			'posts_per_page' => - 1,
			'orderby'        => 'date',
			'order'          => 'DESC',
			'post_status'    => 'publish',
		);

		$results = array();

		$businextcoin_query = new WP_Query( $businextcoin_post_args );

		if ( $businextcoin_query->have_posts() ) :
			while ( $businextcoin_query->have_posts() ) : $businextcoin_query->the_post();
				$title             = get_the_title();
				$results[ $title ] = get_the_ID();
			endwhile;
		endif;
		wp_reset_postdata();

		return $results;
	}

	public static function get_vc_icon_template( $args = array() ) {

		$defaults = array(
			'type'         => '',
			'icon'         => '',
			'class'        => '',
			'parent_hover' => '',
		);

		$args         = wp_parse_args( $args, $defaults );

		vc_icon_element_fonts_enqueue( $args['type'] );

		switch ( $args['type'] ) {
			case 'linea_svg':
				$icon = str_replace( 'linea-', '', $args['icon'] );
				$icon = str_replace( '-', '_', $icon );
				$_svg = BUSINEXTCOIN_THEME_URI . "/assets/svg/linea/{$icon}.svg";
				?>
				<div class="icon">
					<div class="tm-svg"
					     data-svg="<?php echo esc_url( $_svg ); ?>"
						<?php if ( isset( $args['svg_animate'] ) ): ?>
							data-type="<?php echo esc_attr( $args['svg_animate'] ); ?>"
						<?php endif; ?>
						<?php if ( $args['parent_hover'] !== '' ) : ?>
							data-hover="<?php echo esc_attr( $args['parent_hover'] ); ?>"
						<?php endif; ?>
					></div>
				</div>
				<?php
				break;
			default:
				?>
				<div class="icon">
					<span class="<?php echo esc_attr( $args['icon'] ); ?>"></span>
				</div>
				<?php
				break;
		}
	}

	public static function get_notification_bar_list( $default_option = false ) {

		$results = array(
			'none' => esc_html__( 'Hide', 'businextcoin' ),
			'01'   => esc_html__( 'Notification Bar 01', 'businextcoin' ),
		);

		if ( $default_option === true ) {
			$results = array( '' => esc_html__( 'Default', 'businextcoin' ) ) + $results;
		}

		return $results;
	}

	public static function get_top_bar_list( $default_option = false ) {

		$results = array(
			'none' => esc_html__( 'Hide', 'businextcoin' ),
			'01'   => esc_html__( 'Top Bar 01', 'businextcoin' ),
			'02'   => esc_html__( 'Top Bar 02', 'businextcoin' ),
			'03'   => esc_attr__( 'Top Bar 03', 'businextcoin' ),
			'04'   => esc_attr__( 'Top Bar 04', 'businextcoin' ),
			'05'   => esc_attr__( 'Top Bar 05', 'businextcoin' ),
		);

		if ( $default_option === true ) {
			$results = array( '' => esc_html__( 'Default', 'businextcoin' ) ) + $results;
		}

		return $results;
	}

	public static function get_header_list( $default_option = false ) {

		$headers = array(
			'none' => esc_html__( 'Hide', 'businextcoin' ),
			'01'   => esc_html__( 'Header 01', 'businextcoin' ),
			'02'   => esc_html__( 'Header 02', 'businextcoin' ),
			'03'   => esc_attr__( 'Header 03', 'businextcoin' ),
			'04'   => esc_attr__( 'Header 04', 'businextcoin' ),
			'05'   => esc_attr__( 'Header 05', 'businextcoin' ),
		);

		if ( $default_option === true ) {
			$headers = array( '' => esc_html__( 'Default', 'businextcoin' ) ) + $headers;
		}

		return $headers;
	}

	public static function get_sample_countdown_date() {
		$date = date( 'm/d/Y H:i:s', strtotime( '+1 month', strtotime( date( 'm/d/Y H:i:s' ) ) ) );

		return $date;
	}

	/**
	 * process aspect ratio fields
	 */
	public static function process_chart_aspect_ratio( $ar = '4:3', $width = 500 ) {
		$AR = explode( ':', $ar );

		$rat1 = $AR[0];
		$rat2 = $AR[1];

		$height = ( $width / $rat1 ) * $rat2;

		return $height;
	}

	public static function get_body_font() {
		$font = Businextcoin::setting( 'typography_body' );

		if ( isset( $font['font-family'] ) ) {
			return "{$font['font-family']} Helvetica, Arial, sans-serif";
		}

		return 'Helvetica, Arial, sans-serif';
	}

	/**
	 * Check woocommerce plugin active
	 *
	 * @return boolean true if plugin activated
	 */
	public static function active_woocommerce() {
		if ( class_exists( 'WooCommerce' ) ) {
			return true;
		}

		return false;
	}

	public static function get_button_css_selector() {
		return 'button, input[type="button"], input[type="reset"], input[type="submit"],
				.woocommerce #respond input#submit.disabled,
				.woocommerce #respond input#submit:disabled,
				.woocommerce #respond input#submit:disabled[disabled],
				.woocommerce a.button.disabled,
				.woocommerce a.button:disabled,
				.woocommerce a.button:disabled[disabled],
				.woocommerce button.button.disabled,
				.woocommerce button.button:disabled,
				.woocommerce button.button:disabled[disabled],
				.woocommerce input.button.disabled,
				.woocommerce input.button:disabled,
				.woocommerce input.button:disabled[disabled],
				.woocommerce #respond input#submit,
				.woocommerce a.button,
				.woocommerce button.button,
				.woocommerce input.button,
				.woocommerce a.button.alt,
				.woocommerce input.button.alt,
				.woocommerce button.button.alt,
				.button';
	}

	public static function get_button_hover_css_selector() {
		return 'button:hover,
				input[type="button"]:hover,
				input[type="reset"]:hover,
				input[type="submit"]:hover,
				.woocommerce #respond input#submit.disabled:hover,
				.woocommerce #respond input#submit:disabled:hover,
				.woocommerce #respond input#submit:disabled[disabled]:hover,
				.woocommerce a.button.disabled:hover,
				.woocommerce a.button:disabled:hover,
				.woocommerce a.button:disabled[disabled]:hover,
				.woocommerce button.button.disabled:hover,
				.woocommerce button.button:disabled:hover,
				.woocommerce button.button:disabled[disabled]:hover,
				.woocommerce input.button.disabled:hover,
				.woocommerce input.button:disabled:hover,
				.woocommerce input.button:disabled[disabled]:hover,
				.woocommerce #respond input#submit:hover,
				.woocommerce a.button:hover,
				.woocommerce button.button:hover,
				.woocommerce button.button.alt:hover,
				.woocommerce input.button:hover,
				.woocommerce a.button.alt:hover,
				.woocommerce input.button.alt:hover,
				.button:hover';
	}

	public static function get_form_input_css_selector() {
		return "
			input[type='text'],
			input[type='email'],
			input[type='url'],
			input[type='password'],
			input[type='search'],
			input[type='number'],
			input[type='tel'],
			input[type='range'],
			input[type='date'],
			input[type='month'],
			input[type='week'],
			input[type='time'],
			input[type='datetime'],
			input[type='datetime-local'],
			input[type='color'],
			select,
			textarea
		";
	}

	public static function get_form_input_focus_css_selector() {
		return "
			input[type='text']:focus,
			input[type='email']:focus,
			input[type='url']:focus,
			input[type='password']:focus,
			input[type='search']:focus,
			input[type='number']:focus,
			input[type='tel']:focus,
			input[type='range']:focus,
			input[type='date']:focus,
			input[type='month']:focus,
			input[type='week']:focus,
			input[type='time']:focus,
			input[type='datetime']:focus,
			input[type='datetime-local']:focus,
			input[type='color']:focus,
			textarea:focus, select:focus,
			select:focus,
			textarea:focus
		";
	}
}
