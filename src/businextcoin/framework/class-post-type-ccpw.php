<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Businextcoin_CCPW' ) ) {
	class Businextcoin_CCPW {

		public function __construct() {

		}

		public static function get_coin_ticker_bar_list( $default_option = false ) {

			$results = array(
				'none' => esc_html__( 'Hide', 'businextcoin' ),
				'01'   => esc_html__( 'Coin Ticker Bar 01', 'businextcoin' ),
			);

			if ( $default_option === true ) {
				$results = array( '' => esc_html__( 'Default', 'businextcoin' ) ) + $results;
			}

			return $results;
		}

		public static function get_list_crypto_currency_price_shortcode( $default = false ) {
			$args = array(
				'post_type'      => 'ccpw',
				'posts_per_page' => - 1,
				'post_status'    => 'publish',
				'meta_query'     => array(
					'relation' => 'OR',
				),
			);

			$query   = new WP_Query( $args );
			$results = array(
				'' => esc_html__( 'Select a crypto currency widget', 'businextcoin' ),
			);

			if ( $default === true ) {
				$results['default'] = esc_html__( 'Default', 'businextcoin' );
			}

			if ( $query->have_posts() ) {
				while ( $query->have_posts() ) {
					$query->the_post();
					global $post;
					$slug             = $post->post_name;
					$results[ $slug ] = get_the_title();
				}
			}

			wp_reset_postdata();

			return $results;
		}

		public static function get_crypto_currency_shortcode() {
			$widget = Businextcoin::setting( 'crypto_currency_widget' );

			if ( $widget === '' ) {
				return;
			}

			$_businextcoin_args = array(
				'post_type'      => 'ccpw',
				'posts_per_page' => 1,
				'name'           => $widget,
				'post_status'    => 'publish',
			);

			$_businextcoin_query = new WP_Query( $_businextcoin_args );

			if ( $_businextcoin_query->have_posts() ) {
				while ( $_businextcoin_query->have_posts() ) : $_businextcoin_query->the_post();
					echo do_shortcode( '[ccpw id="' . get_the_ID() . '"]' );
				endwhile;
			}
			wp_reset_postdata();
		}
	}

	new Businextcoin_CCPW();
}
