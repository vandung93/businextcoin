<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Enqueue scripts and styles.
 */
if ( ! class_exists( 'Businextcoin_Enqueue' ) ) {
	class Businextcoin_Enqueue {

		protected static $instance = null;

		public function __construct() {
			// Remove WordPress version from any enqueued scripts.
			add_filter( 'style_loader_src', array( $this, 'at_remove_wp_ver_css_js' ), 9999 );
			add_filter( 'script_loader_src', array( $this, 'at_remove_wp_ver_css_js' ), 9999 );

			add_filter( 'stylesheet_uri', array( $this, 'use_minify_stylesheet' ), 10, 2 );

			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue' ) );

			add_filter( 'wpcf7_load_js', '__return_false' );
			add_filter( 'wpcf7_load_css', '__return_false' );

			add_action( 'init', array( $this, 'remove_hint_from_swatches' ), 99 );

			add_action( 'wp_enqueue_scripts', array( $this, 'dequeue_woocommerce_styles_scripts' ), 99 );
			add_action( 'wp_enqueue_scripts', array( $this, 'dequeue_woo_smart_wishlist_scripts' ), 99 );
		}

		/**
		 * @param $src
		 *
		 * @return mixed|string
		 */
		public function at_remove_wp_ver_css_js( $src ) {
			$override = apply_filters( 'pre_at_remove_wp_ver_css_js', false, $src );
			if ( $override !== false ) {
				return $override;
			}

			if ( strpos( $src, 'ver=' ) ) {
				$src = remove_query_arg( 'ver', $src );
			}

			return $src;
		}

		function use_minify_stylesheet( $stylesheet, $stylesheet_dir ) {
			if ( file_exists( get_template_directory_uri() . '/style.min.css' ) ) {
				$stylesheet = get_template_directory_uri() . '/style.min.css';
			}

			return $stylesheet;
		}

		function dequeue_woocommerce_styles_scripts() {
			if ( function_exists( 'is_woocommerce' ) ) {
				if ( ! is_woocommerce() && ! is_cart() && ! is_checkout() ) {
					# Styles
					/*wp_dequeue_style( 'woocommerce-general' );
					wp_dequeue_style( 'woocommerce-layout' );
					wp_dequeue_style( 'woocommerce-smallscreen' );
					wp_dequeue_style( 'woocommerce_frontend_styles' );
					wp_dequeue_style( 'woocommerce_fancybox_styles' );
					wp_dequeue_style( 'woocommerce_chosen_styles' );
					wp_dequeue_style( 'woocommerce_prettyPhoto_css' );*/
					# Scripts
					/*wp_dequeue_script( 'wc_price_slider' );
					wp_dequeue_script( 'wc-single-product' );
					wp_dequeue_script( 'wc-add-to-cart' );
					wp_dequeue_script( 'wc-cart-fragments' );
					wp_dequeue_script( 'wc-checkout' );
					wp_dequeue_script( 'wc-add-to-cart-variation' );
					wp_dequeue_script( 'wc-single-product' );
					wp_dequeue_script( 'wc-cart' );
					wp_dequeue_script( 'wc-chosen' );
					wp_dequeue_script( 'woocommerce' );
					wp_dequeue_script( 'prettyPhoto' );
					wp_dequeue_script( 'prettyPhoto-init' );
					wp_dequeue_script( 'jquery-blockui' );
					wp_dequeue_script( 'jquery-placeholder' );
					wp_dequeue_script( 'fancybox' );
					wp_dequeue_script( 'jqueryui' );*/

					// Scripts + Styles from Woo Smart Compare
					wp_dequeue_script( 'wooscp-frontend' );
					wp_dequeue_script( 'dragarrange' );
					wp_dequeue_script( 'tableHeadFixer' );
				}
			}
		}

		function dequeue_woo_smart_wishlist_scripts() {
			if ( ! class_exists( 'WPcleverWoosw' ) ) {
				return;
			}

			// Dequeue feather font
			wp_dequeue_style( 'feather' );
			wp_dequeue_script( 'woosw-frontend' );
		}

		function enqueue_woocommerce_styles_scripts() {
			wp_enqueue_script( 'wooscp-frontend' );
			wp_enqueue_script( 'dragarrange' );
			wp_enqueue_script( 'tableHeadFixer' );

			wp_enqueue_script( 'woosw-frontend' );
		}

		public static function instance() {
			if ( null === self::$instance ) {
				self::$instance = new self();
			}

			return self::$instance;
		}

		public function remove_hint_from_swatches() {
			add_action( 'wp_enqueue_scripts', array(
				$this,
				'remove_hint',
			) );

		}

		public function remove_hint() {
			wp_dequeue_style( 'hint' );
		}

		/**
		 * Enqueue scripts & styles.
		 *
		 * @access public
		 */
		public function enqueue() {
			$post_type = get_post_type();
			$min       = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG == true ? '' : '.min';

			global $wp_scripts;
			$jquery_version = isset( $wp_scripts->registered['jquery-ui-core']->ver ) ? $wp_scripts->registered['jquery-ui-core']->ver : '1.11.4';
			wp_register_style( 'jquery-ui-style', '//code.jquery.com/ui/' . $jquery_version . '/themes/smoothness/jquery-ui.min.css', array(), $jquery_version );

			// Remove prettyPhoto, default light box of woocommerce.
			wp_dequeue_script( 'prettyPhoto' );
			wp_dequeue_script( 'prettyPhoto-init' );
			wp_dequeue_style( 'woocommerce_prettyPhoto_css' );

			// Prevent enqueue ihotspot on all pages then only enqueue when use.
			wp_dequeue_script( 'ihotspot-js' );
			wp_dequeue_style( 'ihotspot' );

			// Remove font awesome from Yith Wishlist plugin.
			//wp_dequeue_style( 'yith-wcwl-font-awesome' );

			wp_register_style( 'font-ion', BUSINEXTCOIN_THEME_URI . '/assets/fonts/ion/font-ion.min.css', null, null );

			wp_register_style( 'justifiedGallery', BUSINEXTCOIN_THEME_URI . '/assets/libs/justifiedGallery/justifiedGallery.min.css', null, '3.6.3' );
			wp_register_script( 'justifiedGallery', BUSINEXTCOIN_THEME_URI . '/assets/libs/justifiedGallery/jquery.justifiedGallery.min.js', array( 'jquery' ), '3.6.3', true );

			wp_register_style( 'lightgallery', BUSINEXTCOIN_THEME_URI . '/assets/libs/lightGallery/css/lightgallery.min.css', null, '1.6.4' );
			wp_register_script( 'lightgallery', BUSINEXTCOIN_THEME_URI . "/assets/libs/lightGallery/js/lightgallery-all{$min}.js", array(
				'jquery',
				'picturefill',
				'mousewheel',
			), null, true );

			wp_register_style( 'swiper', BUSINEXTCOIN_THEME_URI . '/assets/libs/swiper/css/swiper.min.css', null, '4.0.3' );
			wp_register_script( 'swiper', BUSINEXTCOIN_THEME_URI . "/assets/libs/swiper/js/swiper{$min}.js", array( 'jquery' ), '4.0.3', true );

			wp_register_style( 'magnific-popup', BUSINEXTCOIN_THEME_URI . '/assets/libs/magnific-popup/magnific-popup.css' );
			wp_register_script( 'magnific-popup', BUSINEXTCOIN_THEME_URI . '/assets/libs/magnific-popup/jquery.magnific-popup.js', array( 'jquery' ), BUSINEXTCOIN_THEME_VERSION, true );

			wp_register_style( 'growl', BUSINEXTCOIN_THEME_URI . '/assets/libs/growl/css/jquery.growl.min.css', null, '1.3.3' );
			wp_register_script( 'growl', BUSINEXTCOIN_THEME_URI . "/assets/libs/growl/js/jquery.growl{$min}.js", array( 'jquery' ), '1.3.3', true );

			wp_register_script( 'time-circle', BUSINEXTCOIN_THEME_URI . "/assets/libs/time-circle/TimeCircles{$min}.js", array( 'jquery' ), '1.3.3', true );

			wp_register_style( 'flipclock', BUSINEXTCOIN_THEME_URI . '/assets/libs/flipclock/flipclock.css', null, null );
			wp_register_script( 'flipclock', BUSINEXTCOIN_THEME_URI . "/assets/libs/flipclock/flipclock{$min}.js", array( 'jquery' ), null, true );

			/*
			 * Begin Register scripts to be enqueued later using the wp_enqueue_script() function.
			 */

			wp_register_script( 'slimscroll', BUSINEXTCOIN_THEME_URI . '/assets/libs/slimscroll/jquery.slimscroll.min.js', array( 'jquery' ), '1.3.8', true );
			wp_register_script( 'matchheight', BUSINEXTCOIN_THEME_URI . '/assets/libs/matchHeight/jquery.matchHeight-min.js', array( 'jquery' ), BUSINEXTCOIN_THEME_VERSION, true );
			wp_register_script( 'gmap3', BUSINEXTCOIN_THEME_URI . '/assets/libs/gmap3/gmap3.min.js', array( 'jquery' ), BUSINEXTCOIN_THEME_VERSION, true );
			wp_register_script( 'countdown', BUSINEXTCOIN_THEME_URI . '/assets/libs/jquery.countdown/js/jquery.countdown.min.js', array( 'jquery' ), BUSINEXTCOIN_THEME_VERSION, true );
			wp_register_script( 'typed', BUSINEXTCOIN_THEME_URI . '/assets/libs/typed/typed.min.js', array( 'jquery' ), null, true );

			// Fix Wordpress old version not registered this script.
			if ( ! wp_script_is( 'imagesloaded', 'registered' ) ) {
				wp_register_script( 'imagesloaded', BUSINEXTCOIN_THEME_URI . '/assets/libs/imagesloaded/imagesloaded.min.js', array( 'jquery' ), null, true );
			}

			wp_register_script( 'isotope-masonry', BUSINEXTCOIN_THEME_URI . '/assets/libs/isotope/js/isotope.pkgd.min.js', array( 'jquery' ), BUSINEXTCOIN_THEME_VERSION, true );
			wp_register_script( 'isotope-packery', BUSINEXTCOIN_THEME_URI . '/assets/libs/packery-mode/packery-mode.pkgd.min.js', array(
				'jquery',
				'imagesloaded',
				'isotope-masonry',
			), BUSINEXTCOIN_THEME_VERSION, true );

			wp_register_script( 'sticky-kit', BUSINEXTCOIN_THEME_URI . '/assets/js/jquery.sticky-kit.min.js', array(
				'jquery',
				'businextcoin-script',
			), BUSINEXTCOIN_THEME_VERSION, true );

			wp_register_script( 'smooth-scroll', BUSINEXTCOIN_THEME_URI . '/assets/libs/smooth-scroll-for-web/SmoothScroll.min.js', array(
				'jquery',
			), '1.4.6', true );

			wp_register_script( 'picturefill', BUSINEXTCOIN_THEME_URI . '/assets/libs/picturefill/picturefill.min.js', array( 'jquery' ), null, true );

			wp_register_script( 'mousewheel', BUSINEXTCOIN_THEME_URI . "/assets/libs/mousewheel/jquery.mousewheel{$min}.js", array( 'jquery' ), BUSINEXTCOIN_THEME_VERSION, true );

			wp_register_script( 'lazyload', BUSINEXTCOIN_THEME_URI . "/assets/libs/lazyload/lazyload{$min}.js", array(), BUSINEXTCOIN_THEME_VERSION, true );

			wp_register_script( 'tween-max', BUSINEXTCOIN_THEME_URI . '/assets/libs/tween-max/TweenMax.min.js', array(
				'jquery',
			), BUSINEXTCOIN_THEME_VERSION, true );

			wp_register_script( 'firefly', BUSINEXTCOIN_THEME_URI . "/assets/js/firefly{$min}.js", array(
				'jquery',
			), BUSINEXTCOIN_THEME_VERSION, true );

			wp_register_script( 'wavify', BUSINEXTCOIN_THEME_URI . "/assets/js/wavify{$min}.js", array(
				'jquery',
				'tween-max',
			), BUSINEXTCOIN_THEME_VERSION, true );

			wp_register_script( 'constellation', BUSINEXTCOIN_THEME_URI . "/assets/js/constellation{$min}.js", array(
				'jquery',
			), BUSINEXTCOIN_THEME_VERSION, true );

			wp_register_script( 'odometer', BUSINEXTCOIN_THEME_URI . '/assets/libs/odometer/odometer.min.js', array(
				'jquery',
			), BUSINEXTCOIN_THEME_VERSION, true );

			wp_register_script( 'counter-up', BUSINEXTCOIN_THEME_URI . '/assets/libs/counterup/jquery.counterup.min.js', array(
				'jquery',
			), BUSINEXTCOIN_THEME_VERSION, true );

			wp_register_script( 'counter', BUSINEXTCOIN_THEME_URI . "/assets/js/counter{$min}.js", array(
				'jquery',
			), BUSINEXTCOIN_THEME_VERSION, true );

			wp_register_script( 'chart-js', BUSINEXTCOIN_THEME_URI . '/assets/libs/chart/Chart.min.js', array(
				'jquery',
			), BUSINEXTCOIN_THEME_VERSION, true );

			wp_register_script( 'advanced-chart', BUSINEXTCOIN_THEME_URI . "/assets/js/advanced-chart{$min}.js", array(
				'jquery',
				'waypoints',
				'chart-js',
			), BUSINEXTCOIN_THEME_VERSION, true );

			wp_register_script( 'circle-progress', BUSINEXTCOIN_THEME_URI . '/assets/libs/circle-progress/circle-progress.min.js', array( 'jquery' ), null, true );
			wp_register_script( 'circle-progress-chart', BUSINEXTCOIN_THEME_URI . "/assets/js/circle-progress-chart{$min}.js", array(
				'jquery',
				'waypoints',
				'circle-progress',
			), null, true );

			wp_register_script( 'businextcoin-pricing', BUSINEXTCOIN_THEME_URI . "/assets/js/pricing{$min}.js", array(
				'jquery',
				'matchheight',
			), null, true );

			wp_register_script( 'businextcoin-accordion', BUSINEXTCOIN_THEME_URI . "/assets/js/accordion{$min}.js", array( 'jquery' ), BUSINEXTCOIN_THEME_VERSION, true );

			wp_register_script( 'businextcoin-countdown', BUSINEXTCOIN_THEME_URI . "/assets/js/countdown{$min}.js", array( 'jquery' ), BUSINEXTCOIN_THEME_VERSION, true );

			wp_register_script( 'loan-form', BUSINEXTCOIN_THEME_URI . "/assets/js/loan-form{$min}.js", array( 'jquery' ), BUSINEXTCOIN_THEME_VERSION, true );

			/*
			 * Enqueue the theme's style.css.
			 * This is recommended because we can add inline styles there
			 * and some plugins use it to do exactly that.
			 */
			wp_enqueue_style( 'businextcoin-style', get_stylesheet_uri() );
			wp_enqueue_style( 'font-ion' );
			wp_enqueue_style( 'swiper' );

			/*
			 * End register scripts
			 */

			if ( Businextcoin::setting( 'header_sticky_enable' ) ) {
				wp_enqueue_script( 'headroom', BUSINEXTCOIN_THEME_URI . "/assets/js/headroom{$min}.js", array( 'jquery' ), BUSINEXTCOIN_THEME_VERSION, true );
			}

			if ( Businextcoin::setting( 'smooth_scroll_enable' ) ) {
				wp_enqueue_script( 'smooth-scroll' );
			}

			wp_enqueue_script( 'jquery-smooth-scroll', BUSINEXTCOIN_THEME_URI . '/assets/libs/smooth-scroll/jquery.smooth-scroll.min.js', array( 'jquery' ), BUSINEXTCOIN_THEME_VERSION, true );
			wp_enqueue_script( 'swiper' );
			wp_enqueue_script( 'waypoints', BUSINEXTCOIN_THEME_URI . '/assets/libs/waypoints/jquery.waypoints.min.js', array( 'jquery' ), BUSINEXTCOIN_THEME_VERSION, true );
			wp_enqueue_script( 'smartmenus', BUSINEXTCOIN_THEME_URI . "/assets/libs/smartmenus/jquery.smartmenus{$min}.js", array( 'jquery' ), BUSINEXTCOIN_THEME_VERSION, true );
			//wp_enqueue_script( 'slimscroll' );

			wp_enqueue_style( 'perfect-scrollbar', BUSINEXTCOIN_THEME_URI . '/assets/libs/perfect-scrollbar/css/perfect-scrollbar.min.css' );
			wp_enqueue_style( 'perfect-scrollbar-woosw', BUSINEXTCOIN_THEME_URI . '/assets/libs/perfect-scrollbar/css/custom-theme.css' );
			wp_enqueue_script( 'perfect-scrollbar', BUSINEXTCOIN_THEME_URI . '/assets/libs/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js', array( 'jquery' ), BUSINEXTCOIN_THEME_VERSION, true );

			if ( Businextcoin::setting( 'notice_cookie_enable' ) && ! isset( $_COOKIE['notice_cookie_confirm'] ) ) {
				wp_enqueue_script( 'growl' );
				wp_enqueue_style( 'growl' );
			}

			if ( Businextcoin::setting( 'lazy_load_images' ) ) {
				wp_enqueue_script( 'lazyload' );
			}

			//  Enqueue styles & scripts for single portfolio pages.
			if ( is_singular() ) {

				switch ( $post_type ) {
					case 'portfolio':
						$single_portfolio_sticky = Businextcoin::setting( 'single_portfolio_sticky_detail_enable' );
						if ( $single_portfolio_sticky == '1' ) {
							wp_enqueue_script( 'sticky-kit' );
						}

						wp_enqueue_style( 'lightgallery' );
						wp_enqueue_script( 'lightgallery' );
						break;

					case 'product':
						wp_enqueue_style( 'lightgallery' );
						wp_enqueue_script( 'lightgallery' );
						break;
				}
			}

			/*
			 * The comment-reply script.
			 */
			if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
				if ( $post_type === 'post' ) {
					if ( Businextcoin::setting( 'single_post_comment_enable' ) === '1' ) {
						wp_enqueue_script( 'comment-reply' );
					}
				} elseif ( $post_type === 'portfolio' ) {
					if ( Businextcoin::setting( 'single_portfolio_comment_enable' ) === '1' ) {
						wp_enqueue_script( 'comment-reply' );
					}
				} elseif ( $post_type === 'case_study' ) {
					if ( Businextcoin::setting( 'single_case_study_comment_enable' ) === '1' ) {
						wp_enqueue_script( 'comment-reply' );
					}
				} elseif ( $post_type === 'service' ) {
					if ( Businextcoin::setting( 'single_service_comment_enable' ) === '1' ) {
						wp_enqueue_script( 'comment-reply' );
					}
				} else {
					wp_enqueue_script( 'comment-reply' );
				}
			}

			$maintenance_templates = Businextcoin_Maintenance::get_maintenance_templates_dir();

			if ( is_page_template( $maintenance_templates ) || is_404() ) {
				wp_enqueue_script( 'time-circle' );
				wp_enqueue_script( 'wavify' );
				wp_enqueue_script( 'businextcoin-maintenance', BUSINEXTCOIN_THEME_URI . "/assets/js/maintenance{$min}.js", array( 'jquery' ), BUSINEXTCOIN_THEME_VERSION, true );
			}

			wp_enqueue_script( 'wpb_composer_front_js' );

			/*
			 * Enqueue main JS
			 */
			wp_enqueue_script( 'businextcoin-script', BUSINEXTCOIN_THEME_URI . "/assets/js/main{$min}.js", array(
				'jquery',
			), BUSINEXTCOIN_THEME_VERSION, true );

			if ( Businextcoin_Helper::active_woocommerce() ) {
				if ( Businextcoin::setting( 'shop_archive_quick_view' ) === '1' ) {
					if ( is_shop() || is_cart() || is_product() || ( function_exists( 'is_product_taxonomy' ) && is_product_taxonomy() ) ) {
						wp_enqueue_style( 'magnific-popup' );
						wp_enqueue_script( 'magnific-popup' );
					}
				}

				wp_enqueue_script( 'businextcoin-woo', BUSINEXTCOIN_THEME_URI . "/assets/js/woo{$min}.js", array(
					'businextcoin-script',
				), BUSINEXTCOIN_THEME_VERSION, true );
			}

			/*
			 * Enqueue custom variable JS
			 */

			$js_variables = array(
				'templateUrl'               => BUSINEXTCOIN_THEME_URI,
				'ajaxurl'                   => admin_url( 'admin-ajax.php' ),
				'primary_color'             => Businextcoin::setting( 'primary_color' ),
				'header_sticky_enable'      => Businextcoin::setting( 'header_sticky_enable' ),
				'header_sticky_height'      => Businextcoin::setting( 'header_sticky_height' ),
				'scroll_top_enable'         => Businextcoin::setting( 'scroll_top_enable' ),
				'lazyLoadImages'            => Businextcoin::setting( 'lazy_load_images' ),
				'light_gallery_auto_play'   => Businextcoin::setting( 'light_gallery_auto_play' ),
				'light_gallery_download'    => Businextcoin::setting( 'light_gallery_download' ),
				'light_gallery_full_screen' => Businextcoin::setting( 'light_gallery_full_screen' ),
				'light_gallery_zoom'        => Businextcoin::setting( 'light_gallery_zoom' ),
				'light_gallery_thumbnail'   => Businextcoin::setting( 'light_gallery_thumbnail' ),
				'light_gallery_share'       => Businextcoin::setting( 'light_gallery_share' ),
				'mobile_menu_breakpoint'    => Businextcoin::setting( 'mobile_menu_breakpoint' ),
				'isSingleProduct'           => is_singular( 'product' ),
				'noticeCookieEnable'        => Businextcoin::setting( 'notice_cookie_enable' ),
				'noticeCookieConfirm'       => isset( $_COOKIE['notice_cookie_confirm'] ) ? 'yes' : 'no',
				'noticeCookieMessages'      => wp_kses( __( 'We use cookies to ensure that we give you the best experience on our website. If you continue to use this site we will assume that you are happy with it. <a id="tm-button-cookie-notice-ok" class="tm-button tm-button-xs tm-button-full-wide tm-button-primary style-flat">OK, GOT IT</a>', 'businextcoin' ), array(
					'a' => array(
						'id'    => array(),
						'class' => array(),
					),
				) ),
				'noticeCookieOKMessages'    => esc_html__( 'Thank you! Hope you have the best experience on our website.', 'businextcoin' ),
				'like'                      => esc_html__( 'Like', 'businextcoin' ),
				'unlike'                    => esc_html__( 'Unlike', 'businextcoin' ),
			);
			wp_localize_script( 'businextcoin-script', '$insight', $js_variables );

			/**
			 * Custom JS
			 */
			if ( Businextcoin::setting( 'custom_js_enable' ) == 1 ) {
				wp_add_inline_script( 'businextcoin-script', html_entity_decode( Businextcoin::setting( 'custom_js' ) ) );
			}

			/**
			 * Custom CSS
			 */
			if ( Businextcoin::setting( 'custom_css_enable' ) ) {
				wp_add_inline_style( 'businextcoin-style', html_entity_decode( Businextcoin::setting( 'custom_css' ), ENT_QUOTES ) );
			}
		}
	}

	new Businextcoin_Enqueue();
}
