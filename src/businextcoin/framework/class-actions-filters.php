<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Custom filters that act independently of the theme templates
 */
if ( ! class_exists( 'Businextcoin_Actions_Filters' ) ) {
	class Businextcoin_Actions_Filters {

		protected static $instance = null;

		public function __construct() {
			add_filter( 'wp_kses_allowed_html', array( $this, 'wp_kses_allowed_html' ), 2, 99 );

			/* Move post count inside the link */
			add_filter( 'wp_list_categories', array( $this, 'move_post_count_inside_link_category' ) );
			/* Move post count inside the link */
			add_filter( 'get_archives_link', array( $this, 'move_post_count_inside_link_archive' ) );

			add_filter( 'comment_form_fields', array( $this, 'move_comment_field_to_bottom' ) );

			add_filter( 'embed_oembed_html', array( $this, 'add_wrapper_for_video' ), 10, 3 );
			add_filter( 'video_embed_html', array( $this, 'add_wrapper_for_video' ) ); // Jetpack.

			add_filter( 'excerpt_length', array(
				$this,
				'custom_excerpt_length',
			), 999 ); // Change excerpt length is set to 55 words by default.

			// Adds custom classes to the array of body classes.
			add_filter( 'body_class', array( $this, 'body_classes' ) );

			// Adds custom attributes to body tag.
			add_filter( 'businextcoin_body_attributes', array( $this, 'add_attributes_to_body' ) );

			if ( ! is_admin() ) {
				add_action( 'pre_get_posts', array( $this, 'alter_search_loop' ), 1 );
				add_filter( 'pre_get_posts', array( $this, 'search_filter' ) );
				add_filter( 'pre_get_posts', array( $this, 'empty_search_filter' ) );
			}

			// Add inline style for shortcode.
			add_action( 'wp_footer', array( $this, 'shortcode_style' ) );

			add_filter( 'insightcore_bmw_nav_args', array( $this, 'add_extra_params_to_insightcore_bmw' ) );
		}

		public static function instance() {
			if ( null === self::$instance ) {
				self::$instance = new self();
			}

			return self::$instance;
		}

		public function wp_kses_allowed_html( $allowedtags, $context ) {
			switch ( $context ) {
				case 'businextcoin-default' :
					$allowedtags = array(
						'a'      => array(
							'id'     => array(),
							'class'  => array(),
							'style'  => array(),
							'href'   => array(),
							'target' => array(),
							'rel'    => array(),
							'title'  => array(),
						),
						'img'    => array(
							'id'     => array(),
							'class'  => array(),
							'style'  => array(),
							'src'    => array(),
							'width'  => array(),
							'height' => array(),
							'alt'    => array(),
							'srcset' => array(),
							'sizes'  => array(),
						),
						'div'    => array(
							'id'    => array(),
							'class' => array(),
							'style' => array(),
						),
						'strong' => array(
							'id'    => array(),
							'class' => array(),
							'style' => array(),
						),
						'b'      => array(
							'id'    => array(),
							'class' => array(),
							'style' => array(),
						),
						'span'   => array(
							'id'    => array(),
							'class' => array(),
							'style' => array(),
						),
						'i'      => array(
							'id'    => array(),
							'class' => array(),
							'style' => array(),
						),
						'del'    => array(
							'id'    => array(),
							'class' => array(),
							'style' => array(),
						),
						'ins'    => array(
							'id'    => array(),
							'class' => array(),
							'style' => array(),
						),
						'br'     => array(),
						'ul'     => array(
							'id'    => array(),
							'class' => array(),
							'style' => array(),
							'type'  => array(),
						),
						'ol'     => array(
							'id'    => array(),
							'class' => array(),
							'style' => array(),
							'type'  => array(),
						),
						'li'     => array(
							'id'    => array(),
							'class' => array(),
							'style' => array(),
						),
					);
					break;
			}

			return $allowedtags;
		}

		function add_extra_params_to_insightcore_bmw( $args ) {
			$args['walker'] = new Businextcoin_Walker_Nav_Menu;

			return $args;
		}

		function move_post_count_inside_link_category( $links ) {
			// First remove span that added by woocommerce.
			$links = str_replace( '<span class="count">', '', $links );
			$links = str_replace( '</span>', '', $links );

			// Then add span again for both blog & shop.

			$links = str_replace( '</a> ', ' <span class="count">', $links );
			$links = str_replace( ')', '</span></a>', $links );
			$links = str_replace( '<span class="count">(', '<span class="count">', $links );

			return $links;
		}

		function move_post_count_inside_link_archive( $links ) {
			$links = str_replace( '</a>&nbsp;(', ' (', $links );
			$links = str_replace( ')', ')</a>', $links );

			return $links;
		}


		function change_widget_tag_cloud_args( $args ) {
			/* set the smallest & largest size in px */
			$args['separator'] = ', ';

			return $args;
		}

		function move_comment_field_to_bottom( $fields ) {
			$comment_field = $fields['comment'];
			unset( $fields['comment'] );
			$fields['comment'] = $comment_field;

			return $fields;
		}

		function shortcode_style() {
			global $businextcoin_shortcode_lg_css;
			global $businextcoin_shortcode_md_css;
			global $businextcoin_shortcode_sm_css;
			global $businextcoin_shortcode_xs_css;
			$css = '';

			if ( $businextcoin_shortcode_lg_css && $businextcoin_shortcode_lg_css !== '' ) {
				$css .= $businextcoin_shortcode_lg_css;
			}

			if ( $businextcoin_shortcode_md_css && $businextcoin_shortcode_md_css !== '' ) {
				$css .= "@media (max-width: 1199px) { $businextcoin_shortcode_md_css }";
			}

			if ( $businextcoin_shortcode_sm_css && $businextcoin_shortcode_sm_css !== '' ) {
				$css .= "@media (max-width: 992px) { $businextcoin_shortcode_sm_css }";
			}

			if ( $businextcoin_shortcode_xs_css && $businextcoin_shortcode_xs_css !== '' ) {
				$css .= "@media (max-width: 767px) { $businextcoin_shortcode_xs_css }";
			}

			if ( $css !== '' ) : ?>
				<script>
                    var mainStyle = document.getElementById( 'businextcoin-style-inline-css' );
                    if ( mainStyle !== null ) {
                        mainStyle.textContent += '<?php echo '' . $css; ?>';
                    }
				</script>
			<?php endif;

			/*if ( $css !== '' ) :
				$css = Businextcoin_Minify::css( $css );
				echo '<style scoped>' . $css . '</style>';
			endif;*/
		}

		public function alter_search_loop( $query ) {
			if ( $query->is_main_query() && $query->is_search() ) {
				$number_results = Businextcoin::setting( 'search_page_number_results' );
				$query->set( 'posts_per_page', $number_results );
			}
		}

		/**
		 * Apply filters to the search query.
		 * Determines if we only want to display posts/pages and changes the query accordingly
		 */
		public function search_filter( $query ) {
			if ( $query->is_main_query() && $query->is_search ) {
				$filter = Businextcoin::setting( 'search_page_filter' );
				if ( $filter !== 'all' ) {
					$query->set( 'post_type', $filter );
				}
			}

			return $query;
		}

		/**
		 * Make wordpress respect the search template on an empty search
		 */
		public function empty_search_filter( $query ) {
			if ( isset( $_GET['s'] ) && empty( $_GET['s'] ) && $query->is_main_query() ) {
				$query->is_search = true;
				$query->is_home   = false;
			}

			return $query;
		}

		public function custom_excerpt_length() {
			return 999;
		}

		/**
		 * Add responsive container to embeds
		 */
		public function add_wrapper_for_video( $html, $url ) {
			$array = array(
				'youtube.com',
				'wordpress.tv',
				'vimeo.com',
				'dailymotion.com',
				'hulu.com',
			);

			if ( Businextcoin_Helper::strposa( $url, $array ) ) {
				$html = '<div class="embed-responsive embed-responsive-16by9">' . $html . '</div>';
			}

			return $html;
		}

		public function add_attributes_to_body( $attrs ) {
			$site_width = Businextcoin_Helper::get_post_meta( 'site_width', '' );
			if ( $site_width === '' ) {
				$site_width = Businextcoin::setting( 'site_width' );
			}
			$attrs['data-site-width']    = $site_width;
			$attrs['data-content-width'] = 1200;

			$font = Businextcoin_Helper::get_body_font();

			$attrs['data-font'] = $font;

			return $attrs;
		}

		/**
		 * Adds custom classes to the array of body classes.
		 *
		 * @param array $classes Classes for the body element.
		 *
		 * @return array
		 */
		public function body_classes( $classes ) {
			// Adds a class of group-blog to blogs with more than 1 published author.
			if ( is_multi_author() ) {
				$classes[] = 'group-blog';
			}

			// Adds a class of hfeed to non-singular pages.
			if ( ! is_singular() ) {
				$classes[] = 'hfeed';
			}

			// Adds a class for mobile device.
			if ( Businextcoin::is_mobile() ) {
				$classes[] = 'mobile';
			}

			// Adds a class for tablet device.
			if ( Businextcoin::is_tablet() ) {
				$classes[] = 'tablet';
			}

			// Adds a class for handheld device.
			if ( Businextcoin::is_handheld() ) {
				$classes[] = 'handheld';
				$classes[] = 'mobile-menu';
			}

			// Adds a class for desktop device.
			if ( Businextcoin::is_desktop() ) {
				$classes[] = 'desktop';
				$classes[] = 'desktop-menu';
			}

			if ( Businextcoin_Helper::active_woocommerce() ) {
				$classes[] = 'woocommerce';
			}

			$css_animation = Businextcoin::setting( 'shortcode_animation_enable' );

			if ( ( $css_animation === 'both' ) || ( $css_animation === 'desktop' && Businextcoin::is_desktop() ) || ( $css_animation === 'mobile' && Businextcoin::is_handheld() ) ) {
				$classes[] = 'page-has-animation';
			}

			$skin      = Businextcoin::setting( 'site_skin' );
			$classes[] = "page-skin-$skin";

			$one_page_enable = Businextcoin_Helper::get_post_meta( 'menu_one_page', '' );
			if ( $one_page_enable === '1' ) {
				$classes[] = 'one-page';
			}

			if ( is_singular( 'portfolio' ) ) {
				$style = Businextcoin_Helper::get_post_meta( 'portfolio_layout_style', '' );
				if ( $style === '' ) {
					$style = Businextcoin::setting( 'single_portfolio_style' );
				}
				$classes[] = "single-portfolio-style-$style";
			}

			$header_position = Businextcoin_Helper::get_post_meta( 'header_position', '' );
			if ( $header_position !== '' ) {
				$classes[] = "page-header-$header_position";
			}

			$header_sticky_behaviour = Businextcoin::setting( 'header_sticky_behaviour' );
			$classes[]               = "header-sticky-$header_sticky_behaviour";

			$site_layout = Businextcoin_Helper::get_post_meta( 'site_layout', '' );
			if ( $site_layout === '' ) {
				$site_layout = Businextcoin::setting( 'site_layout' );
			}
			$classes[] = $site_layout;

			$sidebar_status = Businextcoin_Global::instance()->get_sidebar_status();

			if ( $sidebar_status === 'one' ) {
				$classes[] = 'page-has-sidebar page-one-sidebar';
			} elseif ( $sidebar_status === 'both' ) {
				$classes[] = 'page-has-sidebar page-both-sidebar';
			} else {
				$classes[] = 'page-has-no-sidebar';
			}

			return $classes;
		}
	}

	new Businextcoin_Actions_Filters();
}
