<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Security setups
 */
if ( ! class_exists( 'Businextcoin_Security' ) ) {
	class Businextcoin_Security {

		public function __construct() {
			// Disable support for trackbacks in post types.
			add_action( 'admin_init', array( $this, 'posttype_disable_trackbacks' ) );

			// Prefill forms fields with comment author cookie.
			add_action( 'wp_head', array( $this, 'comment_author_cookie' ) );
		}

		public function posttype_disable_trackbacks() {
			$post_types = get_post_types();
			foreach ( $post_types as $post_type ) {
				remove_post_type_support( $post_type, 'trackbacks' );
			}
		}

		public function comment_author_cookie() {
			echo '<script>';
			if ( isset( $_COOKIE[ 'comment_author_' . COOKIEHASH ] ) ) {
				$commentAuthorName  = $_COOKIE[ 'comment_author_' . COOKIEHASH ];
				$commentAuthorEmail = $_COOKIE[ 'comment_author_email_' . COOKIEHASH ];
				echo 'cookieAuthorName = "' . $commentAuthorName . '";';
				echo 'cookieAuthorEmail = "' . $commentAuthorEmail . '";';
			} else {
				echo 'cookieAuthorName = "";';
				echo 'cookieAuthorEmail = "";';
			}
			echo '</script>';
		}

	}

	new Businextcoin_Security();
}
