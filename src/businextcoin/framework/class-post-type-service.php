<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Businextcoin_Service' ) ) {
	class Businextcoin_Service {

		public function __construct() {
			add_action( 'wp_ajax_service_infinite_load', array( $this, 'infinite_load' ) );
			add_action( 'wp_ajax_nopriv_service_infinite_load', array( $this, 'infinite_load' ) );
		}

		public function infinite_load() {
			$args = array(
				'post_type'      => $_POST['post_type'],
				'posts_per_page' => $_POST['posts_per_page'],
				'orderby'        => $_POST['orderby'],
				'order'          => $_POST['order'],
				'paged'          => $_POST['paged'],
				'post_status'    => 'publish',
			);

			if ( ! empty( $_POST['taxonomies'] ) ) {
				$args = Businextcoin_VC::get_tax_query_of_taxonomies( $args, $_POST['taxonomies'] );
			}

			if ( ! empty( $_POST['extra_taxonomy'] ) ) {
				$args = Businextcoin_VC::get_tax_query_of_taxonomies( $args, $_POST['extra_taxonomy'] );
			}

			$style      = isset( $_POST['style'] ) ? $_POST['style'] : 'grid_classic_01';
			$image_size = isset( $_POST['image_size'] ) ? $_POST['image_size'] : '';

			$businextcoin_query = new WP_Query( $args );
			$count          = $businextcoin_query->post_count;

			$response = array(
				'max_num_pages' => $businextcoin_query->max_num_pages,
				'found_posts'   => $businextcoin_query->found_posts,
				'count'         => $businextcoin_query->post_count,
			);

			ob_start();

			if ( $businextcoin_query->have_posts() ) :

				set_query_var( 'businextcoin_query', $businextcoin_query );
				set_query_var( 'count', $count );
				set_query_var( 'image_size', $image_size );

				get_template_part( 'loop/shortcodes/service/style', $style );

			endif;
			wp_reset_postdata();

			$template = ob_get_contents();
			ob_clean();

			$response['template'] = $template;

			echo json_encode( $response );

			wp_die();
		}
	}

	new Businextcoin_Service();
}
