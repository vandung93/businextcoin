<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Businextcoin_Metabox' ) ) {
	class Businextcoin_Metabox {

		/**
		 * Businextcoin_Metabox constructor.
		 */
		public function __construct() {
			add_filter( 'insight_core_meta_boxes', array( $this, 'register_meta_boxes' ) );
			add_filter( 'businextcoin_page_meta_box_presets', array( $this, 'page_meta_box_presets' ) );
		}

		public function page_meta_box_presets( $presets ) {
			$presets[] = 'color_preset';

			return $presets;
		}

		/**
		 * Register Metabox
		 *
		 * @param $meta_boxes
		 *
		 * @return array
		 */
		public function register_meta_boxes( $meta_boxes ) {
			$page_registered_sidebars = Businextcoin_Helper::get_registered_sidebars( true );

			$general_options = array(
				array(
					'title'  => esc_attr__( 'Layout', 'businextcoin' ),
					'fields' => array(
						array(
							'id'      => 'site_layout',
							'type'    => 'select',
							'title'   => esc_attr__( 'Layout', 'businextcoin' ),
							'desc'    => esc_attr__( 'Controls the layout of this page.', 'businextcoin' ),
							'options' => array(
								''      => esc_html__( 'Default', 'businextcoin' ),
								'boxed' => esc_html__( 'Boxed', 'businextcoin' ),
								'wide'  => esc_html__( 'Wide', 'businextcoin' ),
							),
							'default' => '',
						),
						array(
							'id'    => 'site_width',
							'type'  => 'text',
							'title' => esc_attr__( 'Site Width', 'businextcoin' ),
							'desc'  => esc_attr__( 'Controls the site width for this page. Enter value including any valid CSS unit, ex: 1200px. Leave blank to use global setting.', 'businextcoin' ),
						),
						array(
							'id'      => 'content_padding',
							'type'    => 'switch',
							'title'   => esc_attr__( 'Page Content Padding', 'businextcoin' ),
							'default' => '1',
							'options' => array(
								'0' => esc_html__( 'No Padding', 'businextcoin' ),
								'1' => esc_html__( 'Default', 'businextcoin' ),
							),
						),
					),
				),
				array(
					'title'  => esc_attr__( 'Color', 'businextcoin' ),
					'fields' => array(
						array(
							'id'      => 'color_preset',
							'type'    => 'select',
							'title'   => esc_attr__( 'Color Preset', 'businextcoin' ),
							'desc'    => esc_attr__( 'Select a preset of color for this page.', 'businextcoin' ),
							'options' => array(
								'-1' => esc_html__( 'None', 'businextcoin' ),
								'01' => esc_html__( 'Preset 01', 'businextcoin' ),
								'02' => esc_html__( 'Preset 02', 'businextcoin' ),
								'03' => esc_html__( 'Preset 03', 'businextcoin' ),
								'04' => esc_html__( 'Preset 04', 'businextcoin' ),
								'05' => esc_html__( 'Preset 05', 'businextcoin' ),
								'06' => esc_html__( 'Preset 06', 'businextcoin' ),
							),
							'default' => '-1',
						),
					),
				),
				array(
					'title'  => esc_attr__( 'Background', 'businextcoin' ),
					'fields' => array(
						array(
							'id'      => 'site_background_message',
							'type'    => 'message',
							'title'   => esc_attr__( 'Info', 'businextcoin' ),
							'message' => esc_attr__( 'These options controls the background on boxed mode.', 'businextcoin' ),
						),
						array(
							'id'    => 'site_background_color',
							'type'  => 'color',
							'title' => esc_attr__( 'Background Color', 'businextcoin' ),
							'desc'  => esc_attr__( 'Controls the background color of the outer background area in boxed mode of this page.', 'businextcoin' ),
						),
						array(
							'id'    => 'site_background_image',
							'type'  => 'media',
							'title' => esc_attr__( 'Background Image', 'businextcoin' ),
							'desc'  => esc_attr__( 'Controls the background image of the outer background area in boxed mode of this page.', 'businextcoin' ),
						),
						array(
							'id'      => 'site_background_repeat',
							'type'    => 'select',
							'title'   => esc_attr__( 'Background Repeat', 'businextcoin' ),
							'desc'    => esc_attr__( 'Controls the background repeat of the outer background area in boxed mode of this page.', 'businextcoin' ),
							'options' => array(
								'no-repeat' => esc_html__( 'No repeat', 'businextcoin' ),
								'repeat'    => esc_html__( 'Repeat', 'businextcoin' ),
								'repeat-x'  => esc_html__( 'Repeat X', 'businextcoin' ),
								'repeat-y'  => esc_html__( 'Repeat Y', 'businextcoin' ),
							),
						),
						array(
							'id'      => 'site_background_attachment',
							'type'    => 'select',
							'title'   => esc_attr__( 'Background Attachment', 'businextcoin' ),
							'desc'    => esc_attr__( 'Controls the background attachment of the outer background area in boxed mode of this page.', 'businextcoin' ),
							'options' => array(
								''       => esc_html__( 'Default', 'businextcoin' ),
								'fixed'  => esc_html__( 'Fixed', 'businextcoin' ),
								'scroll' => esc_html__( 'Scroll', 'businextcoin' ),
							),
						),
						array(
							'id'    => 'site_background_position',
							'type'  => 'text',
							'title' => esc_html__( 'Background Position', 'businextcoin' ),
							'desc'  => esc_attr__( 'Controls the background position of the outer background area in boxed mode of this page.', 'businextcoin' ),
						),
						array(
							'id'    => 'site_background_size',
							'type'  => 'text',
							'title' => esc_html__( 'Background Size', 'businextcoin' ),
							'desc'  => esc_attr__( 'Controls the background size of the outer background area in boxed mode of this page.', 'businextcoin' ),
						),
						array(
							'id'      => 'content_background_message',
							'type'    => 'message',
							'title'   => esc_attr__( 'Info', 'businextcoin' ),
							'message' => esc_attr__( 'These options controls the background of main content on this page.', 'businextcoin' ),
						),
						array(
							'id'    => 'content_background_color',
							'type'  => 'color',
							'title' => esc_attr__( 'Background Color', 'businextcoin' ),
							'desc'  => esc_attr__( 'Controls the background color of main content on this page.', 'businextcoin' ),
						),
						array(
							'id'    => 'content_background_image',
							'type'  => 'media',
							'title' => esc_attr__( 'Background Image', 'businextcoin' ),
							'desc'  => esc_attr__( 'Controls the background image of main content on this page.', 'businextcoin' ),
						),
						array(
							'id'      => 'content_background_repeat',
							'type'    => 'select',
							'title'   => esc_attr__( 'Background Repeat', 'businextcoin' ),
							'desc'    => esc_attr__( 'Controls the background repeat of main content on this page.', 'businextcoin' ),
							'options' => array(
								'no-repeat' => esc_html__( 'No repeat', 'businextcoin' ),
								'repeat'    => esc_html__( 'Repeat', 'businextcoin' ),
								'repeat-x'  => esc_html__( 'Repeat X', 'businextcoin' ),
								'repeat-y'  => esc_html__( 'Repeat Y', 'businextcoin' ),
							),
						),
						array(
							'id'    => 'content_background_position',
							'type'  => 'text',
							'title' => esc_html__( 'Background Position', 'businextcoin' ),
							'desc'  => esc_attr__( 'Controls the background position of main content on this page.', 'businextcoin' ),
						),
					),
				),
				array(
					'title'  => esc_attr__( 'Header', 'businextcoin' ),
					'fields' => array(
						array(
							'id'      => 'coin_ticker_bar_type',
							'type'    => 'select',
							'title'   => esc_attr__( 'Coin Ticker Bar Type', 'businextcoin' ),
							'desc'    => esc_attr__( 'Select coin ticker bar type that displays on this page.', 'businextcoin' ),
							'default' => '',
							'options' => Businextcoin_CCPW::get_coin_ticker_bar_list( true ),
						),
						array(
							'id'      => 'notification_bar_type',
							'type'    => 'select',
							'title'   => esc_attr__( 'Notification Bar Type', 'businextcoin' ),
							'desc'    => esc_attr__( 'Select notification bar type that displays on this page.', 'businextcoin' ),
							'default' => '',
							'options' => Businextcoin_Helper::get_notification_bar_list( true ),
						),
						array(
							'id'      => 'top_bar_type',
							'type'    => 'select',
							'title'   => esc_attr__( 'Top Bar Type', 'businextcoin' ),
							'desc'    => esc_attr__( 'Select top bar type that displays on this page.', 'businextcoin' ),
							'default' => '',
							'options' => Businextcoin_Helper::get_top_bar_list( true ),
						),
						array(
							'id'      => 'header_type',
							'type'    => 'select',
							'title'   => esc_attr__( 'Header Type', 'businextcoin' ),
							'desc'    => esc_attr__( 'Select header type that displays on this page.', 'businextcoin' ),
							'default' => '',
							'options' => Businextcoin_Helper::get_header_list( true ),
						),
						array(
							'id'      => 'menu_display',
							'type'    => 'select',
							'title'   => esc_attr__( 'Primary menu', 'businextcoin' ),
							'desc'    => esc_attr__( 'Select which menu displays on this page.', 'businextcoin' ),
							'default' => '',
							'options' => Businextcoin_Helper::get_all_menus(),
						),
						array(
							'id'      => 'menu_one_page',
							'type'    => 'switch',
							'title'   => esc_attr__( 'One Page Menu', 'businextcoin' ),
							'default' => '0',
							'options' => array(
								'0' => esc_html__( 'Disable', 'businextcoin' ),
								'1' => esc_html__( 'Enable', 'businextcoin' ),
							),
						),
						array(
							'id'      => 'custom_logo',
							'type'    => 'media',
							'title'   => esc_attr__( 'Custom Logo', 'businextcoin' ),
							'desc'    => esc_attr__( 'Select custom logo for this page.', 'businextcoin' ),
							'default' => '',
						),
						array(
							'id'      => 'custom_logo_width',
							'type'    => 'text',
							'title'   => esc_attr__( 'Custom Logo Width', 'businextcoin' ),
							'desc'    => esc_attr__( 'Controls the width of logo. For ex: 150px', 'businextcoin' ),
							'default' => '',
						),
						array(
							'id'      => 'custom_sticky_logo_width',
							'type'    => 'text',
							'title'   => esc_attr__( 'Custom Sticky Logo Width', 'businextcoin' ),
							'desc'    => esc_attr__( 'Controls the width of sticky logo. For ex: 150px', 'businextcoin' ),
							'default' => '',
						),
					),
				),
				array(
					'title'  => esc_attr__( 'Page Title Bar', 'businextcoin' ),
					'fields' => array(
						array(
							'id'      => 'page_title_bar_layout',
							'type'    => 'select',
							'title'   => esc_attr__( 'Layout', 'businextcoin' ),
							'default' => 'default',
							'options' => array(
								'default' => esc_html__( 'Default', 'businextcoin' ),
								'none'    => esc_html__( 'Hide', 'businextcoin' ),
								'01'      => esc_html__( 'Style 01', 'businextcoin' ),
								'02'      => esc_html__( 'Style 02', 'businextcoin' ),
								'03'      => esc_html__( 'Style 03', 'businextcoin' ),
							),
						),
						array(
							'id'      => 'page_title_bar_background_color',
							'type'    => 'color',
							'title'   => esc_attr__( 'Background Color', 'businextcoin' ),
							'default' => '',
						),
						array(
							'id'      => 'page_title_bar_background',
							'type'    => 'media',
							'title'   => esc_attr__( 'Background Image', 'businextcoin' ),
							'default' => '',
						),
						array(
							'id'      => 'page_title_bar_background_overlay',
							'type'    => 'color',
							'title'   => esc_attr__( 'Background Overlay', 'businextcoin' ),
							'default' => '',
						),
						array(
							'id'    => 'page_title_bar_custom_heading',
							'type'  => 'text',
							'title' => esc_attr__( 'Custom Heading Text', 'businextcoin' ),
							'desc'  => esc_attr__( 'Insert custom heading for the page title bar. Leave blank to use default.', 'businextcoin' ),
						),
					),
				),
				array(
					'title'  => esc_attr__( 'Sidebars', 'businextcoin' ),
					'fields' => array(
						array(
							'id'      => 'page_sidebar_1',
							'type'    => 'select',
							'title'   => esc_html__( 'Sidebar 1', 'businextcoin' ),
							'desc'    => esc_html__( 'Select sidebar 1 that will display on this page.', 'businextcoin' ),
							'default' => 'default',
							'options' => $page_registered_sidebars,
						),
						array(
							'id'      => 'page_sidebar_2',
							'type'    => 'select',
							'title'   => esc_html__( 'Sidebar 2', 'businextcoin' ),
							'desc'    => esc_html__( 'Select sidebar 2 that will display on this page.', 'businextcoin' ),
							'default' => 'default',
							'options' => $page_registered_sidebars,
						),
						array(
							'id'      => 'page_sidebar_position',
							'type'    => 'switch',
							'title'   => esc_html__( 'Sidebar Position', 'businextcoin' ),
							'default' => 'default',
							'options' => Businextcoin_Helper::get_list_sidebar_positions( true ),
						),
					),
				),
				array(
					'title'  => esc_attr__( 'Sliders', 'businextcoin' ),
					'fields' => array(
						array(
							'id'      => 'revolution_slider',
							'type'    => 'select',
							'title'   => esc_attr__( 'Revolution Slider', 'businextcoin' ),
							'desc'    => esc_attr__( 'Select the unique name of the slider.', 'businextcoin' ),
							'options' => Businextcoin_Helper::get_list_revslider(),
						),
						array(
							'id'      => 'slider_position',
							'type'    => 'select',
							'title'   => esc_attr__( 'Slider Position', 'businextcoin' ),
							'default' => 'below',
							'options' => array(
								'above' => esc_attr__( 'Above Header', 'businextcoin' ),
								'below' => esc_attr__( 'Below Header', 'businextcoin' ),
							),
						),
					),
				),
				array(
					'title'  => esc_attr__( 'Footer', 'businextcoin' ),
					'fields' => array(
						array(
							'id'      => 'footer_page',
							'type'    => 'select',
							'title'   => esc_attr__( 'Footer Page', 'businextcoin' ),
							'default' => 'default',
							'options' => Businextcoin_Footer::get_list_footers( true ),
						),
					),
				),
			);

			$meta_boxes[] = array(
				'id'         => 'insight_page_options',
				'title'      => esc_html__( 'Page Options', 'businextcoin' ),
				'post_types' => array( 'page' ),
				'context'    => 'normal',
				'priority'   => 'high',
				'fields'     => array(
					array(
						'type'  => 'tabpanel',
						'items' => $general_options,
					),
				),
			);

			$meta_boxes[] = array(
				'id'         => 'insight_post_options',
				'title'      => esc_html__( 'Page Options', 'businextcoin' ),
				'post_types' => array( 'post' ),
				'context'    => 'normal',
				'priority'   => 'high',
				'fields'     => array(
					array(
						'type'  => 'tabpanel',
						'items' => array_merge( array(
							array(
								'title'  => esc_attr__( 'Post', 'businextcoin' ),
								'fields' => array(
									array(
										'id'    => 'post_media_dark',
										'type'  => 'media',
										'title' => esc_attr__( 'Post Media Dark', 'businextcoin' ),
									),
									array(
										'id'    => 'post_media_light',
										'type'  => 'media',
										'title' => esc_attr__( 'Post Media Light', 'businextcoin' ),
									),
									array(
										'id'    => 'post_gallery',
										'type'  => 'gallery',
										'title' => esc_attr__( 'Gallery Format', 'businextcoin' ),
									),
									array(
										'id'    => 'post_video',
										'type'  => 'textarea',
										'title' => esc_html__( 'Video Format', 'businextcoin' ),
									),
									array(
										'id'    => 'post_audio',
										'type'  => 'textarea',
										'title' => esc_html__( 'Audio Format', 'businextcoin' ),
									),
									array(
										'id'    => 'post_quote_text',
										'type'  => 'text',
										'title' => esc_html__( 'Quote Format - Source Text', 'businextcoin' ),
									),
									array(
										'id'    => 'post_quote_name',
										'type'  => 'text',
										'title' => esc_html__( 'Quote Format - Source Name', 'businextcoin' ),
									),
									array(
										'id'    => 'post_quote_url',
										'type'  => 'text',
										'title' => esc_html__( 'Quote Format - Source Url', 'businextcoin' ),
									),
									array(
										'id'    => 'post_link',
										'type'  => 'text',
										'title' => esc_html__( 'Link Format', 'businextcoin' ),
									),
								),
							),
						), $general_options ),
					),
				),
			);

			$meta_boxes[] = array(
				'id'         => 'insight_product_options',
				'title'      => esc_html__( 'Page Options', 'businextcoin' ),
				'post_types' => array( 'product' ),
				'context'    => 'normal',
				'priority'   => 'high',
				'fields'     => array(
					array(
						'type'  => 'tabpanel',
						'items' => $general_options,
					),
				),
			);

			$meta_boxes[] = array(
				'id'         => 'insight_portfolio_options',
				'title'      => esc_html__( 'Page Options', 'businextcoin' ),
				'post_types' => array( 'portfolio' ),
				'context'    => 'normal',
				'priority'   => 'high',
				'fields'     => array(
					array(
						'type'  => 'tabpanel',
						'items' => array_merge( array(
							array(
								'title'  => esc_attr__( 'Portfolio', 'businextcoin' ),
								'fields' => array(
									array(
										'id'      => 'portfolio_layout_style',
										'type'    => 'select',
										'title'   => esc_attr__( 'Single Portfolio Style', 'businextcoin' ),
										'desc'    => esc_attr__( 'Select style of this single portfolio post page.', 'businextcoin' ),
										'default' => '',
										'options' => array(
											''             => esc_html__( 'Default', 'businextcoin' ),
											'left_details' => esc_html__( 'Left Details', 'businextcoin' ),
											'flat'         => esc_html__( 'Flat', 'businextcoin' ),
											'slider'       => esc_html__( 'Image Slider', 'businextcoin' ),
											'video'        => esc_html__( 'Video', 'businextcoin' ),
											'fullscreen'   => esc_html__( 'Fullscreen', 'businextcoin' ),
										),
									),
									array(
										'id'    => 'portfolio_gallery',
										'type'  => 'gallery',
										'title' => esc_attr__( 'Gallery', 'businextcoin' ),
									),
									array(
										'id'    => 'portfolio_video_url',
										'type'  => 'textarea',
										'title' => esc_html__( 'Video Url', 'businextcoin' ),
									),
									array(
										'id'    => 'portfolio_client',
										'type'  => 'text',
										'title' => esc_html__( 'Client', 'businextcoin' ),
									),
									array(
										'id'    => 'portfolio_date',
										'type'  => 'text',
										'title' => esc_html__( 'Date', 'businextcoin' ),
									),
									array(
										'id'    => 'portfolio_awards',
										'type'  => 'editor',
										'title' => esc_html__( 'Awards', 'businextcoin' ),
									),
									array(
										'id'    => 'portfolio_team',
										'type'  => 'editor',
										'title' => esc_html__( 'Team', 'businextcoin' ),
									),
									array(
										'id'    => 'portfolio_url',
										'type'  => 'text',
										'title' => esc_html__( 'Url', 'businextcoin' ),
									),
								),
							),
						), $general_options ),
					),
				),
			);

			$meta_boxes[] = array(
				'id'         => 'insight_case_study_options',
				'title'      => esc_html__( 'Page Options', 'businextcoin' ),
				'post_types' => array( 'case_study' ),
				'context'    => 'normal',
				'priority'   => 'high',
				'fields'     => array(
					array(
						'type'  => 'tabpanel',
						'items' => $general_options,
					),
				),
			);

			$meta_boxes[] = array(
				'id'         => 'insight_service_options',
				'title'      => esc_html__( 'Page Options', 'businextcoin' ),
				'post_types' => array( 'service' ),
				'context'    => 'normal',
				'priority'   => 'high',
				'fields'     => array(
					array(
						'type'  => 'tabpanel',
						'items' => array_merge( array(
							array(
								'title'  => esc_attr__( 'Service', 'businextcoin' ),
								'fields' => array(
									array(
										'id'      => 'service_style',
										'type'    => 'select',
										'title'   => esc_attr__( 'Single Service Style', 'businextcoin' ),
										'desc'    => esc_attr__( 'Select style of this single service post page.', 'businextcoin' ),
										'default' => '',
										'options' => array(
											''   => esc_html__( 'Default', 'businextcoin' ),
											'01' => esc_html__( 'Style 01', 'businextcoin' ),
											'02' => esc_html__( 'Style 02', 'businextcoin' ),
										),
									),
									array(
										'id'      => 'service_cost',
										'type'    => 'text',
										'title'   => esc_html__( 'Cost', 'businextcoin' ),
										'desc'    => esc_html__( 'Enter cost for this service.', 'businextcoin' ),
										'default' => '',
									),
								),
							),
						), $general_options ),
					),
				),
			);

			$meta_boxes[] = array(
				'id'         => 'insight_testimonial_options',
				'title'      => esc_html__( 'Testimonial Options', 'businextcoin' ),
				'post_types' => array( 'testimonial' ),
				'context'    => 'normal',
				'priority'   => 'high',
				'fields'     => array(
					array(
						'type'  => 'tabpanel',
						'items' => array(
							array(
								'title'  => esc_html__( 'Testimonial Details', 'businextcoin' ),
								'fields' => array(
									array(
										'id'      => 'by_line',
										'type'    => 'text',
										'title'   => esc_html__( 'By Line', 'businextcoin' ),
										'desc'    => esc_html__( 'Enter a byline for the customer giving this testimonial (for example: "CEO of Thememove").', 'businextcoin' ),
										'default' => '',
									),
									array(
										'id'      => 'url',
										'type'    => 'text',
										'title'   => esc_html__( 'Url', 'businextcoin' ),
										'desc'    => esc_html__( 'Enter a URL that applies to this customer (for example: http://www.thememove.com/).', 'businextcoin' ),
										'default' => '',
									),
									array(
										'id'      => 'rating',
										'type'    => 'select',
										'title'   => esc_attr__( 'Rating', 'businextcoin' ),
										'default' => '',
										'options' => array(
											''  => esc_html__( 'Select a rating', 'businextcoin' ),
											'1' => esc_html__( '1 Star', 'businextcoin' ),
											'2' => esc_html__( '2 Stars', 'businextcoin' ),
											'3' => esc_html__( '3 Stars', 'businextcoin' ),
											'4' => esc_html__( '4 Stars', 'businextcoin' ),
											'5' => esc_html__( '5 Stars', 'businextcoin' ),
										),
									),
								),
							),
						),
					),
				),
			);

			$meta_boxes[] = array(
				'id'         => 'insight_footer_options',
				'title'      => esc_html__( 'Footer Options', 'businextcoin' ),
				'post_types' => array( 'ic_footer' ),
				'context'    => 'normal',
				'priority'   => 'high',
				'fields'     => array(
					array(
						'type'  => 'tabpanel',
						'items' => array(
							array(
								'title'  => esc_html__( 'Effect', 'businextcoin' ),
								'fields' => array(
									array(
										'id'      => 'effect',
										'type'    => 'switch',
										'title'   => esc_attr__( 'Footer Effect', 'businextcoin' ),
										'default' => '',
										'options' => array(
											''         => esc_html__( 'Normal', 'businextcoin' ),
											'parallax' => esc_html__( 'Parallax', 'businextcoin' ),
										),
									),
								),
							),
							array(
								'title'  => esc_html__( 'Styling', 'businextcoin' ),
								'fields' => array(
									array(
										'id'      => 'style',
										'type'    => 'select',
										'title'   => esc_attr__( 'Footer Style', 'businextcoin' ),
										'default' => '01',
										'options' => array(
											'01' => esc_html__( 'Style 01', 'businextcoin' ),
											'02' => esc_html__( 'Style 02', 'businextcoin' ),
											'03' => esc_html__( 'Style 03', 'businextcoin' ),
											'04' => esc_html__( 'Style 04', 'businextcoin' ),
										),
									),
								),
							),
						),
					),
				),
			);

			return $meta_boxes;
		}

	}

	new Businextcoin_Metabox();
}
