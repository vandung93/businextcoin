<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Initial OneClick import for this theme
 */
if ( ! class_exists( 'Businextcoin_Import' ) ) {
	class Businextcoin_Import {

		public function __construct() {
			add_filter( 'insight_core_import_demos', array( $this, 'import_demos' ) );
			add_filter( 'insight_core_import_generate_thumb', array( $this, 'import_generate_thumb' ) );
		}

		public function import_demos() {
			return array(
				'insights'         => array(
					'screenshot' => BUSINEXTCOIN_THEME_URI . '/assets/import/insights/screenshot.jpg',
					'name'       => esc_attr__( 'Insights', 'businextcoin' ),
					'url'        => 'http://api.insightstud.io/update/businext/import/insights-packaged-1.0.0.zip',
				),
				'insights-dark'    => array(
					'screenshot' => BUSINEXTCOIN_THEME_URI . '/assets/import/insights-dark/screenshot.jpg',
					'name'       => esc_attr__( 'Insights Dark', 'businextcoin' ),
					'url'        => 'http://api.insightstud.io/update/businext/import/insights-dark-packaged-1.0.0.zip',
				),
				'exchange'         => array(
					'screenshot' => BUSINEXTCOIN_THEME_URI . '/assets/import/exchange/screenshot.jpg',
					'name'       => esc_attr__( 'Exchange', 'businextcoin' ),
					'url'        => 'http://api.insightstud.io/update/businext/import/exchange-packaged-1.0.0.zip',
				),
				'exchange-dark'    => array(
					'screenshot' => BUSINEXTCOIN_THEME_URI . '/assets/import/exchange-dark/screenshot.jpg',
					'name'       => esc_attr__( 'Exchange Dark', 'businextcoin' ),
					'url'        => 'http://api.insightstud.io/update/businext/import/exchange-dark-packaged-1.0.0.zip',
				),
				'performance'      => array(
					'screenshot' => BUSINEXTCOIN_THEME_URI . '/assets/import/performance/screenshot.jpg',
					'name'       => esc_attr__( 'Performance', 'businextcoin' ),
					'url'        => 'http://api.insightstud.io/update/businext/import/performance-packaged-1.0.0.zip',
				),
				'performance-dark' => array(
					'screenshot' => BUSINEXTCOIN_THEME_URI . '/assets/import/performance-dark/screenshot.jpg',
					'name'       => esc_attr__( 'Performance Dark', 'businextcoin' ),
					'url'        => 'http://api.insightstud.io/update/businext/import/performance-dark-packaged-1.0.0.zip',
				),
				'assessment'       => array(
					'screenshot' => BUSINEXTCOIN_THEME_URI . '/assets/import/assessment/screenshot.jpg',
					'name'       => esc_attr__( 'Assessment', 'businextcoin' ),
					'url'        => 'http://api.insightstud.io/update/businext/import/assessment-packaged-1.0.0.zip',
				),
				'assessment-dark'  => array(
					'screenshot' => BUSINEXTCOIN_THEME_URI . '/assets/import/assessment-dark/screenshot.jpg',
					'name'       => esc_attr__( 'Assessment Dark', 'businextcoin' ),
					'url'        => 'http://api.insightstud.io/update/businext/import/assessment-dark-packaged-1.0.0.zip',
				),
				'investment'       => array(
					'screenshot' => BUSINEXTCOIN_THEME_URI . '/assets/import/investment/screenshot.jpg',
					'name'       => esc_attr__( 'Investment', 'businextcoin' ),
					'url'        => 'http://api.insightstud.io/update/businext/import/investment-packaged-1.0.0.zip',
				),
				'investment-dark'  => array(
					'screenshot' => BUSINEXTCOIN_THEME_URI . '/assets/import/investment-dark/screenshot.jpg',
					'name'       => esc_attr__( 'Investment Dark', 'businextcoin' ),
					'url'        => 'http://api.insightstud.io/update/businext/import/investment-dark-packaged-1.0.0.zip',
				),
				'projections-dark' => array(
					'screenshot' => BUSINEXTCOIN_THEME_URI . '/assets/import/projections-dark/screenshot.jpg',
					'name'       => esc_attr__( 'Projections Dark', 'businextcoin' ),
					'url'        => 'http://api.insightstud.io/update/businext/import/projections-dark-packaged-1.0.0.zip',
				),
			);
		}

		/**
		 * Generate thumbnail while importing
		 */
		function import_generate_thumb() {
			return false;
		}
	}

	new Businextcoin_Import();
}
