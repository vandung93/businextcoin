jQuery( document ).ready( function ( $ ) {
    var $overdraftAmountInput = $( '#overdraft-amount-input' );
    var $wallet               = $( '#wallet-balance' );
    var $overdraftAmount      = $( '#overdraft-amount' );
    var $cost                 = $( '#loan-cost' );
    var $units                = $( '.unit-choices' );
    var $durationSlider       = $( "#slider" );
    var $sliderHandle         = $( "#custom-handle" );
    var currency              = '$';
    var thousandSeparator     = ',';

    var _p = 9.80083408333;

    /*var API_URL = "https://www.bitstamp.net/api/v2/ticker/";
    var rates = {
        "btcusd": null,
        "ethusd": null
    };
    $.each(rates, function(endpoint) {
        $.ajax(API_URL + endpoint, {
            method: "POST"
        }).success(function(data) {
            console.log("API", endpoint, data);
            rates[endpoint] = data;
            tryInit();
        });
    });

    function tryInit() {
        for (var k in rates) {
            if (rates[k] === null) {
                return false;
            }
        }
        console.log(rates);
    }*/

    $overdraftAmountInput.on( 'keyup', function () {
        $( this ).val( replaceCommas( $( this ).val() ) );
    } );

    $( '#overdraft-submit' ).on( 'click', function () {
        $( 'body' ).addClass( 'popup-overdraft-opened' );

        initLoanForm();
    } );

    $( '#form-overdraft-close' ).on( 'click', function () {
        $( 'body' ).removeClass( 'popup-overdraft-opened' );
    } );

    $( '.btn-unit' ).on( 'click', function () {
        $( this ).siblings( '.btn-unit' ).removeClass( 'selected' );
        $( this ).addClass( 'selected' );

        updateLoanFormWallet();
    } );

    $overdraftAmount.on( 'keyup', function () {
        $( this ).val( replaceCommas( $( this ).val() ) );
        updateLoanFormWallet();
        updateLoanFormCost();
    } );

    $wallet.on( 'keyup', function () {
        updateLoanFormAmount();
        updateLoanFormCost();
    } );

    function updateLoanFormAmount() {
        var value  = $units.children( '.selected' ).data( 'value' );
        var amount = parseFloat( $wallet.val() ) * value;

        amount = amount.toFixed( 2 );

        $overdraftAmount.val( amount );
    }

    function updateLoanFormWallet() {
        var value = $units.children( '.selected' ).data( 'value' );

        var amount = normalizeNumber( $overdraftAmount.val() );

        amount = amount / value;
        amount = amount.toFixed( 2 );

        $wallet.val( amount );
    }

    function updateLoanFormCost() {
        var val   = normalizeNumber( $overdraftAmount.val() );
        var month = $durationSlider.slider( 'value' );

        var _total = _p * month * val;

        _total = _total.toFixed( 2 );

        _total = replaceCommas( _total );

        $cost.html( currency + _total );
    }

    $durationSlider.slider( {
        min: 1,
        max: 24,
        value: 10,
        animate: true,
        range: 'min',
        create: function () {
            var value = $( this ).slider( "value" );

            if ( value > 1 ) {
                value += ' months';
            } else {
                value += ' month';
            }

            $sliderHandle.html( '<div class="slider-value">' + value + '</span>' );
        },
        slide: function ( event, ui ) {
            var value = ui.value;

            if ( value > 1 ) {
                value += ' months';
            } else {
                value += ' month';
            }

            $sliderHandle.html( '<div class="slider-value">' + value + '</span>' );

            updateLoanFormCost();
        }
    } );

    function initLoanForm() {
        var loanValue = $overdraftAmountInput.val();

        if ( loanValue ) {
            $overdraftAmount.val( loanValue )
                            .trigger( 'keyup' );
        }
    }

    function replaceCommas( yourNumber ) {
        var components = yourNumber.toString().split( "." );
        if ( components.length === 1 ) {
            components[0] = yourNumber;
        }
        components[0] = components[0].replace( /\D/g, "" ).replace( /\B(?=(\d{3})+(?!\d))/g, "," );
        if ( components.length === 2 ) {
            components[1] = components[1].replace( /\D/g, "" );
        }
        return components.join( "." );
    }

    function normalizeNumber( formatedNumber ) {
        formatedNumber = formatedNumber.replace( /,/g, '' );

        formatedNumber = parseFloat( formatedNumber );

        return formatedNumber;
    }
} );
