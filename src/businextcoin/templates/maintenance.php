<?php
/**
 * Template Name: Maintenance
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Businextcoin
 * @since   1.0
 */

get_header( 'blank' );

$single_image = Businextcoin::setting( 'maintenance_single_image' );
$progress_bar = Businextcoin::setting( 'maintenance_progress_bar' );
$percent      = Businextcoin::setting( 'maintenance_percent' );
$title        = Businextcoin::setting( 'maintenance_title' );
$text         = Businextcoin::setting( 'maintenance_text' );
?>
	<div id="maintenance-wrap" class="maintenance-page">

	</div>
<?php get_footer( 'simple' );
