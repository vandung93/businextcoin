<?php
$text = Businextcoin::setting( 'notification_bar_style_01_text' );
?>
<div <?php Businextcoin::notification_bar_class(); ?>>
	<div class="inner">
		<div class="container">
			<div class="row row-eq-height">
				<div class="col-md-8">
					<div class="notification-bar-wrap notification-bar-left">
						<?php echo '<div class="notification-bar-text-wrap"><div class="notification-bar-text">' . $text . '</div></div>' ?>
					</div>
				</div>
				<div class="col-md-4">
					<div class="notification-bar-wrap notification-bar-right">
						<?php Businextcoin_Templates::notification_bar_links(); ?>
					</div>
				</div>
			</div>

			<div id="close-notification-bar" class="close-notification-bar"><span class="ion-android-close"></span></div>
		</div>
	</div>
</div>
