<?php
/**
 * The template part of single service posts.
 *
 * @package Businextcoin
 * @since   1.0
 */

$style = Businextcoin_Helper::get_post_meta( 'service_style', '' );
if ( $style === '' ) {
	$style = Businextcoin::setting( 'single_service_style' );
}
?>

<?php if ( $style === '01' ) { ?>

	<?php if ( has_post_thumbnail() ) { ?>
		<div class="post-feature post-thumbnail">
			<?php
			$full_image_size = get_the_post_thumbnail_url( null, 'full' );
			Businextcoin_Helper::get_lazy_load_image( array(
				'url'    => $full_image_size,
				'width'  => 1170,
				'height' => 650,
				'crop'   => true,
				'echo'   => true,
				'alt'    => get_the_title(),
			) );
			?>
		</div>
	<?php } ?>

<?php } ?>

<?php the_content(); ?>
