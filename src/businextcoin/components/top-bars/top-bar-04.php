<div <?php Businextcoin::top_bar_class(); ?>>
	<div class="container">
		<div class="row row-eq-height">
			<div class="col-md-12">
				<div class="top-bar-wrap">
					<div class="top-bar-left">
						<?php Businextcoin_Templates::top_bar_info(); ?>
					</div>

					<div class="top-bar-right">
						<?php Businextcoin_Templates::top_bar_social_networks(); ?>
						<?php Businextcoin_Templates::top_bar_language_switcher(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
