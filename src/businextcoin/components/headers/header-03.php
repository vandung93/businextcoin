<header id="page-header" <?php Businextcoin::header_class(); ?>>
	<div id="page-header-inner" class="page-header-inner" data-sticky="1">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12">
					<div class="header-wrap">

						<div class="header-left">
							<?php get_template_part( 'components/branding' ); ?>
						</div>

						<?php get_template_part( 'components/navigation' ); ?>

						<div class="header-right">
							<?php Businextcoin_Templates::header_social_networks( array(
								'tooltip_position' => 'bottom',
							) ); ?>

							<?php Businextcoin_Woo::render_mini_cart(); ?>

							<?php Businextcoin_Templates::header_search_button(); ?>

							<?php Businextcoin_Templates::header_language_switcher(); ?>

							<?php Businextcoin_Templates::header_open_mobile_menu_button(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
