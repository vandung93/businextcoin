=== BusinextCoin ===

Contributors: thememove
Tags: editor-style, featured-images, microformats, post-formats, rtl-language-support, sticky-post, threaded-comments, translation-ready

Requires at least: 4.4
Tested up to: 5.x.x
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

BusinextCoin - Supreme Business and Financial WordPress Theme

== Description ==

BusinextCoin brings the young and vibrant look to your website with high flexibility in customization. This is the theme of beautifully crafted pages and elements for multiple purposes.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =


== Changelog ==

= 1.2.0 - September 14 2020 =
Updated: Insight Core plugin - v.1.7.3
Updated: Revolution Slider plugin - v.6.2.22
Updated: WPBakery Page Builder plugin - v.6.3.0
Updated: WPBakery Page Builder Clipboard plugin - v.4.5.7
Updated: Out-date Woocommerce templates.

= 1.1.0 - July 18 2019 =
1. Update WPBakery Page Builder plugin - v.6.0.4
2. Update Revolution Slider plugin - v.6.0.5
3. Fix smooth scroll for working lastes Chrome.
4. Fix some bugs not working with WPBakery 6.x.x

= 1.0.5 - March 06 2019 =
1. Update WPBakery Page Builder plugin - v.5.7
2. Update Revolution Slider plugin - v.5.4.8.3
3. Update WPBakery Page Builder (Visual Composer) Clipboard plugin - v.4.5.1

= 1.0.4 - January 07 2019 =
1. Update Insight Core plugin - v1.5.4.6
2. Update WPBakery Page Builder plugin - v.5.6

= 1.0.3 - October 08 2018 =
1. Update Insight Core plugin - v1.5.4.4
2. Update WPBakery Page Builder plugin - v.5.5.5
3. Change support chanel link

= 1.0.2 - October 08 2018 =
1. Update Insight Core plugin - v1.5.3.8
2. Update WPBakery Page Builder plugin - v.5.5.4
3. Fix custom body typography not working properly

= 1.0.1 - August 02 2018 =
1. Update demo content import.

= 1.0.0 - July 31 2018 =
* Initial release

== Credits ==

* Based on Underscores http://underscores.me/, (C) 2012-2015 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css http://necolas.github.io/normalize.css/, (C) 2012-2015 Nicolas Gallagher and Jonathan Neal, [MIT](http://opensource.org/licenses/MIT)
