<?php
/**
 * The template for displaying all single service posts.
 *
 * @package Businextcoin
 * @since   1.0
 */
get_header();
?>
<?php Businextcoin_Templates::title_bar(); ?>
	<div id="page-content" class="page-content">
		<div class="container">
			<div class="row">

				<?php Businextcoin_Templates::render_sidebar( 'left' ); ?>

				<div class="page-main-content">
					<?php while ( have_posts() ) : the_post(); ?>

						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

							<?php get_template_part( 'components/content-single', 'service' ); ?>

						</article>

						<?php
						// If comments are open or we have at least one comment, load up the comment template.
						if ( Businextcoin::setting( 'single_service_comment_enable' ) === '1' && ( comments_open() || get_comments_number() ) ) :
							comments_template();
						endif;
						?>
					<?php endwhile; ?>
				</div>

				<?php Businextcoin_Templates::render_sidebar( 'right' ); ?>

			</div>
		</div>
	</div>
<?php
get_footer();
