<?php

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Businextcoin_Child_Functions {

	public function __construct() {
		add_action( 'wp_footer', array( $this, 'demo_options_template' ) );
	}

	function demo_options_template() {
		$theme_description  = 'Businext embraces clean pre-made demos, headers and portfolios exclusively designed for businesses & corporates operating in a wide range of industries. Highly responsive, professional and functional are what you can enjoy from this multipurpose theme.';
		$support_link       = 'https://thememove.ticksy.com/';
		$documentation_link = 'https://document.thememove.com/businext/';

		$image_dir = get_stylesheet_directory_uri() . '/assets/images/';
		$links     = array(
			array(
				'url'       => network_home_url( '/insights/' ),
				'thumbnail' => $image_dir . 'home-light-insights-preview.jpg',
				'title'     => 'Insights',
			),
			array(
				'url'       => network_home_url( '/insights-dark/' ),
				'thumbnail' => $image_dir . 'home-dark-insights-preview.jpg',
				'title'     => 'Insights Dark',
			),
			array(
				'url'       => network_home_url( '/exchange/' ),
				'thumbnail' => $image_dir . 'home-light-exchange-preview.jpg',
				'title'     => 'Exchange',
			),
			array(
				'url'       => network_home_url( '/exchange-dark/' ),
				'thumbnail' => $image_dir . 'home-dark-exchange-preview.jpg',
				'title'     => 'Exchange Dark',
			),
			array(
				'url'       => network_home_url( '/performance/' ),
				'thumbnail' => $image_dir . 'home-light-performance-preview.jpg',
				'title'     => 'Performance',
			),
			array(
				'url'       => network_home_url( '/performance-dark/' ),
				'thumbnail' => $image_dir . 'home-dark-performance-preview.jpg',
				'title'     => 'Performance Dark',
			),
			array(
				'url'       => network_home_url( '/assessment/' ),
				'thumbnail' => $image_dir . 'home-light-assessment-preview.jpg',
				'title'     => 'Assessment',
			),
			array(
				'url'       => network_home_url( '/assessment-dark/' ),
				'thumbnail' => $image_dir . 'home-dark-assessment-preview.jpg',
				'title'     => 'Assessment Dark',
			),
			array(
				'url'       => network_home_url( '/investment/' ),
				'thumbnail' => $image_dir . 'home-light-investment-preview.jpg',
				'title'     => 'Investment',
			),
			array(
				'url'       => network_home_url( '/investment-dark/' ),
				'thumbnail' => $image_dir . 'home-dark-investment-preview.jpg',
				'title'     => 'Investment Dark',
			),
			array(
				'url'       => network_home_url( '/projections-dark/' ),
				'thumbnail' => $image_dir . 'home-dark-projections-preview.jpg',
				'title'     => 'Projections Dark',
			),
		);
		?>
		<div class="tm-demo-options-wrapper">
			<div class="tm-demo-options-toolbar">
				<?php $toolbar_link_classes = 'hint--bounce hint--left hint--primary primary-color-hover-important'; ?>
				<a href="#"
				   class="<?php echo esc_attr( $toolbar_link_classes ); ?>"
				   id="toggle-quick-options"
				   aria-label="<?php echo esc_attr__( 'Quick Options', 'businextcoin' ); ?>">
					<i class="ion-ios-gear"></i>
				</a>
				<a href="<?php echo esc_url( $support_link ); ?>"
				   target="_blank"
				   class="<?php echo esc_attr( $toolbar_link_classes ); ?>"
				   aria-label="<?php echo esc_attr__( 'Support Center', 'businextcoin' ); ?>">
					<i class="ion-help-buoy"></i>
				</a>
				<a href="<?php echo esc_url( $documentation_link ); ?>"
				   target="_blank"
				   class="<?php echo esc_attr( $toolbar_link_classes ); ?>"
				   aria-label="<?php echo esc_attr__( 'Documentation', 'businextcoin' ); ?>">
					<i class="ion-document-text"></i>
				</a>
				<a href="<?php echo esc_url( BUSINEXTCOIN_DEMO_BUTTON_LINK ); ?>"
				   target="_blank"
				   class="<?php echo esc_attr( $toolbar_link_classes ); ?>"
				   aria-label="<?php echo esc_attr__( 'Purchase Now', 'businextcoin' ); ?>">
					<i class="ion-ios-cart"></i>
				</a>
			</div>
			<div id="tm-demo-panel" class="tm-demo-panel">
				<div class="tm-demo-panel-header">
					<a class="tm-button style-flat tm-button-sm tm-button-primary tm-btn-purchase has-icon icon-left"
					   href="<?php echo esc_url( BUSINEXTCOIN_DEMO_BUTTON_LINK ); ?>" target="_blank">
						<span class="button-icon">
							<i class="ion-android-cart"></i>
						</span>
						<span class="button-text">
							<?php esc_html_e( 'Buy Now' ); ?>
						</span>
					</a>
					<div class="demo-option-desc">
						<?php echo $theme_description; ?>
					</div>
				</div>

				<div class="quick-option-list">
					<?php foreach ( $links as $link ) : ?>
						<a href="<?php echo $link['url']; ?>"
						   class="hint--bounce hint--top"
						   aria-label="<?php echo esc_html( $link['title'] ); ?>"
						>
							<img src="<?php echo $link['thumbnail']; ?>"
							     alt="<?php echo $link['title']; ?>"
							     width="150" height="180">
						</a>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
		<script type='text/javascript'>
            jQuery( document ).ready( function ( $ ) {
                'use strict';
                $( '#toggle-quick-options' ).on( 'click', function ( e ) {
                    e.preventDefault();
                    $( this ).parents( '.tm-demo-options-wrapper' ).toggleClass( 'open' );
                } );
            } );
		</script>
		<?php
	}
}

new Businextcoin_Child_Functions();
