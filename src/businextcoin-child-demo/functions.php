<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

define( 'BUSINEXTCOIN_DEMO_BUTTON_TEXT', 'Buy Now 59$' );
define( 'BUSINEXTCOIN_DEMO_BUTTON_LINK', 'https://themeforest.net/item/businext-supreme-businesses-and-financial-institutions-wordpress-theme/21535442' );

/**
 * Enqueue scripts for child theme
 */
function businextcoin_child_demo_enqueue_scripts() {
	$min = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG == true ? '' : '.min';

	wp_enqueue_style( 'businextcoin-style', BUSINEXTCOIN_THEME_URI . "/style{$min}.css" );
	wp_enqueue_style( 'businextcoin-child-demo-style', get_stylesheet_directory_uri() . '/style.css', array( 'businextcoin-style' ), wp_get_theme()->get( 'Version' ) );

	// Enqueue BS Script for Dev.
	$domain = wp_parse_url( get_stylesheet_directory_uri() );
	$host   = $domain['host'];

	if ( strpos( $host, '.local' ) !== false ) {
		$url = sprintf( 'http://%s:3000/browser-sync/browser-sync-client.js', $host );
		$ch  = curl_init();
		curl_setopt( $ch, CURLOPT_URL, $url );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		$header = curl_exec( $ch );
		curl_close( $ch );
		if ( $header && strpos( $header[0], '400' ) === false ) {
			wp_enqueue_script( '__bs_script__', $url, array(), null, true );
		}
	}
}

add_action( 'wp_enqueue_scripts', 'businextcoin_child_demo_enqueue_scripts' );

remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'wp_shortlink_wp_head' );
remove_action( 'wp_head', 'wp_generator' );

require_once( get_stylesheet_directory() . '/inc/class-functions.php' );
