<?php
define( 'DEMO_OPTIONS_URL', get_stylesheet_directory_uri() . '/demo-options/' );
define( 'DEMO_OPTIONS_PATH', get_stylesheet_directory() . '/demo-options/' );

add_action( 'wp_enqueue_scripts', 'demo_options_enqueue_assets' );

function demo_options_enqueue_assets() {
	wp_enqueue_script( 'demo-options-bootstrap', DEMO_OPTIONS_URL . 'assets/components/bootstrap/bootstrap.js', array(), '1.0', true );

	wp_enqueue_style( 'demo-options-spectrum', DEMO_OPTIONS_URL . 'assets/components/spectrum/spectrum.css', array(), '1.0' );
	wp_enqueue_script( 'demo-options-spectrum', DEMO_OPTIONS_URL . 'assets/components/spectrum/spectrum.js', array(), '1.0', true );

	//wp_enqueue_style( 'demo-options-app', DEMO_OPTIONS_URL . 'assets/css/front.css', array(), '1.0' );
	wp_enqueue_script( 'demo-options-app', DEMO_OPTIONS_URL . 'assets/js/front.js', array(
		'jquery',
		'backbone',
	), '1.0', true );
}

add_action( 'wp_footer', 'demo_options_generate_html' );

function demo_options_generate_html() {
	$settings = apply_filters( 'demo_options_settings', array() );

	// Fill values.
	foreach ( $settings as &$section ) {
		foreach ( $section['settings'] as &$setting ) {

			if ( 'reload' === $setting['transport'] ) {

				if ( ! empty( $setting['reload'] ) && 'param' === $setting['reload'] ) {
					if ( isset( $_GET[ $setting['settings'] ] ) ) {
						$setting['value'] = $_GET[ $setting['settings'] ];
					}
				} else {

					$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

					foreach ( $setting['choices'] as $key => $value ) {

						if ( $actual_link == $key ) {
							$setting['value'] = $key;
							break;
						}
					}
				}
			}

			if ( ! isset( $setting['value'] ) ) {
				$setting['value'] = $setting['default'];
			}
		}
	}
	require( DEMO_OPTIONS_PATH . 'templates/app.php' );
}
