(
	function( $ ) {
		var Control = Backbone.Model.extend( {
												 initialize: function() {
													 this.on( 'change', this.onChange );
												 }, onChange: function() {
				var settings = this.get( 'settings' ), value = this.get( 'value' );

				if ( this.get( 'transport' ) == 'postMessage' ) {
					var $style = $( '#do-style-' + settings );

					if ( ! $style.length ) {
						$style = $( '<style id="do-style-' + settings + '"></style>' ).appendTo( 'body' );
					}

					var output = this.get( 'output' ).replace( /\$value/g, this.get( 'value' ) );

					$style.html( output );
				} else {
					if ( typeof this.get( 'reload' ) != 'undefined' && this.get( 'reload' ) == 'param' ) {
						var query = window.location.search, params = [];

						if ( query ) {
							query = query.replace( new RegExp( settings + '=[^&]+&*', 'g' ), '' );
							query = query.replace( /&+$/, '' );
							query = query.replace( /^\?/, '' );
						}

						window.location.search = '?' + query + '&' + settings + '=' + value;
					} else {
						window.location = value;
					}
				}
			}
											 } );

		var ControlCollection = Backbone.Collection.extend( {
																model: Control
															} );

		var Section = Backbone.Model.extend( {
												 defaults: {
													 label: '', controls: []
												 }
											 } );

		var SectionCollection = Backbone.Collection.extend( {
																model: Section
															} );

		var ControlView = Backbone.View.extend( {
													type: 'none',
													tagName: 'div',
													className: 'do-control',
													initialize: function() {
														this.template = _.template( $( '#do-control-' + this.model.get( 'type' ) + '-template' )
																						.html() );
													},
													render: function() {
														this.$el.html( this.template( this.model.attributes ) );

														if ( typeof this.model.attributes['settings'] != 'undefined' ) {
															this.$el.attr( 'id', 'do-control-' + this.model.attributes['settings'] );
														}

														if ( $.isFunction( this.initControl ) ) {
															this.initControl();
														}

														return this;
													}
												} );

		ControlView.create = function( type, properties ) {
			var controlView;

			switch ( type ) {
				case 'dropdown':
					controlView = new DropdownControlView( properties );
					break;
				case 'color':
					controlView = new ColorControlView( properties );
					break;
				case 'buttonset':
					controlView = new ButtonsetControlView( properties );
					break;
				case 'colorpattern':
					controlView = new ColorPatternControlView( properties );
					break;
				case 'imagepattern':
					controlView = new ImagePatternControlView( properties );
					break;
				default:
					console.log( 'Unknown control', properties );
					break;
			}

			return controlView;
		};

		var DropdownControlView = ControlView.extend( {
														  type: 'dropdown', events: {
				'change select': 'triggerChange',
			}, triggerChange: function() {
				this.model.set( 'value', this.$el.find( 'select' )[0].value );
			}
													  } );

		var ColorControlView = ControlView.extend( {
													   type: 'color', initControl: function() {
				var self   = this;
				var $input = this.$el.find( '.do-color-input' );

				if ( ! $input.data( 'spectrum.id' ) ) {
					$input.spectrum( {
										 color: self.model.get( 'value' ),
										 showInput: true,
										 showButtons: false,
										 preferredFormat: 'rgb',
										 showAlpha: true,
										 appendTo: '#do-container',
										 move: function( color ) {
											 self.triggerChange( color.toRgbString() );
										 },
										 change: function( color ) {
											 self.triggerChange( color.toRgbString() );
										 }
									 } );
				}
			}, triggerChange: function( value ) {
				this.model.set( 'value', value );
			}
												   } );

		var ButtonsetControlView = ControlView.extend( {
														   type: 'buttonset', events: {
				'click .do-button': 'triggerChange',
			}, triggerChange: function( evt ) {
				var $button = $( evt.target );

				this.model.set( 'value', $button.data( 'value' ) );
				this.$el.find( '.active' ).removeClass( 'active' );
				$button.addClass( 'active' );

				return false;
			}
													   } );

		var ColorPatternControlView = ControlView.extend( {
															  type: 'colorpattern', events: {
				'click .do-button': 'triggerChange',
			}, triggerChange: function( evt ) {
				var $button = $( evt.target );

				this.model.set( 'value', $button.data( 'value' ) );
				this.$el.find( '.active' ).removeClass( 'active' );
				$button.addClass( 'active' );

				return false;
			}
														  } );

		var ImagePatternControlView = ControlView.extend( {
															  type: 'imagepattern', events: {
				'click .do-button': 'triggerChange',
			}, triggerChange: function( evt ) {
				var $button = $( evt.target );

				this.model.set( 'value', $button.data( 'value' ) );
				this.$el.find( '.active' ).removeClass( 'active' );
				$button.addClass( 'active' );

				return false;
			}
														  } );

		var SectionView = Backbone.View.extend( {
													tagName: 'div',
													className: 'do-section panel',
													template: _.template( $( '#do-section-template' ).html() ),
													initialize: function() {
														var self      = this;
														this.controls = new ControlCollection();
														this.listenTo( this.controls, 'add', this.addControl );

														_.each( this.model.get( 'settings' ), function( setting ) {
															self.controls.add( new Control( $.extend( {
																										  type: setting.type
																									  }, setting ) ) );
														} );
													},
													render: function() {
														var self = this;
														this.$el.html( this.template( this.model.attributes ) );

														this.controls.each( function( control ) {
															var controlView = ControlView.create( control.get( 'type' ), { model: control } );
															self.$el.find( '.control-list' ).append( controlView.el );
															controlView.render();
														} );

														this.$el.find( '.control-list' )
															.on( 'shown.bs.collapse', function() {
																document.cookie = 'do_open_section=' + $( this )
																		.attr( 'id' ) + ';path=/';
															} );

														var match = document.cookie.match( /do_open_section=([^;]+)/ );
														if ( match ) {
															this.$el.find( '#' + match[1] ).addClass( 'in' );
														} else if ( this.model.first ) {
															this.$el.find( '.control-list' ).addClass( 'in' );
														}

														return this;
													}
												} );

		var AppView = Backbone.View.extend( {
												el: '#do-container', events: {
				'click .do-toggle-panel': 'togglePanel',
				'mouseover .do-toggle-panel': 'toggleMouseOverHandler',
				'mouseout .do-toggle-panel': 'toggleMouseOutHandler',
			}, initialize: function() {
				var self      = this;
				this.sections = new SectionCollection();
				this.listenTo( this.sections, 'add', this.addSection );

				if ( $.isArray( do_sections ) ) {
					_.each( do_sections, function( section_data_item, index ) {
						var section = new Section( $.extend( {}, section_data_item ) );

						if ( 0 === index ) {
							section.first = true;
						}

						self.sections.add( section );
					} );
				}

				if ( ! document.cookie.match( /do_toggle_open=1/ ) ) {
					self.$el.addClass( 'open' );

					setTimeout( function() {
						self.$el.removeClass( 'open' );
						document.cookie = 'do_toggle_open=1;path=/'
					}, 4000 );
				}
			}, addSection: function( section ) {
				var sectionView = new SectionView( { model: section } );
				this.$el.find( '.do-section-list' ).append( sectionView.render().el );
			}, togglePanel: function() {
				this.$el.toggleClass( 'open' );
				return false;
			}, toggleMouseOverHandler: function() {
				this.$el.find( '.do-toggle-panel .fa' ).removeClass( 'fa-spin' );
			}, toggleMouseOutHandler: function() {
				this.$el.find( '.do-toggle-panel .fa' ).addClass( 'fa-spin' );
			}
											} );

		$( document ).ready( function() {
			new AppView();
		} );
	}
)( jQuery );

jQuery( document ).ready( function( $ ) {
	'use strict';

	$( '#do-section-list' ).niceScroll( {
											//autohidemode: false,
											cursorcolor: '#000',
											cursoropacitymax: 0.5,
											cursorborder: 0,
											cursorborderradius: 0
										} );
} );
