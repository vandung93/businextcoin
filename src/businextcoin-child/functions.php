<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Enqueue child scripts
 */
if ( ! function_exists( 'businextcoin_child_enqueue_scripts' ) ) {
	function businextcoin_child_enqueue_scripts() {
		$min = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG == true ? '' : '.min';

		wp_enqueue_style( 'businextcoin-style', BUSINEXTCOIN_THEME_URI . "/style{$min}.css" );
		wp_enqueue_style( 'businextcoin-child-style', get_stylesheet_directory_uri() . '/style.css', array( 'businextcoin-style' ), wp_get_theme()->get( 'Version' ) );
	}
}

add_action( 'wp_enqueue_scripts', 'businextcoin_child_enqueue_scripts' );
